<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 6/17/17
 * Time: 18:59
 */

class Libauth{

    function logged_in(){
        if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'v-popcult' || $_SERVER['PHP_AUTH_PW'] != 'sfpopcult') {
            header('WWW-Authenticate: Basic realm="MyProject"');
            header('HTTP/1.0 401 Unauthorized');
            die('Access Denied');
        }else{
            return true;
        }
    }
}