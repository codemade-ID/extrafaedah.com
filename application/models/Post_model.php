<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model{
	
    function __construct(){
		parent::__construct();
    }

    function get_all($offset=0,$limit=10, $order_by = 'id', $sortorder = 'DESC', $param=array()){
			$db = $this->db;
			$table = "submission";
			$select = !empty($param['select'])?$param['select']:'*';
			$where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
			$like_clause = !empty($param['like_clause'])?$param['like_clause']:'';
			$db->select($select)
				->from($table.' a')
				;
			(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			(!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
			$db->order_by($order_by,$sortorder);
			$q = $db->get();
			$data['results'] = $q->result();
				$db->select('count(a.id) as total_results')
				;
			 	(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			 	(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			$q = $db->get($table .' a');
			$row = $q->row();
			$data['total_results'] = $row->total_results;
			return $data;
	}

	function add(){
		$data = array(
    		"namalengkap"		=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->author_name,
    		// "profesi"			=>	$this->input->post("profesi"),
    		"instagramlink"		=>	$this->input->post("instagramlink"),
    		"email"				=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->author_name,
    		"thumbnail"			=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->thumbnail_url,
    		"status"			=>	$this->input->post("status"),
    		"modified_datetime"	=>	date("Y-m-d H:i:s")
    	);
          
       $this->db->insert('submission',$data);

		return true;
	}

	function update($id){
		$data = array(
    		"namalengkap"		=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->author_name,
    		// "profesi"			=>	$this->input->post("profesi"),
    		"instagramlink"		=>	$this->input->post("instagramlink"),
    		"email"				=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->author_name,
    		"thumbnail"			=>	$this->_get_ig_post($this->input->post("instagramlink"))->instagram->thumbnail_url,
    		"status"			=>	$this->input->post("status"),
    		"modified_datetime"	=>	date("Y-m-d H:i:s")
    	);
          
        // var_dump($this->_get_ig_post($this->input->post("instagramlink"))->instagram->thumbnail_url);   exit;
        $this->db->where('id',$id);    
		$this->db->update('submission',$data);

		return true;
	}

	function _get_ig_image($ig_link = ""){
		$ch = curl_init(); 

	    curl_setopt($ch, CURLOPT_URL, "https://api.codemade.co.id/instagram/post?url=".$ig_link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		$output = curl_exec($ch); 
		curl_close($ch);      
		$response = json_decode($output);
		// var_dump($response);exit;
		return $response->image_url;
	}

	function _get_ig_post($ig_link = ""){
		$ch = curl_init(); 

	    curl_setopt($ch, CURLOPT_URL, "https://api.codemade.co.id/instagram/post?url=".$ig_link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		$output = curl_exec($ch); 
		curl_close($ch);      
		$response = json_decode($output);
		return $response;
	}

	function upload_thumbnail($field_name){
		if(!empty($_FILES[$field_name]['name']))
        {

                // do upload
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'jpg|JPG|png|PNG';
                $config['max_size'] = '10000';
                
                $this->load->library('upload', $config);

                if ($this->upload->do_upload($field_name))
                {
                    $upload_data = $this->upload->data();
                    return $upload_data['file_name'];
                }else{
                	echo  $this->upload->display_errors('<p>', '</p>');
                	exit;
                }
        }
	}

	function get_by_id($id){
		$Q =  $this->db->select("*")
			->from('posts a')
			->where('id', $id)
			->get();
		return $Q->row();
		$Q->free_result();
	}

	function delete($id){
		$this->db->where('id',$id)->update('submission', array('status' => -1));
		return TRUE;
	}

}