<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model{
	
    function __construct(){
		parent::__construct();
    }

    function get_winners($tahap = 1, $prize = 10){
        $sql    =   "SELECT * FROM sfweb_wow_winner
                         WHERE 1
                            AND tahap = ?
                            AND prize = ?
                         ORDER BY mdn ASC

                        ";
            $query  =   $this->db->query($sql, array($tahap, $prize));
            return $query->result();
    }

    function search_winners($tahap = 1, $mdn = 10){
        $sql    =   "SELECT * FROM sfweb_wow_winner
                         WHERE 1
                            AND tahap = ? 
                            AND (mdn = ? OR coupon = ?)
                         ORDER BY mdn ASC

                        ";
            $query  =   $this->db->query($sql, array($tahap, $mdn, $mdn));
            return $query->row();
    }

    function get_galeri_undian($tahap = 1, $tipe = 'photos'){
        $sql    =   "SELECT * FROM sfweb_wow_galeriundian
                         WHERE 1
                            AND tahap = ?
                            AND tipe = ?
                         ORDER BY id ASC

                        ";
            $query  =   $this->db->query($sql, array($tahap, $tipe));
            return $query->result();
    }


    function get_table($offset=0,$limit=10, $order_by = 'id', $sortorder = 'DESC', $param=array()){
			$db = $this->db;
			$table = $param['table'];
			$select = !empty($param['select'])?$param['select']:'*';
			$join_table = !empty($param['join_table'])?$param['join_table']:'';
			$where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
			$like_clause = !empty($param['like_clause'])?$param['like_clause']:'';
			$or_like_clause = !empty($param['or_like_clause'])?$param['or_like_clause']:'';
			$group_by = !empty($param['group_by'])?$param['group_by']:'';
			$where_in = !empty($param['where_in'])?$param['where_in']:'';
			// $db->distinct();
			$db->select($select)
				->from($table.' a')
				;
			if(!empty($join_table)){
				foreach($join_table as $key => $val){
					$db->join($key,$val);
				}
			}
			(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			(!empty($or_like_clause)||!empty($or_like_clause))?$db->like($or_like_clause):'';
			(!empty($where_in))?$db->where_in($where_in['field'], $where_in['value']):'';
			(!empty($group_by))?$db->group_by($group_by):'';
			$db->order_by($order_by,$sortorder);
			$q = $db->get();
			$data['results'] = $q->result();
				// $db->distinct();
				$db->select('count(a.'.$order_by.') as total_results')
				;
				if(!empty($join_table)){
					foreach($join_table as $key => $val){
						$db->join($key,$val);
					}
				}
				(!empty($where_in))?$db->where_in($where_in):'';
			 	(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			 	(!empty($where_in))?$db->where_in($where_in['field'], $where_in['value']):'';
			 	(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			 	(!empty($or_like_clause)||!empty($or_like_clause))?$db->like($or_like_clause):'';
			 	(!empty($group_by))?$db->group_by($group_by):'';
			$q = $db->get($table .' a');
			$row = $q->row();
			$data['total_results'] = $row->total_results;
			return $data;
	}

	function get_table_db_old($offset=0,$limit=10, $order_by = 'id', $sortorder = 'DESC', $param=array()){
			$db = $this->load->database('maritim',TRUE);
			$table = $param['table'];
			$select = !empty($param['select'])?$param['select']:'*';
			$join_table = !empty($param['join_table'])?$param['join_table']:'';
			$where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
			$like_clause = !empty($param['like_clause'])?$param['like_clause']:'';
			$db->distinct();
			$db->select($select)
				->from($table.' a')
				;
			if(!empty($join_table)){
				foreach($join_table as $key => $val){
					$db->join($key,$val);
				}
			}
			(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			(!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
			$db->order_by($order_by,$sortorder);
			$q = $db->get();
			$data['results'] = $q->result();
				$db->distinct();
				$db->select('count('.$order_by.') as total_results')
				;
				if(!empty($join_table)){
					foreach($join_table as $key => $val){
						$db->join($key,$val);
					}
				}
			 	(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			 	(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			$q = $db->get($table .' a');
			$row = $q->row();
			$data['total_results'] = $row->total_results;
			return $data;
	}
}