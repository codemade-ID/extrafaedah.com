<section id="concert-prod">
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/wow-concert-desktop-copy.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/wow-concert-desktop-copy.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
	<div class="bg-white pad-10 pad-l pad-r your-ticket">
		<h2 class="text-center color-merah TruenoSBd">
			<a href="<?php echo base_url();?>concert/special-invitation" class="color-merah">
			GET YOUR SPECIAL INVITATION <span><img src="<?php echo base_url()?>assets/img/ticket_icon.png"></span>
			</a>
		</h2>
		<!-- <h2 class="text-center color-merah TruenoSBd">GET YOUR SPECIAL INVITATION</h2>
		<p class="text-center color-merah TruenoSBd font-20">25 Februari 2020</p> -->
	</div>
	<div class="line-up">
		<h2 class="text-center TruenoRg font-30 mar-40 mar-l mar-r mar-b" style="font-weight: normal;">
			LINE UP
		</h2>
		<h4 class="text-center font-22 TruenoRg">INTERNATIONAL ARTIST</h4>
		<div class="container artist_main">
			<div class="row">
				<div class="col-md-6">
					<img src="<?php echo base_url()?>assets/img/artist_main.png" class="img-responsive">
				</div>
				<div class="col-md-6">
					<img src="<?php echo base_url()?>assets/img/artist_main.png" class="img-responsive">
				</div>
			</div>
		</div>
		<h4 class="text-center mar-30 mar-l mar-r mar-b font-22 TruenoRg">LOCAL ARTIST</h4>
		<div class="container local">
			<div class="row mar-30 mar-l mar-r mar-b">
				<?php if(!empty($lineup)) :?>
					<?php foreach($lineup as $l):?>
						<div class="col-md-3">
							<img src="<?php echo $l['image']?>" class="img-responsive">
							<!-- <span><?php echo $l['name']?></span> -->
						</div>
					<?php endforeach;?>
				<?php endif;?>
			</div>
		</div>
	</div>
	<?php $this->load->view('go_tnc');?>
</section>