<section id="gallery">
	<div class="container">
		<h1>Lipsync Challenge</h1>
		<div class="row">
			<div class="col-md-6">
				<p>Mechanism:</p>
				<ol>
					<li>Follow Instagram @smartfrenworld</li>
					<li>Share video lipsync lagu pilihan sambil bergaya di Instagram</li>
					<li>Ceritakan alasan kamu bertemu artis pilihanmu di caption dengan #SmartfrenWOW #WOWlipsyncBattle #WOWconcert</li>
					<li>Tag 3 teman dan @smartfrenworld</li>
					<li>Pilihan lagu sudah ditentukan oleh Smartfren</li>
					<li>Peserta diperbolehkan berjoget dalam video</li>
				</ol>
			</div>
			<div class="col-md-6">
				<p>Periode : 4-17 September</p>
				<p>Hadiah: <br />
				2 Exclusive Photo Session Per-Community with Chung Ha or Ateez</p>
				<p>
				Pilihan Lagu:<br />
				Chung Ha - Snapping<br />
				ATEEZ - Wave
				</p>
			</div>
			<div class="col-md-12 mar-50 mar-l mar-r">
				<p class="lu-name">CHUNG HA</p>
				<div class="ateez multiple-items">
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/chunghaindonesia/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_ChunghaIndonesia.png" class="img-responsive">
							<p class="lu-name">Chunghaindonesia</p>
						</a>
					</div>
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/igeneration.official/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_igeneration.png" class="img-responsive">
							<p class="lu-name">Igeneration</p>
						</a>
					</div>
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/covermusiktop/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_CoverMusikTop.png" class="img-responsive">
							<p class="lu-name">Covermusiktop</p>
						</a>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					// $('.ateez').slick({
					// 	  dots: false,
					// 	  infinite: false,
					// 	  speed: 300,
					// 	  slidesToShow: 4,
					// 	  slidesToScroll: 1,
					// 	  responsive: [
					// 	    {
					// 	      breakpoint: 1024,
					// 	      settings: {
					// 	        slidesToShow: 3,
					// 	        slidesToScroll: 3,
					// 	        infinite: true,
					// 	        dots: true
					// 	      }
					// 	    },
					// 	    {
					// 	      breakpoint: 600,
					// 	      settings: {
					// 	        slidesToShow: 2,
					// 	        slidesToScroll: 2
					// 	      }
					// 	    },
					// 	    {
					// 	      breakpoint: 480,
					// 	      settings: {
					// 	        slidesToShow: 1,
					// 	        slidesToScroll: 1
					// 	      }
					// 	    }
					// 	    // You can unslick at a given breakpoint now by adding:
					// 	    // settings: "unslick"
					// 	    // instead of a settings object
					// 	  ]
					// 	});
				})
			</script>
			<div class="col-md-12 mar-50 mar-l mar-r">
				<p class="lu-name">ATEEZ</p>
				<div class="chungha multiple-items">
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/covermusiktop/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_AteezIndonesia.png" class="img-responsive">
							<p class="lu-name">Atez indonesia</p>
						</a>
					</div>
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/wowfakta.ateez/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_WowfaktaAteez.png" class="img-responsive">
							<p class="lu-name">Wowfakta ateez</p>
						</a>
					</div>
					<div class="col-md-3 text-center">
						<a href="https://www.instagram.com/vokalplus/" target="_blank">
							<img src="assets/img/lipsync/LipsyncCommunity_VokalPlus.png" class="img-responsive">
							<p class="lu-name">Vokal plus:</p>
						</a>
					</div>
					
				</div>
			</div>
			<script type="text/javascript">
				/**
				 * Created by chase.marcum on 11/13/2014.
				 */
				$(document).ready(function ($) {
				  
				  var gadgetCarousel = $(".multiple-items");
				  
				  gadgetCarousel.each(function() {
				    if ($(this).is(".ateez")) {
						$(this).slick({
					          dots: false,
							  infinite: false,
							  speed: 300,
							  slidesToShow: 4,
							  slidesToScroll: 1,
							  responsive: [
							    {
							      breakpoint: 1024,
							      settings: {
							        slidesToShow: 3,
							        slidesToScroll: 3,
							        infinite: true,
							        dots: true
							      }
							    },
							    {
							      breakpoint: 600,
							      settings: {
							        slidesToShow: 2,
							        slidesToScroll: 2
							      }
							    },
							    {
							      breakpoint: 480,
							      settings: {
							        slidesToShow: 1,
							        slidesToScroll: 1
							      }
							    }
							    // You can unslick at a given breakpoint now by adding:
							    // settings: "unslick"
							    // instead of a settings object
							  ]
				      });
				    } 
				    else if ($(this).is(".chungha")){
				      $(this).slick({
				          dots: false,
						  infinite: false,
						  speed: 300,
						  slidesToShow: 4,
						  slidesToScroll: 1,
						  responsive: [
						    {
						      breakpoint: 1024,
						      settings: {
						        slidesToShow: 3,
						        slidesToScroll: 3,
						        infinite: true,
						        dots: true
						      }
						    },
						    {
						      breakpoint: 600,
						      settings: {
						        slidesToShow: 2,
						        slidesToScroll: 2
						      }
						    },
						    {
						      breakpoint: 480,
						      settings: {
						        slidesToShow: 1,
						        slidesToScroll: 1
						      }
						    }
						    // You can unslick at a given breakpoint now by adding:
						    // settings: "unslick"
						    // instead of a settings object
						  ]
				      });
				    }
				    else {
				      $(this).slick();
				    }
				  })
				  
				  
				  
				  
				}); // end of os-carousel.js
			</script>
		</div>
		<div class="text-center mar-50">
			<a href="/wow" class="btn btn-back">Back</a>
		</div>
	</div>
</section>