<div class="page-content-wrapper">
        <div class="page-content">
            

            <h3 class="page-title">
                <?php echo $title;?>
            </h3>

            <div class="row ">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <a href="<?php echo base_url()?>/admin/post/add" class="btn btn-success">Add Record</a>
                        </div>
                        <div class="portlet-body">
                            <table class="table datatable table-striped table-bordered table-hover" id="videos">
                                <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Nama Lengkap
                                    </th>
                                    <th>
                                        Instagram
                                    </th>
                                    <th>
                                        Link Instagram
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                                </thead>
                                <?php if(!empty($rows)) :?>
                                    <tbody>
                                    <?php foreach($rows as $row) :?>
                                        <tr>
                                        <td><?php echo $row->id?></td>
                                        <td><?php echo ($row->status == 1?'approved':'pending');?></td>
                                        <td class="center"><?php echo $row->namalengkap?></td>
                                        <td class="center"><?php echo $row->email?></td>
                                        <td class="center"><?php echo $row->instagramlink?></td>
                                        <td class="center" rel="<?=$row->id?>">
                                            <a class="btn btn-info" href="<?php echo base_url()?>admin/post/edit/<?php echo $row->id?>/?<?php echo $query_string;?>">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a class="btn btn-danger delete" href="#">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                <?php endif;?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>