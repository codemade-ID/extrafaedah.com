<div class="page-content-wrapper">
        <div class="page-content">
            
            <h3 class="page-title">
                <?php echo $title;?>
            </h3>
            <?php echo validation_errors();?>
            <div class="row ">
                <form method="post" enctype="multipart/form-data" action="">
                    <div class="col-md-8 col-sm-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="tools">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-save"></i> Save
                                    </button>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <!-- <div class="form-group">
                                    <label for="">Nama Lengkap</label>
                                    <input type="text"
                                           name="namalengkap"
                                           class="form-control"
                                           value="<?php echo (!empty($row->namalengkap)?$row->namalengkap:'')?>"
                                           id="title" required>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="">Instagram Account</label>
                                    <input type="text"
                                           name="email"
                                           class="form-control"
                                           value="<?php echo (!empty($row->email)?$row->email:'')?>"
                                           id="title" required>
                                    
                                </div> -->

                                <div class="form-group">
                                    <label for="">Instagram Post Link</label>
                                    <input type="text"
                                           name="instagramlink"
                                           class="form-control"
                                           value="<?php echo (!empty($row->instagramlink)?$row->instagramlink:'')?>"
                                           id="title" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="control-group">
                                    <label class="control-label">
                                    Approval    
                                    <span class="f_req"></span></label>
                                    <div class="controls">
                                        <select name="status"
                                                class="form-control selecter"
                                                data-placeholder="Select"
                                                id="status" required>
                                            <option value="1" <?php if(@$row->status == 1) echo "selected";?>>Approved</option>
                                            <option value="0" <?php if(@$row->status == 0) echo "selected";?>>Pending</option>
                                        </select>
                                    </div>
                                </div>
                        <div class="control-group">
                            <label class="control-label">Thumbnail</label>
                            <?php if(!empty($row->thumbnail)) :?>
                                <img src="<?php echo $row->thumbnail?>" class="img-responsive" width="200">
                            <?php endif;?>
                        </div>

                        <!-- <div class="control-group">
                            <label class="control-label">Change Thumbnail</label>
                            <div class="controls">
                                <input type="file" name="change_thumbnail" value="">
                            </div>
                        </div> -->
                    </div>
                </form>
            </div>

        </div>
    </div>

<link href="<?php echo base_url()?>assets2/css/uploader.css" rel="stylesheet" type="text/css"/>
<noscript><link rel="stylesheet" href="<?php echo base_url()?>assets2/css/uploader-noscript.css"></noscript>
<script src="<?php echo base_url()?>assets2/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){

    $("a.clear_feature_image").click(function(){
    	$("input[name='feature_image']").val("");
    });
    $("a.clear_thumbnail_image").click(function(){
    	$("input[name='thumbnail_image']").val("");
    });
    // The trick to keep the editor in the sample quite small
    // unless user specified own height.
   

    $( ".panel-heading span.clickable" ).each(function(index) {
		$(this).on('click', function(e){
		    var $this = $(this);
			if(!$this.hasClass('panel-collapsed')) {
				$this.parents('.panel').find('.panel-body').slideUp();
				$this.addClass('panel-collapsed');
				$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
			} else {
				$this.parents('.panel').find('.panel-body').slideDown();
				$this.removeClass('panel-collapsed');
				$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
			}
		});
	});


	$("a.remove-panel").click(function(){
		// removePanel();
		$(this).parent().parent().remove();
	});
	$("a.more-panel").click(function(){
		// removePanel();
		var panel = $('.panel.panel-primary').first();
		panel.clone('true').insertAfter(panel);
	});

    });

</script>

<script>
function openCustomRoxy(id){
  $('#'+id).dialog({modal:true, width:875,height:600});
}
function closeCustomRoxy(id){
  $('#'+id).dialog('close');
}
</script>
<style type="text/css">

.clickable{
    cursor: pointer;   
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}
</style>