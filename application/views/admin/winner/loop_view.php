<?php if(!empty($results)) : ?>
    <?php $i=1; foreach($results as $row) : ?>
        <tr>
            <td><?= $row->winner_id?></td>
            <td><?= $row->year?></td>
            <td><?= $row->month?></td>
            <td><?= $row->winner_type?></td>
            <td><?= $row->name?></td>
            <td><?= $row->country?></td>
            <td><?= $row->quote?></td>
            <td class="text-center">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="<?= site_url('admin/winner/edit/'.$row->winner_id)?>" class="btn btn-sm btn-outline-success">Edit</a>
                    <a href="javascript:;" data-id="<?= $row->winner_id?>" class="deleterow btn btn-sm btn-outline-danger">Delete</a>
                </div>
            </td>
        </tr>
    <?php endforeach?>
<?php endif?>

<script type="text/javascript">
    $('.deleterow').click(function(){
        var conf = confirm('Are you sure you want to delete this item?');
        if(conf == true){
            var id = $(this).data('id');
            $.post('<?php echo base_url()?>admin/winner/delete',{id : id},function(data){
                if(data == "success"){
                    return true;
                }
            });
            $(this).parents('tr').fadeOut(function(){
                $(this).remove();
            });
        }
    });
</script>