<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Add Winner</h3>
			<div class="card card-outline-secondary">
				<div class="card-block">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="exampleInputEmail1">YEAR</label>
							<select class="form-control" name="year">
								<?php
							    for ($i = 2018; $i >= 2010; $i--) {
							        echo "<option value='$i'>$i</option>";
							    }
							    ?>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">MONTH</label>
							<select class="form-control" name="month">
								<?php
							    for ($i = 0; $i < 12; $i++) {
							        $time = strtotime(sprintf('%d months', $i));   
							        $label = date('F', $time);   
							        $value = date('n', $time);
							        echo "<option value='$value'>$label</option>";
							    }
							    ?>
							</select>
						</div>
						<fieldset class="form-group">
							<legend>PRIZE</legend>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="winner_type" id="optionsRadios1" value="UTAMA" checked>
									UTAMA
								</label>
							</div>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="winner_type" id="optionsRadios2" value="MOBIL">
									MOBIL
								</label>
							</div>
						</fieldset>
						<div class="form-group">
							<label for="exampleInputEmail1">NAME</label>
							<input type="text" class="form-control" name="name">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">COUNTRY</label>
							<input type="text" class="form-control" name="country">
						</div>
						<div class="form-group">
							<label for="exampleTextarea">QUOTE</label>
							<textarea class="form-control" id="exampleTextarea" rows="3" name="quote"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Photo Thumbnail</label>
							<input type="file" class="form-control" name="photo_thumbnail">
							<?php if(!empty($row->photo_thumbnail)) :?>
								<img src="<?php echo base_url()?>static/uploads/<?php echo $row->photo_thumbnail?>" class="img-responsive">
							<?php endif;?>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Photo Big</label>
							<input type="file" class="form-control" name="photo_big">
							<?php if(!empty($row->photo_big)) :?>
								<img src="<?php echo base_url()?>static/uploads/<?php echo $row->photo_big?>" class="img-responsive">
							<?php endif;?>
						</div>

						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?= base_url()?>admin/question" class="btn btn-outline-secondary">Back</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

