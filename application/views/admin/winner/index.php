<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Winner List</h3>
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <a href="<?= base_url()?>admin/winner/add" class="btn btn-outline-primary">Add New</a>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>YEAR</th>
                                <th>MONTH</th>
                                <th>PRIZE</th>
                                <th>NAME</th>
                                <th>COUNTRY</th>
                                <th>QUOTE</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/winner/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

