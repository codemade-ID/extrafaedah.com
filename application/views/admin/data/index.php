<div class="page-content-wrapper">
        <div class="page-content">
            

            <h3 class="page-title">
                <?php echo $title;?>
            </h3>

            <div class="row ">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="tools">
                                
                                <a href="<?php echo base_url()?>admin/data/silkair/export" class="btn "><i class="fa fa-plus"></i> Export</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table datatable table-striped table-bordered table-hover" id="videos">
                                <thead>
                                <tr>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        gender
                                    </th>
                                    <th>
                                        email
                                    </th>
                                    <th>
                                        birthday
                                    </th>
                                    <th>
                                        phone
                                    </th>
                                    <th>
                                        province
                                    </th>

                                    <th>
                                        share fb?
                                    </th>
                                    <th>
                                        ref
                                    </th>
                                    <th>
                                        datetime
                                    </th>
                                </tr>
                                </thead>
                                <?php if(!empty($rows)) :?>
                                    <tbody>
                                    <?php foreach($rows as $row) :?>
                                        <tr>
                                        <td><?php echo $row->first_name.' '.$row->last_name;?></td>
                                        <td class="center"><?php echo $row->gender?></td>
                                        <td class="center"><?php echo $row->email?></td>
                                        <td class="center"><?php echo $row->birthday?></td>
                                        <td class="center"><?php echo $row->phone?></td>
                                        <td class="center"><?php echo $row->province?></td>
                                        <td class="center"><?php echo $row->share_fb?></td>
                                        <td class="center"><?php echo $row->ref?></td>
                                        <td class="center"><?php echo $row->datetime?></td>
                                        
                                        
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                <?php endif;?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>