<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Share Twitter</h3>
            <div class="card card-outline-secondary">
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>Datetime</th>
                                <th>User Email</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php if(!empty($results)) : ?>
                                <?php foreach($results as $row) : ?>
                                    <tr>
                                        <td><?= date('Y-m-d H:i:s',strtotime($row->shared_datetime))?></td>
                                        <td><?= $row->shared_email?></td>
                                    </tr>
                                <?php endforeach?>
                            <?php endif?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>