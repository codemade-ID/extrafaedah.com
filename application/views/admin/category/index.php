<div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">
                Category
            </h3>

            <div class="row ">
                <div class="col-md-4 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-purple-plum">
                                <i class="icon-pencil font-purple-plum"></i>
                                <span class="caption-subject bold uppercase">Category Form</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form" method="post">
                                <div class="form-group">
                                    <label class="control-label">Name</label>

                                    <div class="input-icon right">
                                        <input type="text"
                                               name="category-name"
                                               value=""
                                               required
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Parent</label>
                                    <select name="category-parent" id="category-parent" class="form-control select2"
                                            data-placeholder="Select Parent">
                                        <option></option>
                                        <?php foreach($categories as $category) :?>
                                            <option value="<?php echo $category->id;?>"
                                                    >
                                                <?php echo $category->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <select name="category-status" id="category-status" class="form-control select2"
                                            data-placeholder="Select Status" required>
                                        <option></option>
                                        <option value="0">Draft</option>
                                        <option value="1">Publish</option>
                                        <option value="-1">Delete</option>
                                    </select>
                                </div>

                                <div class="form-actions right">
                                    <input type="hidden"
                                           value=""
                                           name="category-id">
                                    <button type="submit" class="btn blue">
                                        <i class="fa fa-save"></i>
                                        Save
                                    </button>
                                    <button type="button"
                                            onclick="resetForm()"
                                            class="btn default">
                                        <i class="fa fa-trash"></i>
                                        Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-purple-plum">
                                <i class="icon-list font-purple-plum"></i>
                                <span class="caption-subject bold uppercase">Category List</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="categories_container" class="tree-demo"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>


<script src="/assets2/js/tree-view.js" type="text/javascript"></script>
<script>
    var start = 1;

    $(function () {
        $("#categories_container").jstree({
            "core" : {
                "themes" : {
                    "responsive": true
                },
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                        return '/admin/category/json';
                    },
                    'data' : function (node) {
                        return { 'parent' : node.id };
                    }
                }
            }
        }).bind("select_node.jstree", function (e, data) {
            resetForm();
            var node = data.node;
            var detail = node.data;

            $('input[name="category-name"]').val(node.text);
            $('input[name="category-id"]').val(node.id);
            $('#category-status').val(detail.menu_status);
            $('#category-status').trigger('change');
            $('#category-parent').val(node.parent);
            $('#category-parent').trigger('change');
        }).bind("loaded.jstree", function (e, data) {
            $(this).jstree("open_all");
        });

        $('.select2').select2();
    });

    function resetForm() {
        $('input[name="category-name"]').val('');
        $('input[name="category-id"]').val('');
        $('#category-status').select2('val', '')
        $('#category-parent').select2('val', '')
    }
</script>