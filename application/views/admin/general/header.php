<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url()?>assets/img/favicon.ico">
    <title>Millionaire</title>
    <link href="<?= base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/bootstrap-table.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">

    <script type="text/javascript" src="<?= base_url()?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url()?>assets/js/tether.min.js"></script>
    <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>

    <script src="<?= base_url()?>assets/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dataTables').DataTable({
                "order": [[ 0, "asc" ]]
            } );
        });
    </script>

    <script src="<?= base_url()?>assets/js/bootstrap-table.min.js"></script>
    <script>
        var $table = $('#bootstrapTable');
    </script>

</head>

<body>

<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-primary">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">Changi Millionaire</a>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?=($this->uri->segment(2) == 'winner')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/winner">Winner <?=($this->uri->segment(2) == 'winner')?'<span class="sr-only">(current)</span>':'';?></a>
            </li>
            <li class="nav-item <?=($this->uri->segment(2) == 'newsletter')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/newsletter">Newsletter <?=($this->uri->segment(2) == 'result')?'<span class="sr-only">(current)</span>':'';?></a>
            </li>
            <li class="nav-item <?=($this->uri->segment(2) == 'games')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/games">Games <?=($this->uri->segment(2) == 'result')?'<span class="sr-only">(current)</span>':'';?></a>
            </li>
            <!-- 
            <li class="nav-item <?=($this->uri->segment(2) == 'answer')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/answer">Answer <?=($this->uri->segment(2) == 'answer')?'<span class="sr-only">(current)</span>':'';?></a>
            </li>
            <li class="nav-item <?=($this->uri->segment(3) == 'facebook')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/share/facebook">Share Facebook<?=($this->uri->segment(3) == 'facebook')?'<span class="sr-only">(current)</span>':'';?></a>
            </li>
            <li class="nav-item <?=($this->uri->segment(3) == 'twitter')?'active':'';?>">
                <a class="nav-link" href="<?= base_url()?>admin/share/twitter">Share Twitter<?=($this->uri->segment(3) == 'twitter')?'<span class="sr-only">(current)</span>':'';?></a>
            </li> -->
        </ul>
        <a class="btn btn-outline-secondary" href="<?= base_url()?>" target="_blank">Go to site</a>
    </div>
</nav>
