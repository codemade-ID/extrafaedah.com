<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Question List</h3>
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <a href="<?= base_url()?>admin/question/add" class="btn btn-outline-primary">Add New</a>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Question</th>
                                <th>Opt A</th>
                                <th>Opt B</th>
                                <th>Opt C</th>
                                <th>Answer</th>
                                <th>Hint</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/question/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

