<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">Edit Question</h3>
			<div class="card card-outline-secondary">
				<div class="card-block">
					<form method="post">
						<div class="form-group">
							<label for="exampleInputEmail1">Title</label>
							<input type="text" class="form-control" name="question_title" value="<?=$row->question_title;?>">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Option A</label>
							<input type="text" class="form-control" name="option_a" value="<?=$row->option_a;?>">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Option B</label>
							<input type="text" class="form-control" name="option_b" value="<?=$row->option_b;?>">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Option C</label>
							<input type="text" class="form-control" name="option_c" value="<?=$row->option_c;?>">
						</div>
						<fieldset class="form-group">
							<legend>Answer</legend>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="question_answer" id="optionsRadios1" value="a" <?=($row->question_answer == 'a')?'checked':'';?>>
									A
								</label>
							</div>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="question_answer" id="optionsRadios2" value="b" <?=($row->question_answer == 'b')?'checked':'';?>>
									B
								</label>
							</div>
							<div class="form-check disabled">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="question_answer" id="optionsRadios3" value="c" <?=($row->question_answer == 'c')?'checked':'';?>>
									C
								</label>
							</div>
						</fieldset>
						<div class="form-group">
							<label for="exampleTextarea">Hint</label>
							<textarea class="form-control" id="exampleTextarea" rows="3" name="question_hint"><?=$row->question_hint;?></textarea>
						</div>

						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?= base_url()?>admin/question" class="btn btn-outline-secondary">Back</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

