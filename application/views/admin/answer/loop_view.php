<?php if(!empty($results)) : ?>
    <?php $i=1; foreach($results as $row) : ?>
        <tr>
            <td><?= $row->firstname?></td>
            <td><?= $row->lastname?></td>
            <td><?= $row->email?></td>
            <td><?= $row->question_title?></td>
            <td><?= $row->user_answer?></td>
            <td><?= $row->status_answer?></td>
            <td><?= date('d M Y H:i:s',strtotime($row->datetime))?></td>
        </tr>
    <?php endforeach?>
<?php endif?>