<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Quiz Answer</h3>
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <a href="<?php echo base_url();?>admin/answer/export" class="btn btn-outline-danger">Export</a>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Question</th>
                                <th>User Answer</th>
                                <th>Status Answer</th>
                                <th>Date Answer</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/answer/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>