<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 7/3/17
 * Time: 10:36
 */

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=Answer.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

?>

<table border="1" width="100%">
    <thead>
    <tr>
        <th>No</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Question</th>
        <th>User Answer</th>
        <th>Status Answer</th>
        <th>Date Answer</th>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($results)) : ?>
        <?php $i=1; foreach($results as $row) : ?>
            <tr>
                <td><?=$i;?></td>
                <td><?= $row->firstname?></td>
                <td><?= $row->lastname?></td>
                <td><?= $row->email?></td>
                <td><?= $row->question_title?></td>
                <td><?= $row->user_answer?></td>
                <td><?= $row->status_answer?></td>
                <td><?= date('d M Y H:i:s',strtotime($row->datetime))?></td>
            </tr>
        <?php $i++; endforeach?>
    <?php endif?>
    </tbody>
</table>