<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Quiz Answer &nbsp;&nbsp; <a href="<?php echo base_url();?>admin/answer/export" class="btn btn-outline-danger">Export</a></h3>

            <table id="bootstrapTable"
                   data-toggle="table"
                   data-url="<?=base_url()?>admin/answer/get_data"
                   data-height="600"
                   data-side-pagination="server"
                   data-pagination="true"
                   data-page-list="[5, 10, 20, 50, 100, 200]"
                   data-search="true"
                   data-sort-name="datetime"
                   data-sort-order="desc"
                   class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th data-field="datetime" style="width: 250px" data-sortable="true">Datetime</th>
                    <th data-field="firstname" data-sortable="true">First Name</th>
                    <th data-field="lastname" data-sortable="true">Last Name</th>
                    <th data-field="email" data-sortable="true">Email</th>
                    <th data-field="question_title" data-sortable="true">Question</th>
                    <th data-field="user_answer" data-sortable="true">User Answer</th>
                    <th data-field="status_answer" data-sortable="true">Status Answer</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>