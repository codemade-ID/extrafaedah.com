<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 7/3/17
 * Time: 10:36
 */

/*header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");*/

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=Results.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

?>

<table border="1" width="100%">
    <thead>
    <tr>
        <th>No</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Session ID</th>
        <th>Poin</th>
        <th>Date</th>
        <th>Agreement</th>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($results)) : ?>
        <?php $i=1; foreach($results as $row) : ?>
            <tr>
                <td><?=$i;?></td>
                <td><?= $row->firstname?></td>
                <td><?= $row->lastname?></td>
                <td><?= $row->email?></td>
                <td><?= $row->session_id?></td>
                <td><?= $row->right_answer?></td>
                <td><?= date('d M Y H:i',strtotime($row->datetime))?></td>
                <td class="text-center"><?= ($row->agreement == 1)?'Yes':'No'?></td>
            </tr>
            <?php $i++; endforeach?>
    <?php endif?>
    </tbody>
</table>