<?php if(!empty($users)) : ?>
    <?php $i=1; foreach($users as $row) : ?>
        <tr>
            <td style="width: 250px"><?= date('d M Y H:i:s',strtotime($row->datetime))?></td>
            <td><?= $row->firstname?></td>
            <td><?= $row->lastname?></td>
            <td><?= $row->email?></td>
            <td><?= $row->session_id?></td>
            <td><?= ($row->right_answer > 5)?5:$row->right_answer;?></td>
            <td class="text-center"><?= ($row->agreement == 1)?'Yes':'No'?></td>
        </tr>
    <?php endforeach?>
<?php endif?>
