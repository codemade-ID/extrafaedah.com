<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Quiz Results</h3>
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <a href="<?php echo base_url();?>admin/result/export" class="btn btn-outline-danger">Export</a>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTablesResults">
                            <thead>
                            <tr>
                                <th style="width: 250px">Date</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Session ID</th>
                                <th>Poin</th>
                                <th>Agreement</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/result/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>