<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Games</h3>
            <a href="<?php echo current_url()?>/export" class="btn btn-warning">Export to Excel</a><br /><br />
            <div class="card card-outline-secondary">
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>GENDER</th>
                                <th>BIRTHDAY</th>
                                <th>EMAIL</th>
                                <th>PHONE</th>
                                <th>PROVINCE</th>
                                <th>SCORE</th>
                                <th>NEWSLETTER</th>
                                <th>SHARE FB</th>
                                <th>SHARE TW</th>
                                <th>REF</th>
                                <th>DATETIME</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/games/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

