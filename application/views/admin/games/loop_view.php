<?php if(!empty($results)) : ?>
    <?php $i=1; foreach($results as $row) : ?>
        <tr>
            <td><?= $row->id?></td>
            <td><?= $row->first_name.' '.$row->last_name ?></td>
            <td><?= $row->gender?></td>
            <td><?= $row->birthday?></td>
            <td><?= $row->email?></td>
            <td><?= $row->phone?></td>
            <td><?= $row->province?></td>
            <td><?= $row->score?></td>
            <td><?= $row->newsletter?></td>
            <td><?= $row->share_fb?></td>
            <td><?= $row->share_tw?></td>
            <td><?= $row->ref?></td>
            <td><?= $row->datetime?></td>
            <td class="text-center">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="javascript:;" data-id="<?= $row->id?>" class="deleterow btn btn-sm btn-outline-danger">Delete</a>
                </div>
            </td>
        </tr>
    <?php endforeach?>
<?php endif?>

<script type="text/javascript">
    $('.deleterow').click(function(){
        var conf = confirm('Are you sure you want to delete this item?');
        if(conf == true){
            var id = $(this).data('id');
            $.post('<?php echo base_url()?>admin/games/delete',{id : id},function(data){
                if(data == "success"){
                    return true;
                }
            });
            $(this).parents('tr').fadeOut(function(){
                $(this).remove();
            });
        }
    });
</script>