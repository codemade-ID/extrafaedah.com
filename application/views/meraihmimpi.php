<section class="bg-fill pad-b" style="position: relative;">
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/meraihmimpi-header-m.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/meraihmimpi-header.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
	<section id="section04" class="demo" style="text-align: center;">
		Raih Mimpimu sekarang dan dapatkan Hadiahnya
		<div style="width: 300px; margin: 0 auto; text-align: center;">
			<a href="#tos" class="scrollTos"><span></span></a>
		</div>
	  
	</section>
</section>
<section class="pad-b" id="tos">
	<div class="container">
		<div class="text-center">
			<p class="mimpilead">Meraih Mimpi #SmartfrenWOW (Via Vallen, Rizky Febian, Jaz Hayat)</p>
		</div>
		<div class="text-center mar-30 mar-l mar-r">
			<img src="<?php echo base_url()?>assets/img/meraihmimpi-yukikutan.png" width="60%" class="mimpileadimg">
		</div>
	</div>
</section>
<section>
	<div class="resp-container">
		<iframe class="resp-iframe" width="800" height="642" src="https://www.youtube.com/embed/Z8GVhS1K950?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ></iframe>
	</div>
</section>
<section class="">
	<div class="container">
		<p style="background: #fff;font-size: 34px; padding: 5px 10px;font-family: TruenoBd;margin: 0 auto; text-align: center;color: #000;margin-bottom: 30px;" class="width-percent-80 mimpilead">
			<a href="javascript:;"  data-toggle="modal" data-target="#lirik">
			KLIK DI SINI UNTUK LIRIK MERAIH MIMPI</a></p>
		</div>
		<p class="text-center font-26 TruenoBd uppercase mimpileada">Yuk Ikut Cover Theme Song Meraih Mimpi #smartfrenWOW</p>
		<div class="row">
			<div class="col-md-6">
				<?php if($this->ua->is_mobile()) :?>
					<img src="<?php echo base_url()?>assets/img/merainmimpi-caranyagini-m.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
				<?php else:?>
					<img src="<?php echo base_url()?>assets/img/merainmimpi-caranyagini.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
				<?php endif;?>
			</div>
			<div class="col-md-6">
				<?php if($this->ua->is_mobile()) :?>
					<img src="<?php echo base_url()?>assets/img/meraihmimpi-hadiah-m.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
				<?php else:?>
					<img src="<?php echo base_url()?>assets/img/meraihmimpi-hadiah.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
				<?php endif;?>
			</div>
		</div>
	</div>
</section>
<?php /*
<section class="">
	<div class="container">
		<p class="text-center font-26 TruenoBd uppercase mimpileada pad-20">AYO IKUTI ROAD SHOW WOW MUSIK KARNIVAL</p>
		<div class="row multiple-items h text-center" id="lineup" style="background: #fff; padding: 0 20px ;">
			<div class="item">
				<div class="row">
					<div class="col-md-6">
						<p class="font-50 text-left color-black TruenoBd location-city">JAKARTA</p>
						<p class="font-30 text-left color-black uppercase TruenoBd location">Gedung Sarinah, 7 November</p>
						<div class="text-left font-20 uppercase TruenoBd color-orange" style="display: block; clear: both;margin: 20px 0 0;">
							<a href="javascript:;" style="color: #ff1659">Karnival</a>
							<a href="javascript:;" style="color: #ff1659">Pop UP Baazaar</a>
							<a href="javascript:;" style="color: #ff1659">Musik Performance</a>
							<a href="javascript:;" style="color: #ff1659">dll</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=jakarta&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="row">
					<div class="col-md-6">
						<p class="font-50 text-left color-black TruenoBd location-city">JAKARTA</p>
						<p class="font-30 text-left color-black uppercase TruenoBd location">Gedung Sarinah, 7 November</p>
						<div class="text-left font-20 uppercase TruenoBd color-orange" style="display: block; clear: both;margin: 20px 0 0;">
							<a href="javascript:;" style="color: #ff1659">Karnival</a>
							<a href="javascript:;" style="color: #ff1659">Pop UP Baazaar</a>
							<a href="javascript:;" style="color: #ff1659">Musik Performance</a>
							<a href="javascript:;" style="color: #ff1659">dll</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=jakarta&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.multiple-items').slick({
				  dots: false,
				  infinite: false,
				  speed: 300,
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        infinite: true,
				        dots: true
				      }
				    },
				    {
				      breakpoint: 600,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    }
				    // You can unslick at a given breakpoint now by adding:
				    // settings: "unslick"
				    // instead of a settings object
				  ]
			});
		})
		
	</script>
</section>
*/ ?>
<!-- Modal -->
<div class="modal fade" id="lirik" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
      	<h3 class="uppercase" style="color: #FF1659">Lirik Meraih Mimpi</h3>
        <p>Setiap saat setiap waktu</p>
		<p>Untung tak jua menentu</p>
		<p>Apalagi yang kutunggu</p>
		<p>saat ini kupastikan</p>
		<p>&nbsp;</p>
		<p>Kita semua pake Smartfren</p>
		<p>Berbagi suka dan senang</p>
		<p>Jelajah luas dan cepat</p>
		<p>Semua kau persembahkan</p>
		<p>*pakai smartfren *</p>
		<p>*dapat poin*</p>
		<p>Hanya itu, poin itu..</p>
		<p>&nbsp;</p>
		<p>Tetap fokus kita kejar meraih mimpi.</p>
		<p>(Tetap fokus kita kejar bersama Smartfren)</p>
		<p>Terus fokus, kejar poin. Hanya itu, poin itu</p>
		<p>Tetap fokus kita dapat kan kemenangan.</p>
		<p>(Tetap fokus kita dapat meraih mimpi.)</p>
		<p>&nbsp;</p>
		<p>Wow yo ayo, yo ayo wow yo ayo. Yo ayo wow yo ayo.. Wow wow wow.o.o.oooWow yo ayo, yo ayo wow yo ayo. Yo ayo wow yo ayo..</p>
		<p>kita datang kita menang kita Smartfren..&nbsp;</p>
		<p>&nbsp;</p>
		<p>Kalau menang nasib baik</p>
		<p>&nbsp;Bikin semua jadi asyik</p>
		<p>Semua dapat oportuniti</p>
		<p>Kita terus coba lagi</p>
		<p>&nbsp;</p>
		<p>Kalau menang *nasib *baik</p>
		<p>Bikin semua jadi *asyik *</p>
		<p>Semua dapat oportuniti</p>
		<p>Kita terus coba lagi</p>
		<p>&nbsp;</p>
		<p>Smartfren wow</p>
      </div>
    </div>
  </div>
</div>