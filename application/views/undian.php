<section class="bg-white">
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/banner-top-undian-m.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/banner-top-undian.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
	<!-- <p class="text-center" id="btn-pemenang" style="display: none;">
		<a href="<?php echo base_url()?>pemenang" class="btn btn-back" style="font-size: 18px;background: #FF1659 ">PEMENANG UNDIAN SMARTFREN WOW</a>
	</p> -->
	<a href="<?php echo base_url()?>pemenang">
		<img src="<?php echo base_url()?>assets/img/CONGRATS-01.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	</a>
	<script type="text/javascript">
		let countDownDates = new Date("Oct 7, 2019 00:00:00").getTime();
		let y = setInterval(function () {
          // Get today's date and time
          let nows = new Date().getTime();

          // Find the distance between now and the count down date
          let distance_ = countDownDates - nows;

          if (distance_ < 0) {
            clearInterval(y);
            $('#btn-pemenang').show();
          }
        }, 1000);
	</script>
	<section id="time" style="display: block; background: #FF1659" >
		<div class="container">
			<div>
				<h2 class="text-center">Pengundian Pemenang Tahap 1 Akan Segera Dimulai!</h2>
<p class="text-center">Ikuti proses pengundian pemenang tahap 1 di social media smartfren</p>

				<!-- <img src="assets/img/get-ready-for-concert.png" class="img-responsive center"> -->
				<div id="demo" class="board">
					<div class="row">
						<div class="col-md-4 text-center" id="days"></div>
						<div class="col-md-4 text-center" id="hours"></div>
						<div class="col-md-4 text-center" id="minutes"></div>
					</div>
				</div>
				<script>
			        // Set the date we're counting down to
			        let countDownDate = new Date("Oct 4, 2019 15:00:00").getTime();

			        // Update the count down every 1 second
			        let x = setInterval(function () {

			          // Get today's date and time
			          let now = new Date().getTime();

			          // Find the distance between now and the count down date
			          let distance = countDownDate - now;

			          // Time calculations for days, hours, minutes and seconds
			          let days = Math.floor(distance / (1000 * 60 * 60 * 24));
			          days > 9 ? days : days = '0' + days;
			          let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			          hours > 9 ? hours : hours = '0' + hours;
			          let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			          minutes > 9 ? minutes : minutes = '0' + minutes;
			          let seconds = Math.floor((distance % (1000 * 60)) / 1000);

			          // Display the result in the element with id="demo"
			          // document.getElementById("demo").innerHTML = days + " " + hours + " " + minutes;
			          document.getElementById("days").innerHTML = days;
			          document.getElementById("hours").innerHTML = hours;
			          document.getElementById("minutes").innerHTML = minutes;


			          // If the count down is finished, write some text

			          if (distance < 0) {
			            clearInterval(x);
			            $('#time').hide();
			            // document.getElementById("demo").innerHTML = "WOW";
			          }
			        }, 1000);
			      </script>

			</div>
		</div>
	</section>
	<img src="<?php echo base_url()?>assets/img/carousel-355.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<div class="container">
		<p class="text-center color-hitam font-24 mar-30 mar-l mar-r TruenoBd">Hadiah Undian Smartfren WOW Tahap I</p>
		<div class="row">
			<?php foreach($hadiah as $h) :?>
				<div class="col-md-4 text-center">
					<div class="lu-img">
						<img src="<?php echo $h['image']?>" class="img-responsive">
					</div>
				</div>
			<?php endforeach;?>
		</div>
	</div>
	<div class="container">
		<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          FAQ PROGRAM UNDIAN SMARTFREN WOW
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol{margin-left: 30px;}
		    </style>
		    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p>Q: Bagaimana cara bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan akan mendapatkan 1 Kupon Undian dengan menukarkan 100 SmartPoin pada aplikasi MySmartfren.</p>
					<!--<ol style="list-style-type: lower-alpha;">
					<li>Aktifasi Kartu Perdana baru minimal Rp.30.000,- akan mendapatkan 1 Kupon Undian;</li>
					<li>Pertama kali mengunduh dan registrasi aplikasi MySmartfren akan mendapatkan 2 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di luar aplikasi MySmartfren akan mendapatkan 1 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di aplikasi MySmartfren akan mendapatkan 3 Kupon Undian.</li>
					</ol>-->
					<p>&nbsp;</p>
					<p>Q: Siapa saja yang bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Semua pelanggan Smartfren baik Pascabayar maupun Prabayar dapat mengikuti Program Undian selama memiliki SmartPoin pada aplikasi MySmartfren</p>
					<--<ol style="list-style-type: lower-alpha;">
					<li>Transaksi auto renewal;</li>
					<!-- <li>Isi Ulang paket data EVO untuk Modem WiFi;</li> -->
					<li>Aktifasi Kartu Perdana baru dibawah Rp.30.000,-;</li>
					<li>Pembelian paket layanan Smartfren dibawah Rp.30,000,-</li>
					</ol>-->
					<p>&nbsp;</p>
					<p>Q: Berapa banyak pemenang dan apa saja hadiah pada Program Undian Smartfren WOW ini?</p>
					<p>A: Jumlah pemenang pada Program Undian Smartfren WOW adalah 1,000 Pemenang. Dengan Hadiah Utama berupa 1 Rumah bernilai milyaran, 1 Mobil Innova Tipe G A/T, 3 Vespa tipe LS 125 dan Hadiah Undian lainnya berupa 5 iPhone XS (64GB), 10 Paket Liburan Bali (3H2M), 60 Samsung A50, 120 Bluetooth Swarovski, 300 Voucher Pulsa senilai 500 ribu dan Voucher Belanja senilai 250 ribu.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang dimaksud dengan Kupon Undian?</p>
					<p>A: Kupon adalah bentuk kepemilikan nomor undian yang akan digunakan untuk pengundian hadiah.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan periode Program Undian Smartfren WOW berlangsung?</p>
					<p>A: Periode Undian Smartfren WOW berlaku mulai 10 September 2019 &ndash; 31 Oktober 2019. Program Undian Smartfren WOW dibagi menjadi 2 Tahapan:</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Tahapan I : 10 September 2019 &ndash; 30 September 2019</li>
					<li>Tahapan II : 01 Oktober 2019 &ndash; 31 Oktober 2019</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Kapan Saya menerima Kupon Undian setelah melakukan pengaktifan nomor/pembelian paket?</p>
					<p>A: Kupon Undian akan diterima paling lama 3 jam.</p>
					<p>&nbsp;</p>
					<p>Q: Dimana Saya dapat melihat Kupon Undian?</p>
					<p>A: Pelanggan dapat melihat Kupon Undian di halaman Smartpoint lalu klik &ldquo;Voucher Saya&rdquo; pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian tersebut memiliki masa kadaluarsa?</p>
					<p>A: Ya,&nbsp; Kupon memiliki masa kadaluarsa yang akan tertera pada masing-masing kupon sesuai dengan periode Program Undian Smartfren WOW.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian yang sudah kadaluarsa bisa diikutsertakan kembali pada tahapan berikutnya?</p>
					<p>A: Tidak, Kupon Undian yang sudah kadaluarsa akan hangus. Pelanggan harus melakukan transaksi kembali untuk mendapatkan Kupon Undian pada tahapan berikutnya.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa dipindahkan atau dibagikan ke nomor Smartfren lain?</p>
					<p>A: Tidak, Kupon Undian yang Anda miliki tidak bisa dipindahkan atau dibagikan ke nomor Smartfren lain.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa saya tukarkan dengan uang?</p>
					<p>A: Kupon Undian tidak dapat ditukarkan dengan uang. Kupon Undian hanya digunakan sebagai bukti kepemilikan nomor undian unik yang tertera pada setiap Kupon Undian yang ada pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang terjadi jika saya mengganti nomor Smartfren saya?</p>
					<p>A: Kupon Undian yang terdapat pada nomor sebelumnya akan hangus apabila nomor Anda sudah tidak aktif lagi.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah 1 pelanggan dapat memenangkan lebih dari 1 hadiah?</p>
					<p>A: Untuk Hadiah Utama pelanggan hanya mendapatkan 1 jenis hadiah selama periode Program Undian Smartfren WOW berlangsung. Namun, pelanggan berhak untuk mendapatkan kesempatan untuk memenangkan hadiah undian lainnya di setiap tahapan.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan pengundian akan dilakukan?</p>
					<p>A: Pengundian akan dilakukan setiap bulan pada tanggal 7 setiap bulannya atau selambat-lambatnya 14 hari setelah periode per tahapan selesai.</p>
					<p>&nbsp;</p>
					<p>Q: Bagaimana cara mengetahui pemenang Program Undian Smartfren WOW?</p>
					<p>A: Untuk pemenang Hadiah Utama, pelanggan akan dihubungi langsung pada saat pengundian berlangsung, sedangkan untuk pemenang hadiah undian lainnya bisa akan diinformasikan pada aplikasi MySmartfren masing-masing pemenang dan website Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Saya dikenakan biaya untuk mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan tidak akan dikenakan biaya untuk mengikuti Program Undian Smartfren WOW, hanya untuk pajak Hadiah Utama ditanggung oleh masing-masing pemenang sedangkan hadiah undian lainnya akan ditanggung oleh Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah hadiah yang saya menangkan bisa ditukar atau diuangkan?</p>
					<p>A: Hadiah yang sudah dimenangkan tidak dapat ditukar ataupun diuangkan, kecuali untuk Hadiah Utama Rumah dengan mengikuti syarat &amp; ketentuan yang berlaku.</p>
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		          S&K PROGRAM UNDIAN SMARTFREN WOW
		        </button>
		      </h2>
		    </div>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p>SYARAT &amp; KETENTUAN PROGRAM UNDIAN SMARTFREN WOW</p>
					<ol>
					<li>Program Undian Smartfren WOW berlaku hanya untuk Pelanggan Smartfren Pascabayar dan Prabayar dengan nomor Smartfren yang berstatus aktif.</li>
					<li>Pelanggan akan menerima Kupon Undian sebagai bukti peserta Program Undian Smartfren WOW.</li>
					<li>Periode Program Undian Smartfren WOW:
					<ol>
					<li>Tahapan I : 10 September 2019 &ndash; 30 September 2019</li>
					<li>Tahapan II : 01 Oktober 2019 &ndash; 31 Oktober 2019</li>
					</ol>
					</li>
					</ol>
					<ol start="4">
					<li>Pelanggan akan mendapatkan Kupon Undian dengan cara:
					<ol style="list-style-type: lower-alpha;">
					<li>Melakukan aktifasi Kartu Perdana Smartfren minimal Rp.30.000,- akan mendapatkan&nbsp; 1 Kupon Undian;</li>
					<li>Mengunduh aplikasi MySmartfren akan mendapatkan 2 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren minimal Rp.30.000,- diluar aplikasi MySmartfren akan mendapatkan 1 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren minimal Rp. 30.000,- di aplikasi MySmartfren akan mendapatkan 3 Kupon Undian;</li>
					</ol>
					</li>
					<li>Pelanggan dapat melakukan pengecekan Kupon Undian pada halaman &ldquo;Voucher Saya&rdquo; pada aplikasi MySmartfren.</li>
					<li>Program Undian Smartfren WOW tidak berlaku untuk Karyawan Smartfren ataupun keluarga inti karyawan.</li>
					<li>SMARTFREN menyediakan 1,000 pemenang setiap tahapannya dengan hadiah sebagai berikut;
					<ol style="list-style-type: lower-alpha;">
					<li>Hadiah Utama Undian berupa 1 Rumah area Jabodetabek, 1 Mobil Innova Tipe G A/T, 3 Vespa tipe LS 125;</li>
					<li>Hadiah Undian lainnya berupa 5 iPhone XS (64GB), 10 Paket Liburan Bali (3H2M), 60 Samsung A50, 120 Bluetooth Swarovski, 300 Voucher Pulsa senilai 500 ribu dan Voucher Belanja senilai 250 ribu.</li>
					</ol>
					</li>
					<li>Pengumuman pemenang hadiah utama undian akan dilakukan langsung melalui panggilan telepon.</li>
					<li>Pengumuman pemenang hadiah undian akan dilakukan melalui aplikasi MySmartfren untuk selanjutnya akan mengikuti ketentuan dan prosedur yang berlaku di SMARTFREN. Pemenang akan menerima notifikasi untuk melengkapi data diri pada aplikasi MySmartfren sebelum melakukan pengambilan/penebusan hadiah.</li>
					<li>Pengumuman pemenang yang terpilih dan telah tervalidasi akan dipublikasikan melalui website resmi SMARTFREN (<a href="http://www.smartfren.com">www.smartfren.com</a>/WOW) dan media massa (TV/Koran) selambat-lambatnya 30 (tiga puluh) hari setelah tanggal pengundian.</li>
					<li>Selain Hadiah Utama Undian berupa rumah, SMARTFREN akan memberikan kode voucher sebagai bentuk penebusan hadiah. Kode voucher dikeluarkan oleh Blibli.com, selaku pihak yang ditunjuk oleh SMARTFREN untuk bertanggungjawab terkait pengadaan dan pengiriman hadiah kepada pemenang resmi Program Undian Smartfren WOW.</li>
					<li>Seluruh hadiah, kecuali Hadiah Utama Undian rumah, akan dikirim selambat-lambatnya 30 (tigapuluh) hari setelah kelengkapan data pemenang yang valid dan benar diterima oleh SMARTFREN.</li>
					<li>Waktu pengiriman untuk hadiah mobil dan motor akan menyesuaikan dengan kesiapan barang dan kelengkapan data lengkap pemenang.</li>
					<li>Untuk hadiah berupa rumah yang akan diberikan berbeda dengan bentuk rumah yang tertera pada materi komunikasi seperti Billboard, Iklan TV dan channel lainnya.</li>
					<li>Untuk hadiah berupa mobil dan motor akan diberikan dalam kondisi off the road. Biaya balik nama, pengurusan surat-surat, dan/atau biaya lainnya yang mungkin timbul, ditanggung sepenuhnya oleh pemenang.</li>
					<li>Pajak Hadiah Utama Undian ditanggung oleh Pemenang.</li>
					<li>Pajak Hadiah Undian lainnya ditanggung oleh SMARTFREN.</li>
					<li>Hadiah tidak dapat ditukarkan dengan produk lain atau uang tunai, kecuali Hadiah Utama Rumah dapat di uangkan mengikuti syarat &amp; ketentuan yang berlaku.</li>
					<li>Pemenang tidak dapat memilih warna dan/atau tipe hadiah yang diberikan. Warna dan/atau tipe hadiah yang diberikan akan disesuaikan dengan ketersediaan stock yang ada.</li>
					<li>Jika Peserta dan/atau pihak ketiga dicurigai melakukan penipuan dan/atau kecurangan apapun bentuknya, maka SMARTFREN berhak untuk meminta pertanggungjawaban kepada Peserta atau orang yang diyakini bertanggungjawab dan/atau terkait dengan kegiatan tersebut.</li>
					<li>SMARTFREN tidak bertanggungjawab jika terjadi kesalahan/kelalaian yang berada di luar pengawasan SMARTFREN, namun tidak terbatas pada jaringan internet, sistem informasi, pengiriman yang dilakukan dengan menggunakan jasa pihak ketiga, dan kesalahan peserta dalam memberikan informasi, dan kesalahan/kelalaian pihak lainnya.</li>
					<li>Setiap Peserta menjamin bahwa setiap data-data dan informasi yang diberikan kepada SMARTFREN berdasarkan syarat dan ketentuan ini adalah benar dan valid.</li>
					<li>Untuk informasi lebih lanjut terkait Promo Undian WOW, pelanggan dapat membaca pada website resmi SMARTFREN di&nbsp;<a href="http://www.smartfren.com">www.smartfren.com</a>/WOW.</li>
					<li>Hati-hati terhadap motif/modus penipuan atau permintaan data-data oleh pihak yang tidak bertanggung jawab yang mengatasnamakan SMARTFREN dan/atau Promo Undian WOW. Promo Undian WOW TIDAK DIPUNGUT BIAYA APAPUN, kecuali untuk hadiah utama undian dimana pajak dan biaya-biaya terkait dengan pengurusan dokumen-dokumen akan ditanggung sepenuhnya oleh pemenang. Untuk menghindari penipuan, setiap komunikasi melalui telepon kepada Peserta dan/atau pemenang akan dilakukan melalui nomor call center SMARTFREN di nomor 777, 888 atau 08888188888 &nbsp;atau jika melalui email, maka email tersebut akan dikirimkan melalui alamat email wow@smartfren.com. Jika Peserta dan/atau pemenang menerima telepon atau email dari nomor atau email selain dimaksud di atas yang mengatasnamakan SMARTFREN dan/atau Promo Undian WOW, maka Peserta diharapkan menghubungi call center SMARTFREN untuk konfirmasi.</li>
					</ol>
					<p>&nbsp;</p>
		      </div>
		    </div>
		  </div>
		  
		</div>
	</div>
</section>
<section>
	<div class="container">
		<p class="text-center font-24 mar-30 mar-l mar-r TruenoBd">Prosesi Pengundian Tahap 1 #SmartfrenWOW</p>
		<div class="row multiple-items h text-center" id="lineup">
			<?php foreach($gallery as $lu) :?>
				<div class="item">
					<div class="lu-img">
						<a href="<?php echo $lu['image']?>" class="fancybox">
							<img src="<?php echo $lu['image']?>" class="img-responsive">
						</a>
					</div>
				</div>
			<?php endforeach;?>
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
	<script type="text/javascript">
			$(document).ready(function(){
				$("a.fancybox").fancybox();
				$('.multiple-items').slick({
					  dots: false,
					  infinite: false,
					  speed: 300,
					  slidesToShow: 3,
					  slidesToScroll: 1,
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 1,
					        infinite: true,
					        dots: true
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    },
					    {
					      breakpoint: 480,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    }
					    // You can unslick at a given breakpoint now by adding:
					    // settings: "unslick"
					    // instead of a settings object
					  ]
				});
			})
			
		</script>
</section>
<section class="bg-white">
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="resp-container">
				<iframe class="resp-iframe" width="100%" height="642" src="https://www.youtube.com/embed/aMF6YGvhlMw?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="row mar-10 mar-r mar-l thumb-vid">
				<div class="col-md-2"></div>
				<div class="col-md-4 text-center">
					<a href="javascript:;" class="tvc" rel="aMF6YGvhlMw">
						<img src="<?php echo base_url()?>assets/img/thumb-vid1.png">
					</a>
				</div>
				<div class="col-md-4 text-center">
					<a href="javascript:;" class="tvc" rel="RavKKmb1quc">
						<img src="<?php echo base_url()?>assets/img/thumb-vid2.png">
					</a>
				</div>
				<!-- <div class="col-md-4 text-center">
					<a href="javascript:;" class="tvc" rel="">
						<img src="<?php echo base_url()?>assets/img/raffle1.png">
					</a>
				</div> -->
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a.tvc').click(function(){
				var rel = $(this).attr('rel');
				if(rel == ''){
					return false;
				}
				$('iframe.resp-iframe').attr('src','https://www.youtube.com/embed/'+rel+'?autoplay=1');
			});
		});
	</script>
</section>
<section class="bg-red">
	<p class="text-center font-24 mar-30 mar-l mar-r TruenoBd">Cara Mudah Ikutan Undian Smartfren WOW</p>
	<div class="row pad-30 text-center">
		<div class="col-md-4">
			<img src="<?php base_url()?>assets/img/ikutan-1.png" class="img-responsive">
			<p class="lu-name">Aktifkan Kartu Perdana <br /> Smartfren min 30RB</p>
		</div>
		<div class="col-md-4">
			<a href="https://linktr.ee/Smartfren" target="_blank">
				<img src="<?php base_url()?>assets/img/ikutan-2.png" class="img-responsive">
			</a>
			<p class="lu-name">Download <br /> Aplikasi MySmartfren</p>
		</div>
		<div class="col-md-4">
			<img src="<?php base_url()?>assets/img/ikutan-3.png" class="img-responsive">
			<p class="lu-name">Beli Paket Layanan <br /> Smartfren min 30RB</p>
		</div>
	</div>
</section>
<div class="box-activity" id="layanantar">
		<div class="container">
			<!-- <h3 class="color-hitam  font-26">Layanan Antar</h3> -->
	       	<div class="row mar-20 mar-l mar-r">
				<div class="col-sm-12">
					<ul class="commerce">
						<!-- <li class="text-center" style="list-style: none;">
							Produk dikirim langsung dari Smartfren <br /> <br />
							<a href="https://www.smartfren.com/id/delivery-order/"  target="_blank" class="btn btn-warning font-22 color-white">Klik Di Sini</a> <br /> <br />
						</li> -->
						<li class="text-center"style="list-style: none;">
							Beli Produk Smartfren Sekarang! <br /> <br />
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										 <div class="col-md-1"></div> 
										<div class="col-md-2">
											<a href="https://www.tokopedia.com/smartfren?nref=shphead">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_tokopedia.png" class="img-responsive">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://www.bukalapak.com/u/smartfrenofficial">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_bukalapak.png" class="img-responsive">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://www.blibli.com/merchant/smartfren-official/SMO-56333">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_blibli.png" class="img-responsive">
											</a>
										</div>
										<div class="col-md-2">
											<a href="http://m.elevenia.co.id/store/smartfren-official-store">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_elevania.png" class="img-responsive">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://shopee.co.id/smartfrenstore?v=2f5&smtt=0.0.3">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_shopee.png" class="img-responsive">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://www.smartfren.com/id/galeri">
												<img src="<?php echo base_url()?>assets/img/MerchantButton_gallery.png" class="img-responsive">
											</a>
										</div>
									</div>
								</div>
							</div>
							
						</li>
					</ul>
					<div class="list">
					</div>
				</div>
			</div>
			
		</div>
	</div>
<a href="https://www.smartfren.com/wow/meraihmimpi">
		<img src="<?php echo base_url()?>assets/img/Carousel-WOWSquad.png" width="100%">
	</a>
<section class="bg-white">
	<p class="text-center color-hitam font-24 TruenoBd">Cara Cek Kupon Undian Smartfren WOW</p>
	<div class="row text-center">
		<div class="col-md-6">
			<a href="https://linktr.ee/Smartfren">
			<img src="<?php base_url()?>assets/img/mysf-phone.png" class="img-responsive img-sfid">
			</a>
		</div>
		<div class="col-md-6">
			<ul class="check-kupon">
				<li>
					<img src="<?php echo base_url()?>assets/img/check-1.png">
					<div class="text">
						<p class="head">Download aplikasi MySmartfren</p>
						<p>Di <a href="https://apps.apple.com/id/app/mysmartfren/id1209898190" target="_blank">App Store</a> atau <a href="https://play.google.com/store/apps/details?id=com.smartfren" target="_blank">Play Store</a></p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-2.png">
					<div class="text">
						<p class="head">Daftar atau Masuk di aplikasi MySmartfren</p>
						<p>Bisa menggunakan email atau nomor Smartfren</p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-3.png">
					<div class="text">
						<p class="head">Pilih Smartpoin</p>
						<p>Cari lambang koin pada halaman</p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-4.png">
					<div class="text">
						<p class="head">Temukan Kupon</p>
						<p>Temukan kupon pada halaman "Voucher Saya"</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
</section>
	
