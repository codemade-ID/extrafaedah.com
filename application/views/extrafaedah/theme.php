<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php echo @$fb_share;?></title>
	<meta name="description" content="Yuk Bisa Yuk Bikin #RamadanExtraFaedah Tahun Ini!" />
	<link rel="icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.ico"/>
	<!-- S:OG_FB -->
	<meta property="og:url" content="<?php echo current_url();?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo @$fb_share;?>" />
	<meta name="og:image" content="<?php echo base_url();?>img/pp.png" />
	<meta property="og:site_name" content="#RamadanExtraFaedah" />
	<meta property="og:description" content="Yuk Bisa Yuk Bikin #RamadanExtraFaedah Tahun Ini!" />
	<meta property="article:author" content="https://www.facebook.com/smartfren" />
	<meta property="article:publisher" content="https://www.facebook.com/smartfren" />
	<meta property="fb:app_id" content="525085630997607" />
	<meta property="fb:pages" content="62225654545" />
	<!-- E:OG_FB -->
	<!-- S:TC -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@smartfrenworld" />
	<meta name="twitter:creator" content="@smartfrenworld">
	<meta name="twitter:title" content="#RamadanExtraFaedah">
	<meta name="twitter:description" content="<?php echo @$tw_share;?>">
	<meta name="twitter:image" content="<?php echo base_url();?>img/pp.png" />
	<!-- E:TC -->

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Gudea:400,400italic,700'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/fonts/stylesheet.css?v=1" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css?v=1.8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/frontend.css?v=2.10" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css?v=2.8" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/grid-2.6/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo-smartfren.png"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick-theme.css"/>
	<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.min.js"></script>

	<link rel="shortcut icon" href="https://www.smartfren.com/static/assets/images/Icon.ico" type="image/x-icon" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21133138-6"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-21133138-6');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NM4WJW6');</script>
	<!-- End Google Tag Manager -->

</head>
<body id="header" class="extrafaedah">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NM4WJW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="kv-top">
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>assets/img/sf-logo.svg" class="logo logo-left" width="150">
				</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse hide">
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="#home">HOME</a></li>
	                <li><a href="#gallery-post" class="scrollTos">GALLERY</a></li>
	                <li><a href="#contactus" class="scrollTos">CONTACT US</a></li>
	            </ul>
	        </div>
	    </nav>
	</div>
	<div class="container text-center">
		<div id="banner-top" class="carousel slide" data-ride="carousel">
			<?php if(!$this->ua->is_mobile()):?>
			    <div class="carousel-item active">
			      <img src="<?php echo base_url();?>assets/img/ef-banner-1-1-1.jpg" class="img-responsive" alt="Banner 1">
			    </div>
			    <div class="carousel-item">
			    	<a href="#learnmore"  class="scrollTos">
			      		<img src="<?php echo base_url();?>assets/img/ef-banner-2.jpg" class="img-responsive" alt="Banner 2">
			      	</a>
			    </div>
			    <!-- <div class="carousel-item">
			      <img src="<?php echo base_url();?>assets/img/ef-banner-3.jpg" class="img-responsive" alt="Banner 1">
			    </div> -->
			    
		    <?php else:?>
				<div class="carousel-item active">
			      <img src="<?php echo base_url();?>assets/img/ef-banner-1-1-1-m.jpg" class="img-responsive" alt="Banner 1">
			    </div>
			    <div class="carousel-item">
			    	<a href="#learnmore"  class="scrollTos">
					      <img src="<?php echo base_url();?>assets/img/ef-banner-2-m.jpg" class="img-responsive" alt="Banner 1">
					  </a>
			    </div>
			    <!-- <div class="carousel-item">
			      <img src="<?php echo base_url();?>assets/img/ef-banner-3-m.jpg" class="img-responsive" alt="Banner 1">
			    </div> -->
			<?php endif;?>
		    
	  	</div>
	  	<div class="mar-20 mar-l mar-r">
	  		<h3 class="color-black">
	  			<span>YUK BISA YUK BIKIN</span>
	  			<img src="<?php echo base_url();?>assets/img/ef-img-hashtag.png">
	  			<span>TAHUN INI!</span>
	  		</h3>
	  	</div>
	  	<div class="cta">
	  		<a href="#learnmore" class="font-20 scrollTos">
		  		CEK DI SINI <br />
		  		<img class="" src="<?php echo base_url();?>assets/img/ef-arrow-down.png" width="90">
		  	</a>
	  	</div>
	</div>
</div>
<section class="cara-ikutan">
	<div class="container">
		<p class="text-center mar-40 mar-r mar-l mar-t">Mau Ramadan 2021 tetap EXTRA FAEDAH? Yuk Bisa Yuk ikutin saran Bundah Paedah buat Anya dan Arief plus bisa menangin hadiah berfaedah!</p>
		
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="resp-container">
					<iframe class="resp-iframe" width="800" height="642" src="https://www.youtube.com/embed/Or1fwAW7w-E?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ></iframe>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="container" id="learnmore">
		<h2 class="text-center color-black mar-40">CARA IKUTAN NGONTEN EXTRA FAEDAH</h2>
		<div class="row howto">
			<div class="col-md-6 logo">
				<img src="<?php echo base_url();?>assets/img/ef-logo.png" class="img-responsive">
			</div>
			<div class="col-md-6">
				<div class="teks">
					<ul>
						<li><img src="<?php echo base_url()?>assets/img/ef-howto-1.png">
							<div class="note">
							Follow Instagram <a href="https://www.instagram.com/smartfrenworld" target="_blank" rel="noopener" aria-invalid="true"><strong>@smartfrenworld </strong></a>
							</div>
						</li>

						<li><img src="<?php echo base_url()?>assets/img/ef-howto-2.png">
							<div class="note">
								Buat konten Extra Faedah versi kamu berupa video/foto dengan caption yang paling faedah
							</div>
						</li>

						<li><img src="<?php echo base_url()?>assets/img/ef-howto-3.png">
							<div class="note">
								Upload di Instagram kamu dengan mention ke <a href="https://www.instagram.com/smartfrenworld" target="_blank" rel="noopener" aria-invalid="true"><strong>@smartfrenworld </strong></a> <b>dan 2 teman kamu</b>
							</div>
						</li>

						<li><img src="<?php echo base_url()?>assets/img/ef-howto-4.png">
							<div class="note">
								Gunakan hashtag <b>#RamadanExtraFaedah</b> <b>#UnlimitedBisaSemua</b>
							</div>
						</li>
						
					</ul>
				</div>
			</div>
			<div class="col-md-12 text-center">
				<br />
				<br />
				<br />
				<span class="0"><a href="javascript:;" class="color-putih pad-20" data-toggle="modal" data-target="#tnc">Syarat dan Ketentuan</a></span>
			</div>
		</div>
	</div>
	
</section>
<section class="netijen">
	<div class="container">
		<h2 class="text-center color-black mar-10">&nbsp;</h2>
		<div class="row">
			<div class="col-md-12">
				<?php if(!$this->ua->is_mobile()):?>
					<img src="<?php echo base_url()?>assets/img/ef-desktop-hadiah.jpg" class="img-responsive">
				<?php else:?>
					<img src="<?php echo base_url()?>assets/img/ef-mobile-hadiah.jpg" class="img-responsive">
				<?php endif;?>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('extrafaedah/tnc');?>
<section class="netijen">
	<div class="container">
		<h2 class="text-center color-black mar-40">CERITA FAEDAH NETIJEN</h2>
		<p class="text-center">Ini dia yang udah ngonten Extra Faedah!
Yuk, Bikin konten Extra Faedah versi kamu sekarang!</p>
		<div class="slick-gallery" id="gallery-post">
			<?php if(!empty($submission)) :?>
					<?php $i=0; foreach($submission as $s):?>
						<div class="item">
							<div class="row">
								<div class="col-md-12 post <?php echo $i;?>">
									<a href="<?php echo $s->instagramlink;?>" target="_blank">
										<img src="<?php echo $s->thumbnail;?>" class="img-responsive">
										<span class="play-btn"></span>
									</a>
								</div>
							</div>
						</div>
					<?php $i++;endforeach;?>
			<?php endif;?>
		</div>
		<?php if($this->ua->is_mobile()):?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.slick-gallery').slick({
					  dots: false,
					  infinite: true,
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  autoplay: false,
		  			  autoplaySpeed: 3000,
					});
				})
			</script>
		<?php else:?>
			<script type="text/javascript">
			$(document).ready(function(){
				$('.slick-gallery').slick({
				  dots: false,
				  infinite: true,
				  slidesToShow: 4,
				  slidesToScroll: 1,
				  autoplay: false,
	  			  autoplaySpeed: 3000,
				});
			})
		</script>
		<?php endif;?>
		
	</div>
	
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p>&copy; Copyright 2021 smartfren. All Rights Reserved</p>
			</div>
			<div class="col-md-6">
				<ul>
					<li>
						<a href="https://www.smartfren.com/privacy-policy" target="_blank">Privacy Policy</a>
					</li>
					<li>
						<a href="https://www.smartfren.com/terms-of-use" target="_blank">Term of Use</a>
					</li>
					<li>
						<a href="https://www.smartfren.com/terms-condition" target="_blank">Terms & Condition</a>
					</li>
				</ul>
				                                       
			</div>
		</div>
	</div>
</footer>
<a href="javascript:;" class="totop" style="font-size: 40px;display: block;position: fixed;bottom: 20%; right: 20px;color: #000; display: none;z-index: 9999">
	<i class="fa fa-arrow-circle-up"></i>
</a>
<a href="https://www.smartfren.com/explore/product/unlimited/" target="_blank" style="font-size: 40px;display: block;position: fixed;top: 20%; right: 0px;color: #000; z-index: 1">
	<img id="cta-sticky" src="<?php echo base_url()?>assets/img/ef-sticky.png" width="40">
</a>
<script type="text/javascript">
	$('.totop').click(function(){
		$('html,body').animate({scrollTop:0},500);
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()) {
	        $('.totop:hidden').stop(true, true).fadeIn();
	    } else {
	        $('.totop').stop(true, true).fadeOut();
	    }
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('a.scrollTos').click(function(){
			var href = $(this).attr('href');
			$('html, body').animate({
		        scrollTop: $(href).offset().top
		    }, 2000);
		});

	});

	$('#banner-top').slick({
		autoplay:true,
        arrows: true,
        // prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        // nextArrow:"<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>"
    
	});
</script>
</body>
</html>
