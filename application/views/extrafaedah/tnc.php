<!-- Modal -->
<div class="modal fade" id="tnc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      	<p class="text-center font-20 bold">Ramadan Extra Faedah - Digital Competition</p>
<p dir="ltr">Periode : 7 April - 12 Mei</p>
<p dir="ltr">Cara Ikutan Ngonten Extra Faedah</p>
<p dir="ltr">Buat kamu yang udah dapet hadiah #RamadanExtraFaedah&nbsp;</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Follow instagram Smartfren (@smartfrenworld)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Buat konten Extra Faedah versi kamu berupa video atau foto dengan caption yang paling faedah</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Upload di Instagram kamu dan mention ke @smartfrenworld</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Gunakan hashtag #RamadanExtraFaedah , #UnlimitedBisaSemua dan mention temanmu</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Upload sebanyak-banyaknya karena tidak ada batasan jumlah submission.</p>
</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Hadiah #RamadanExtraFaedah :</p>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Periode 7 - 13 April&nbsp;</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Utama: Playstation 5 (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (1 pcs)&nbsp;</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Juragan Faedah: Samsung Active 2 (1 pcs), Poco X3 (1 pcs), dan Kartu Perdana smartfren Unlimited MAXI (2 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Gang Faedah: Samsung A02 (1 pcs) dan Kartu Perdana smartfren&nbsp; Unlimited MAXI (3 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Pasukan Faedah: Merchandise kokreasi WOWlabs (5 pcs) dan Kartu Perdana smartfren Unlimited MAXI (5 pcs)</p>
</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Periode 14 - 20 April&nbsp;</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Utama: iPhone 12 (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (1 pcs)&nbsp;</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Juragan Faedah: Samsung Active 2 (1 pcs), Poco X3 (1 pcs), dan Kartu Perdana smartfren Unlimited MAXI (2 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Gang Faedah: Redmi 9C (3 pcs) dan Kartu Perdana smartfren Unlimited MAXI (3 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Pasukan Faedah: Merchandise kokreasi WOWlabs (5 pcs) dan Kartu Perdana smartfren Unlimited MAXI (5 pcs)</p>
</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Periode 21 - 27 April&nbsp;</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Utama: Fujifilm Xa5 (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (1 pcs)&nbsp;</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Juragan Faedah: Samsung Active 2 (1 pcs), Poco X3 (1 pcs), dan Kartu Perdana smartfren Unlimited MAXI (2 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Gang Faedah: Samsung A02 (3 pcs) dan Kartu Perdana smartfren Unlimited MAXI (3 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Pasukan Faedah: Exclusive Merchandise kokreasi WOWlabs (5 pcs) dan Kartu Perdana smartfren Unlimited MAXI (5 pcs)</p>
</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Periode 28 April - 4 Mei</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Utama: Sepeda Exclusive Addy Debil x smartfren (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (1 pcs)&nbsp;</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Juragan Faedah: Samsung Active 2 (1 pcs), Poco X3 (1 pcs), dan Kartu Perdana smartfren Unlimited MAXI (2 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Gang Faedah: Redmi 9C (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (3 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Pasukan Faedah: Merchandise kokreasi WOWlabs (5 pcs) dan Kartu Perdana smartfren Unlimited MAXI (5 pcs)</p>
</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p dir="ltr">Periode 5 - 12 Mei</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Utama: Asus Note Book A56 (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (1 pcs)&nbsp;</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Juragan Faedah: Samsung Active 2 (1 pcs), Poco X3 (1 pcs), dan Kartu Perdana smartfren Unlimited MAXI (2 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Gang Faedah: Samsung A02 (1 pcs) dan Kartu Perdana smartfren Unlimited MAXI (3 pcs)</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Hadiah Pasukan Faedah: Merchandise kokreasi WOWlabs (5 pcs) dan Kartu Perdana smartfren Unlimited MAXI (5 pcs)</p>
</li>
</ol>
<p>Syarat &amp; Ketentuan</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Foto, video, serta caption tidak boleh mengandung unsur SARA &amp; Pornografi</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Peserta diperbolehkan untuk membuat konten lebih dari satu kali</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Smartfren berhak untuk melakukan pengubahan syarat &amp; ketentuan sewaktu-waktu</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Seluruh isu atau perselisihan yang mungkin timbul sehubungan dengan syarat dan ketentuan ini dibuat berdasarkan dan tunduk terhadap hukum yang berlaku di Republik Indonesia. Peserta dengan ini sepakat untuk menyelesaikan seluruh permasalahan melalui musyawarah untuk mencapai mufakat</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Smartfren berhak untuk melakukan diskualifikasi peserta yang tidak memenuhi, melanggar atau dicurigai melakukan kecurangan</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Seluruh submisi #RamadanExtraFaedah dan #UnlimitedBisaSemua yang dikirimkan sepenuhnya menjadi milik Smartfren.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Keputusan juri mutlak, dan tidak dapat diganggu gugat.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;" aria-level="1">
<p dir="ltr" role="presentation">Pengiriman hadiah dilakukan 60 hari kerja setelah data pemenang sudah di lengkapi</p>
</li>
</ol>
<p>&nbsp;</p>

      </div>
    </div>
  </div>
</div>