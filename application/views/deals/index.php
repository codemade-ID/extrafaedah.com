<section class="bg-white">
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/deals/banner_head_mobile.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/deals/banner_head_desktop.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
</section>
<section class="deals bg-white day">
	<div class="container">
		<h1 class="color-hitam-sf TruenoBd text-center font-40">WOW Deals</h1>
		<!-- <div class="row mar-30 mar-r mar-l">
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						SENIN
						<p>6 Jan</p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#SENIN">Lihat Detail</a>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						SELASA
						<p>7 Jan</p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#SELASA">Lihat Detail</a>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						RABU
						<p>8 Jan</p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#RABU">Lihat Detail</a>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						KAMIS
						<p>9 Jan</p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#KAMIS">Lihat Detail</a>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						JUMAT
						<p>10 Jan</p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#JUMAT">Lihat Detail</a>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div> -->

		<div class="row mar-30 mar-r mar-l">
			<div class="col-md-1"></div>

			<?php foreach($result as $r) :?>
			<div class="col-md-2">
				<div class="boxing-day">
					<div class="box-top">
						<?php echo $r->day;?>
						<p><?php echo $r->text_simple;?></p>
					</div>
					<div class="box-bottom">
						<a href="javascript:;" data-toggle="modal" data-target="#POP-<?php echo $r->id?>">Lihat Detail</a>
					</div>
				</div>
			</div>
			<?php endforeach;?>
			
			<div class="col-md-1"></div>
		</div>
	</div>
	
	<?php $this->load->view('deals/popupdb');?>
	<?php //$this->load->view('deals/popup9');?>
</section>
<section class="deals bg-red-sf text-center cara">
	<div class="container">
		<p class="color-hitam bold text-center font-20 color-putih">CARA MENDAPATKAN PROMO WOW DEALS</p>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-2">
				<img src="<?php base_url()?>assets/img/deals/cara-1.png" class="img-responsive">
				<p class="lu-name">Pantengin terus aplikasi MySmartfren</p>
			</div>
			<div class="col-md-2">
				<img src="<?php base_url()?>assets/img/deals/cara-2.png" class="img-responsive">
				<p class="lu-name">Pilih menu "SmartPoin"</p>
			</div>
			<div class="col-md-2">
				<img src="<?php base_url()?>assets/img/deals/cara-3.png" class="img-responsive">
				<p class="lu-name">Pilih menu "Tukar Poin"</p>
			</div>
			<div class="col-md-2">
				<img src="<?php base_url()?>assets/img/deals/cara-4.png" class="img-responsive">
				<p class="lu-name">Pilih Banner "WOW Deals"</p>
			</div>
			<div class="col-md-2">
				<img src="<?php base_url()?>assets/img/deals/cara-5.png" class="img-responsive">
				<p class="lu-name">Pilih menu "Kupon" untuk cek voucher</p>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</section>