<?php foreach($result as $r) :?>
<!-- Modal -->
<div class="modal fade" id="POP-<?php echo $r->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih"><?php echo $r->day;?>, <?php echo $r->text;?></h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_1_1;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_1_2;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_1_3;?>.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_2_1;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_2_2;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_2_3;?>.png" class="img-responsive">
        	</div>
        	
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_3_1;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_3_2;?>.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/<?php echo $r->row_3_3;?>.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
