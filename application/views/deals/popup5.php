<!-- Modal -->
<div class="modal fade" id="SENIN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">SENIN, 9 Desember 2019</h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Grab.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ilotte.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kompas.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Shopee.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Zalora.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/RedDoorz.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="SELASA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">SELASA, 10 Desember 2019</h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Shopee.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/RedDoorz.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kompas.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ilotte.png" class="img-responsive">
        	</div>
        	
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/KFC.jpg" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ruangguru.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="RABU" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">RABU, 11 Desember 2019</h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Grab.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Shopee.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kompas.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Zalora.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Giladiskon.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ilotte.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="KAMIS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">KAMIS, 12 Desember 2019</h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/KFC.jpg" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ilotte.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Zalora.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ruangguru.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kompas.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Shopee.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="JUMAT" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">JUMAT, 13 Desember 2019</h1>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">9-10 AM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kompas.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Shopee.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">12-13 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Grab.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Giladiskon.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        </div>
        <p class="text-center color-putih bg-red-sf pad-20 pad-l pad-r">17 - 18 PM</p>
        <div class="row pad-20">
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Kopikenangan.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Zalora.png" class="img-responsive">
        	</div>
        	<div class="col-md-4">
        		<img src="<?php echo base_url()?>assets/img/deals/partner/Ruangguru.png" class="img-responsive">
        	</div>
        </div>
        <div class="bg-red-sf pad-20"></div>
		
		<p class="color-hitam content">
			Tukar 1 SmartPoin kamu sekarang untuk mendapatkan Deals di Merchant di atas
			<br />
			<center>
				<a href="https://my.smartfren.com/" target="_blank" class="btn btn-danger color-putih" style="background: #ff1659">Go to MySF</a>
			</center>
			<br />
			<br />
		</p>
      </div>
    </div>
  </div>
</div>