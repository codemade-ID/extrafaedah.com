<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php echo @$fb_share;?></title>
	<meta name="description" content="Smartfren WOW is Coming Now!" />
	<!-- S:OG_FB -->
	<meta property="og:url" content="<?php echo current_url();?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo @$fb_share;?>" />
	<meta name="og:image" content="<?php echo base_url();?>img/pp.png" />
	<meta property="og:site_name" content="smartfren.com" />
	<meta property="og:description" content="Smartfren WOW is Coming Now!" />
	<meta property="article:author" content="https://www.facebook.com/smartfren" />
	<meta property="article:publisher" content="https://www.facebook.com/smartfren" />
	<meta property="fb:app_id" content="525085630997607" />
	<meta property="fb:pages" content="62225654545" />
	<!-- E:OG_FB -->
	<!-- S:TC -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@smartfrenworld" />
	<meta name="twitter:creator" content="@smartfrenworld">
	<meta name="twitter:title" content="Smartfren WOW is Coming Now!">
	<meta name="twitter:description" content="<?php echo @$tw_share;?>">
	<meta name="twitter:image" content="<?php echo base_url();?>img/pp.png" />
	<!-- E:TC -->

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Gudea:400,400italic,700'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/fonts/stylesheet.css?v=1" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css?v=1.6" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/frontend.css?v=2.6" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css?v=2.6" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/grid-2.6/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo-smartfren.png"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick-theme.css"/>
	<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.min.js"></script>

	<link rel="shortcut icon" href="https://www.smartfren.com/static/assets/images/Icon.ico" type="image/x-icon" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21133138-6"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-21133138-6');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NM4WJW6');</script>
	<!-- End Google Tag Manager -->

</head>
<body id="header">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NM4WJW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
	<div class="container">
		<div class="row">
			<div class="col-md-3 text-left pad-10 pad-b">
				<?php /*
				<?php if($this->uri->segment(1) == '' && !$this->ua->is_mobile()) :?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="logo logo-left" width="150">
					</a>
				<?php elseif($this->uri->segment(1) != ''):?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="logo logo-left" width="150">
					</a>
				<?php else:?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="logo logo-left" width="150">
					</a>
				<?php endif;?>
				*/ ?>
				<?php if($this->uri->segment(1) != 'deals') :?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="logo logo-left" width="150">
					</a>
				<?php endif;?>
			</div>
			<div class="col-md-6 text-center">
				<?php if($this->uri->segment(1) == 'deals') :?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/wow_logo.png" class="logo logo-center" width="150">
					</a>
					<style type="text/css">
						header{background-color: #fff;}
						.fa{color: #000;}
					</style>
				<?php elseif($this->uri->segment(1) != 'fest') :?>
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/logo-wow-new.png" class="logo logo-center" width="150">
					</a>
				<?php endif;?>
			</div>
			<div class="col-md-3 text-right pad-10 pad-b">
				<div class="shr" style="">
		    		<div style="" class="shrp">
			    		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url();?>" target="_blank">
			    			<i class="fa fa-facebook" aria-hidden="true"></i>
			    		</a>
			    		<a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(@$tw_share)?>" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
			    		</a>
		    		</div>
		    	</div>

				<a href="javascript:;" class="btn-shr">
					<img src="https://www.searchpng.com/wp-content/uploads/2019/02/Sgare-White-Icon-PNG-715x715.png" class="logo logo-right" width="32">
				</a>
				<script type="text/javascript">
			    	$('a.btn-shr').click(function(){
			    		$('.shr').toggle();
			    	});
			    </script>

			</div>
		</div>
	</div>
</header>
<?php echo @$content;?>
<a href="javascript:;" class="totop" style="font-size: 40px;display: block;position: fixed;bottom: 20%; right: 20px;color: #000; display: none;z-index: 9999">
	<i class="fa fa-arrow-circle-up"></i>
</a>
<script type="text/javascript">
	$('.totop').click(function(){
		$('html,body').animate({scrollTop:0},500);
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()) {
	        $('.totop:hidden').stop(true, true).fadeIn();
	    } else {
	        $('.totop').stop(true, true).fadeOut();
	    }
	});
</script>
<!-- Footer -->
<footer>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 segment-one">
          <h2>Download Aplikasi <br />MySmartfren</h2>
          <a href="https://play.google.com/store/apps/details?id=com.smartfren" target="_blank"><img src="<?php echo base_url();?>assets/img/googleplay.png" width="130" height="40"></a>
          <a href="https://apps.apple.com/id/app/mysmartfren/id1209898190" target="_blank"><img src="<?php echo base_url();?>assets/img/applestore.png" width="130" height="40"></a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 segment-two">
          <h2>Follow Us <br /> &nbsp;</h2>
          <a href="https://www.facebook.com/smartfren/" target="_blank"><i class="facebook"><img src="<?php echo base_url();?>assets/img/fb.png" width="40"
                height="40"></i></a>
          <a href="https://twitter.com/smartfrenworld?s=09" target="_blank"><i class="twitter"><img src="<?php echo base_url();?>assets/img/tw.png" width="40"
                height="40"></i></a>
          <a href="https://instagram.com/smartfrenworld?igshid=ndapwdekp6li" target="_blank"><i class="instagram"><img src="<?php echo base_url();?>assets/img/instagram.png" width="40"
                height="40"></i></a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 segment-three">
          <h2>Contact Us <br /> &nbsp;</h2>
          <p><b>Smartfren User:</b> 888 <br>
            <b>Other:</b> 08888 1 88888</br>
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript">
	$(document).ready(function(){
		$('a.scrollTos').click(function(){
			var href = $(this).attr('href');
			$('html, body').animate({
		        scrollTop: $(href).offset().top - 80
		    }, 2000);
		});

	});
</script>
</body>
</html>
