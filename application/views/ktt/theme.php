<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php echo @$fb_share;?></title>
	<meta name="description" content="Smartfren WOW is Coming Now!" />
	<!-- S:OG_FB -->
	<meta property="og:url" content="<?php echo current_url();?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo @$fb_share;?>" />
	<meta name="og:image" content="<?php echo base_url();?>img/pp.png" />
	<meta property="og:site_name" content="smartfren.com" />
	<meta property="og:description" content="Smartfren WOW is Coming Now!" />
	<meta property="article:author" content="https://www.facebook.com/smartfren" />
	<meta property="article:publisher" content="https://www.facebook.com/smartfren" />
	<meta property="fb:app_id" content="525085630997607" />
	<meta property="fb:pages" content="62225654545" />
	<!-- E:OG_FB -->
	<!-- S:TC -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@smartfrenworld" />
	<meta name="twitter:creator" content="@smartfrenworld">
	<meta name="twitter:title" content="#KreatifTanpaTapi">
	<meta name="twitter:description" content="<?php echo @$tw_share;?>">
	<meta name="twitter:image" content="<?php echo base_url();?>img/pp.png" />
	<!-- E:TC -->

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Gudea:400,400italic,700'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/fonts/stylesheet.css?v=1" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css?v=1.8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/frontend.css?v=2.9" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css?v=2.8" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/grid-2.6/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo-smartfren.png"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick-theme.css"/>
	<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.min.js"></script>

	<link rel="shortcut icon" href="https://www.smartfren.com/static/assets/images/Icon.ico" type="image/x-icon" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21133138-6"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-21133138-6');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NM4WJW6');</script>
	<!-- End Google Tag Manager -->

</head>
<body id="header">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NM4WJW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="kv-top">
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="logo logo-left" width="150">
				</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav navbar-right">
	                <li><a href="#home">HOME</a></li>
	                <li><a href="#gallery-post" class="scrollTos">GALLERY</a></li>
	                <li><a href="#contactus" class="scrollTos">CONTACT US</a></li>
	            </ul>
	        </div>
	    </nav>
	</div>
	<div class="container text-center">
		<img src="/assets/img/logo.png" class="img-responsive logo-kv" width="500">
	</div>
	<div class="cta text-center">
		<a href="#form" class="btn btn-white sbmt scrollTos">
			SUBMIT FILE FORM
		</a>
	</div>
	
</div>
<section id="form">
	<div class="container">
		<div class="row text-center bg-form">
			<div class="col-md-7">
				<form action="" method="POST" enctype="multipart/form-data" class="form-signin">
					<h2 class="form-signin-heading">SUBMIT FILE FORM</h2>
					<?php echo validation_errors();?>
					<?php echo $this->upload->display_errors('<p class="color-merah">', '</p>');?>
					<!-- <p class="color-hijau font-26"><?php echo $this->session->flashdata("msg");?></p> -->
					<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
					<label for="namalengkap" class="sr-only">Nama Lengkap</label>
			        <input name="namalengkap" type="text" class="form-control" placeholder="Nama Lengkap" required value="<?php echo set_value("namalengkap")?>">
			        <label for="profesi" class="sr-only">Profesi</label>
			        <input name="profesi" type="text" class="form-control" placeholder="Profesi" required value="<?php echo set_value("profesi")?>">
				    <label for="instagramlink" class="sr-only">Instagram Link</label>
			        <input name="instagramlink" type="text" class="form-control" placeholder="Instagram Link" required value="<?php echo set_value("instagramlink")?>">
					<label for="email" class="sr-only">Email</label>
			        <input name="email" type="email" class="form-control" placeholder="Email" required value="<?php echo set_value("email")?>">
			        <input name="filenames" type="hidden" class="form-control" placeholder="Filename" required value="<?php echo set_value("filename")?>">
					<p class="text-left">
						<a href="javascript:;" id="button-file">
							<i class="fa fa-upload" aria-hidden="true"></i> Upload file
						</a>
						<input id="file-input" type="file" name="filename" style="display: none;" required="" />
					</p>
					<div class="progress" width="20" style="display: none;">
					  <div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0">
					    <!-- <p id="meter"></p>% -->
					  </div>
					</div>
					<script type="text/javascript">
						$('#button-file').on('click', function() {
						    $('#file-input').trigger('click');
						});
						$(document).ready(function(){

							$(".form-signin input[type=submit]").click(function(){
								// $(".btnsubmit").hide();
								// $(".loading").show();
								// $(".form-signin").submit();
							});
							$(".form-signin").submit(function(){
								$(".btnsubmit").hide();
								$(".loading").show();
								return true;
							});
							$('input[type="file"]#file-input').change(function(e){
					            // var fileName = e.target.files[0].name;
					            // var iCon = '<i class="fa fa-upload" aria-hidden="true"></i> ';
					            // $("a#button-file").html(iCon+fileName);
					            // $("a#button-file i").after(fileName);
					        });

							$('input[type="file"]#file-input').on('change', function(e) {
								$(".progress").show();
								$(".progress-bar").css("width","0px");
								var file_data = e.target.files[0];
								var form_data = new FormData();                  
							    form_data.append('filename', file_data);
							    $.ajax({
							        url: '/ktt/upload', // point to server-side PHP script 
							        dataType: 'text',  // what to expect back from the PHP script, if anything
							        cache: false,
							        contentType: false,
							        processData: false,
							        data: form_data,                         
							        type: 'post',
							        xhr: function()
  {
									    var xhr = new window.XMLHttpRequest();
									    //Upload progress
									    xhr.upload.addEventListener("progress", function(evt){
									      if (evt.lengthComputable) {
									        var percentComplete = evt.loaded / evt.total;
									        //Do something with upload progress
									        console.log(percentComplete*100);
									        var meter = percentComplete*100;
									        $(".progress-bar").css("width",meter+"%");
									        // $("#meter").text(meter+"%");
									      }
									    }, false);
									    
									    return xhr;
									  },
							        success: function(i){
							        	if(i != 'error'){
							        		var iCon = '<i class="fa fa-upload" aria-hidden="true"></i> ';
								            $("a#button-file").html(iCon+i);
								            $("input[name=filenames]").val(i);
							        	}else{
							        		alert(i);
							        	}
							        	$(".progress").hide();
							            // alert(php_script_response); // display response from the PHP script, if any
							        }
							     });
							});



							<?php if($this->session->flashdata("msg") == "success"):?>
					            $('#resultPost').modal('show');
					        <?php endif;?>
						});
					</script>
					<p class="text-left">
						<a href="/assets/[smartfren] Kreatif Tanpa Tapi - Project Guideline.pdf" download="">
						<i class="fa fa-download" aria-hidden="true"></i> Download dokumen brief
						</a>
					</p>
					<p>
						<input type="submit" class="btn btn-dark btnsubmit" value="Submit">
					</p>
				</form>

				<div class="modal" tabindex="-1" role="dialog" id="resultPost">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<h3>TERIMA KASIH</h3>
				        <p>Karya kamu sudah kami terima dan akan kami review. Karyamu akan muncul 1x24 Jam di Gallery. Sekarang upload karyamu ke instagram pribadi dengan hashtag #KreatifTanpaTapi untuk mendapatkan hadiah langsung uang tunai dan tunggu pengumuman pemenang hadiah utama di sosial media smartfren!</p>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div class="col-md-5 tnc">
				<h3>TERMS AND CONDITION</h3>
				<ul>
				<li>Project #KreatifTanpaTapi tidak terbuka untuk umum</li>
				<li>Karya tidak boleh mengandung unsur SARA &amp; Pornografi</li>
				<li>Peserta diperbolehkan untuk memberikan karya lebih dari satu kali</li>
				<li>Smartfren berhak untuk melakukan pengubahan syarat &amp; ketentuan sewaktu-waktu</li>
				<li>Seluruh isu atau perselisihan yang mungkin timbul sehubungan dengan syarat dan ketentuan ini dibuat berdasarkan dan tunduk terhadap hukum yang berlaku di Republik Indonesia. Peserta dengan ini sepakat untuk menyelesaikan seluruh permasalahan melalui musyawarah untuk mencapai mufakat</li>
				<li>Smartfren berhak untuk melakukan diskualifikasi peserta yang tidak memenuhi, melanggar atau dicurigai melakukan kecurangan</li>
				<li>Seluruh submisi #KreatifTanpaTapi yang dikirimkan menjadi milik Smartfren dan kreator bersama.</li>
				<li>Keputusan juri mutlak, dan tidak dapat diganggu gugat.</li>
				</ul>
				<p>&nbsp;</p>
			</div>
		</div>
	</div>
	
</section>
<section id="gallery-post">
	<div class="container">
		<?php if(!empty($submission)) :?>
		<h2>GALLERY</h2>
		<?php endif;?>
		<?php /*
		<div class="row">
			
				
				<?php foreach($submission as $s):?>
					<div class="col-md-3 post">
						<a href="<?php echo $s->instagramlink?>" target="_blank">
							<img src="/uploads/<?php echo $s->thumbnail;?>" class="img-responsive" alt="<?php echo $s->namalengkap?>">
						</a>
						<div class="onhover">
							<p class="name"><?php echo $s->namalengkap?></p>
							<p class="profesi"><?php echo $s->profesi?></p>
							<div class="link">
								<!-- <a href="javascript:;" class="love">
									<i class="fa fa-heart" aria-hidden="true"></i>
								</a> -->
								<a href="<?php echo $s->instagramlink?>" target="_blank">
									<i class="fa fa-external-link" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach;?>
				
			
		</div>
		*/ ?>
		<div id="galleries"></div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#pagination').on('click','a',function(e){
			       e.preventDefault(); 
			       var pageno = $(this).attr('data-ci-pagination-page');
			       loadPagination(pageno);
			     });
				loadPagination(0);
				function loadPagination(pagno){
			       $.ajax({
			         url: '/load/'+pagno,
			         type: 'get',
			         dataType: 'html',
			         success: function(response){
			            // $('#pagination').html(response.pagination);
			            // createTable(response.result,response.row);
			            $("#galleries").html(response);
			         }
			       });
			     }
			});
		</script>
	</div>
</section>
<section id="contactus">
	<div class="container">
		<h2>CONTACT US</h2>
		<div class="row">
			<div class="col-sm-5">
				<div class="box-rounded bg-putih text-center">
					<img src="/assets/img/logo-2.jpg" width="60%">
					<p class="color-hitam">
						#KreatifTanpaTapi adalah sebuah project spesial dengan memberikan “Kanvas” untuk membuka peluang bagi para pekerja kreatif untuk menyalurkan setiap karya dari keahlian yang mereka miliki. <br />
						Dengan #KreatifTanpaTapi mereka bisa menuangkan ide mereka menjadi karya yang unik, fresh dan orisinil.
					</p>
				</div>
				<div class="box-rounded bg-putih text-center">
					<h2>CONTACT INFO</h2>
					<p class="color-hitam">
						<span class="TruenoBd">Ardyan Endardo</span> <br />
<i>(On Behalf of smartfren's Official Agency Partner)</i> <br />
ardyan.endardo@popcult.co.id<br />
+62 888 0103 2512<br />
					</p>
				</div>
			</div>
			<div class="col-sm-7 pad-50  text-center">
				<img src="/assets/img/perdana.png" width="80%">
			</div>
		</div>
		<div class="row text-center commerce">
			<div class="col-md-3">
				<img src="/assets/img/icon_shopee.png" width="60%">
				<br /><br />
				<a target="_blank" href="https://shopee.co.id/smartfrenstore?v=5e1&smtt=0.0.3" class="btn btn-white color-merah">
					BELI DI SINI
				</a>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/icon_tokopedia.png" width="60%">
				<br /><br />
				<a target="_blank" href="https://tokopedia.link/fI5BkQBUx1" class="btn btn-white color-merah">
					BELI DI SINI
				</a>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/icon_blibli.png" width="60%">
				<br /><br />
				<a target="_blank" href="https://www.blibli.com/merchant/smartfren-official/SMO-56333#commerce" class="btn btn-white color-merah">
					BELI DI SINI
				</a>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/icon_bukalapak.png" width="60%">
				<br /><br />
				<a target="_blank" href="https://www.bukalapak.com/u/smartfrenofficial" class="btn btn-white color-merah">
					BELI DI SINI
				</a>
			</div>
		</div>
	</div>
</section>
<?php //echo @$content;?>
<a href="javascript:;" class="totop" style="font-size: 40px;display: block;position: fixed;bottom: 20%; right: 20px;color: #000; display: none;z-index: 9999">
	<i class="fa fa-arrow-circle-up"></i>
</a>
<script type="text/javascript">
	$('.totop').click(function(){
		$('html,body').animate({scrollTop:0},500);
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()) {
	        $('.totop:hidden').stop(true, true).fadeIn();
	    } else {
	        $('.totop').stop(true, true).fadeOut();
	    }
	});
</script>
<!-- Footer -->
<footer>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 segment-one">
          <h2>Download Aplikasi <br />MySmartfren</h2>
          <a href="https://play.google.com/store/apps/details?id=com.smartfren" target="_blank"><img src="<?php echo base_url();?>assets/img/googleplay.png" width="130" height="40"></a>
          <a href="https://apps.apple.com/id/app/mysmartfren/id1209898190" target="_blank"><img src="<?php echo base_url();?>assets/img/applestore.png" width="130" height="40"></a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 segment-two">
          <h2>Follow Us <br /> &nbsp;</h2>
          <a href="https://www.facebook.com/smartfren/" target="_blank"><i class="facebook"><img src="<?php echo base_url();?>assets/img/fb.png" width="40"
                height="40"></i></a>
          <a href="https://twitter.com/smartfrenworld?s=09" target="_blank"><i class="twitter"><img src="<?php echo base_url();?>assets/img/tw.png" width="40"
                height="40"></i></a>
          <a href="https://instagram.com/smartfrenworld?igshid=ndapwdekp6li" target="_blank"><i class="instagram"><img src="<?php echo base_url();?>assets/img/instagram.png" width="40"
                height="40"></i></a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 segment-three">
          <h2>Contact Us <br /> &nbsp;</h2>
          <p><b>Smartfren User:</b> 888 <br>
            <b>Other:</b> 08888 1 88888</br>
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript">
	$(document).ready(function(){
		$('a.scrollTos').click(function(){
			var href = $(this).attr('href');
			$('html, body').animate({
		        scrollTop: $(href).offset().top
		    }, 2000);
		});

	});
</script>
</body>
</html>
