<?php if(!empty($submission)) :?>
	<?php foreach($submission as $s):?>
		<div class="col-md-3 post">
			<a href="<?php echo $s->instagramlink?>" target="_blank">
				<img src="/uploads/<?php echo $s->thumbnail;?>" class="img-responsive" alt="<?php echo $s->namalengkap?>">
			
			<div class="onhover">
				<p class="name"><?php echo $s->namalengkap?></p>
				<p class="profesi"><?php echo $s->profesi?></p>
				<div class="link">
					<!-- <a href="javascript:;" class="love">
						<i class="fa fa-heart" aria-hidden="true"></i>
					</a> 
					<a href="<?php echo $s->instagramlink?>" target="_blank">
						<i class="fa fa-external-link" aria-hidden="true"></i>
					</a>
					-->
				</div>
			</div>
			</a>
		</div>
	<?php endforeach;?>
	<div class="clear"></div>
	<div id="pagination"><?php echo @$pagination?></div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#pagination').on('click','span',function(e){
		       e.preventDefault(); 
		       var pageno = $(this).children('a').attr('data-ci-pagination-page');
		       loadPagination(pageno);
		     });

			function loadPagination(pagno){
		       $.ajax({
		         url: '/load/'+pagno,
		         type: 'get',
		         dataType: 'html',
		         success: function(response){
		            // $('#pagination').html(response.pagination);
		            // createTable(response.result,response.row);
		            $("#galleries").html(response);
		         }
		       });
		     }
		});
	</script>
<?php endif;?>