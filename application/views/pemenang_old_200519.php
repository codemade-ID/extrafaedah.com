<section class="bg-fill" id="winner">
	<img src="<?php echo base_url()?>assets/img/winner-banner-top-<?php echo $tahap;?>.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<div class="container">
		<div class="tabbing">
			<ul>
				<li class="<?php if($tahap == 1) echo "active";?>"><a href="<?php echo base_url();?>pemenang-1">Tahap 1</a></li>
				<li class="<?php if($tahap == 2) echo "active";?>"><a href="<?php echo base_url();?>pemenang-2">Tahap 2</a></li>
				<li class="<?php if($tahap == 3) echo "active";?>"><a href="<?php echo base_url();?>pemenang-3">Tahap 3</a></li>
				<li class="<?php if($tahap == 4) echo "active";?>"><a href="<?php echo base_url();?>pemenang-4">Tahap 4</a></li>
				<li class="<?php if($tahap == 5) echo "active";?>"><a href="<?php echo base_url();?>pemenang-5">Tahap 5</a></li>
				<li class="<?php if($tahap == 6) echo "active";?>"><a href="<?php echo base_url();?>pemenang-6">Tahap 6</a></li>
				
			</ul>
		</div>
		<style type="text/css">
			.tabbing ul{padding: 0;margin: 10px 0;}
			.tabbing ul li{display: inline-block;padding: 5px 40px;}
			.tabbing ul li.active {border-bottom: 4px solid #FF1659}
			.tabbing ul li.active a{color: #FF1659; font-weight: bold;}
		</style>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-5 pad-30 pad-t pad-b">
					<?php if(!empty($photos)) :?>
					<p class="color-hitam bold text-center font-20">Foto Penyerahan Hadiah</p>
					<div class="row multiple-items h text-center" id="lineup">
						<?php foreach($photos as $p) :?>
						<div class="item">
							<div class="lu-img">
								<img src="<?php echo $p->path;?>"
								 class="img-responsive" style="margin: 0 auto; width: 100%;">
							</div>
						</div>
						<?php endforeach;?>
					</div>
					<?php endif;?>
				</div>
				<div class="col-md-5 pad-30 pad-t pad-b">
					<?php if(!empty($videos)) :?>
					<p class="color-hitam bold text-center font-20">Video Penyerahan Hadiah</p>
					<div class="row multiple-items h text-center" id="lineup">
						<?php foreach($videos as $v) :?>
							<div class="item">
								<div class="lu-img resp-container">
									<iframe class="resp-iframe" width="100%" height="auto" src="<?php echo $v->path;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						<?php endforeach;?>
					</div>
					<?php endif;?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
		<style type="text/css">
			.slick-prev:before{margin-left: 24px!important;}
			.slick-next{right: 16px!important;}
			.slick-prev, .slick-next{z-index: 9}
		</style>
		<script type="text/javascript">
			$(document).ready(function(){
				$("a.fancybox").fancybox();
				$('.multiple-items').slick({
					  dots: false,
					  infinite: false,
					  speed: 300,
					  slidesToShow: 1,
					  slidesToScroll: 1,
					  centerPadding: '60px',
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1,
					        infinite: true,
					        dots: true
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    },
					    {
					      breakpoint: 480,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    }
					    // You can unslick at a given breakpoint now by adding:
					    // settings: "unslick"
					    // instead of a settings object
					  ]
				});
			})
		</script>
		<div class="winner-search pad-30 pad-l pad-r pad-b">
			<div class="row">
				<form action="javascript:;">
					<div class="col-md-11">
						<input type="text" name="q" class="form-control" placeholder="Cari Kode Kupon atau no hp kamu di sini">
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.winner-search form').submit(function(){
						var mdn = $('input[name=q]').val();
						$('.keyword').html('Hasil pencarian "'+ mdn +'"');
						$.post('<?php echo base_url()?>welcome/winner_search', {mdn : mdn, tahap : '<?php echo $tahap;?>'}, function(e){

							$('.thead-result').show();
							if(e == 'null'){
								$('.table-result').hide();
							}else{
								var obj = JSON.parse(e);
								$('.table-result').show();
								$('.result-mdn').text(obj.mdn);
								$('.result-coupon').text(obj.coupon);
								$('.result-prize').text(obj.prize);
							}
							// console.log(e);
						});
					});
				});
			</script>
			<div class="result">
				<p class="keyword"></p>
				<div class="table-responsive">
					<table class="table">
					  <thead class="thead-result" style="display: none;">
					    <tr>
					      <th scope="col">Nomor MDN</th>
					      <th scope="col">Nomor Kupon</th>
					      <th scope="col">Jenis Hadiah</th>
					    </tr>
					  </thead>
					  <tbody class="table-result" style="display: none;">
					    <tr style="background: #EBF3FF">
					      <td class="result-mdn"></td>
					      <td class="result-coupon"></td>
					      <td class="result-prize"></td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
		
			<div class="winner-list pad-30 pad-l pad-r  pad-t">
				<p class="text-center color-hitam font-24 mar-30 mar-l mar-r TruenoBd">Pemenang Hadiah Utama</p>
				<?php if(!empty($rumah)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img">
							<img src="<?php echo base_url()?>assets/img/winner-rumah.png" class="">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">1 Rumah BSD</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php foreach($rumah as $p) :?>
								    <tr class="utama">
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($mobil)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img">
							<img src="<?php echo base_url()?>assets/img/winner-mobil.png" class="">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">1 Mobil Innova</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody>
							    <?php foreach($mobil as $p) :?>
								    <tr class="utama">
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($vespa)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img">
							<img src="<?php echo base_url()?>assets/img/winner-vespa.png" class="">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">3 Motor Vespa
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="vespa"><i class="fa fa-angle-down"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="vespa">
							    <?php $i=1;foreach($vespa as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<p class="text-center color-hitam font-24 mar-30 mar-l mar-r TruenoBd">Daftar Semua Pemenang</p>
				<?php if(!empty($samsungnote10)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian3/Note-01.png" style="width: 30%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">5 Samsung Note 10
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="samsungnote10"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="samsungnote10" style="display: none;">
							    <?php $i=1;foreach($samsungnote10 as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($iphone)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-iphone.png" style="width: 30%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">5 Unit iPhone XS
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="iphone"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="iphone" style="display: none;">
							    <?php $i=1;foreach($iphone as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($iphonexr)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian2/hadiah-5-iphone-xs.png" style="width: 30%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">5 Unit iPhone XR
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="iphone"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="iphone" style="display: none;">
							    <?php $i=1;foreach($iphonexr as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($bali)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-bali.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">10 Paket Liburan
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="bali"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="bali" style="display: none;">
							    <?php $i=1;foreach($bali as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($tabungansimas5)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian3/Simas.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">10 Tabungan Sinarmas @Rp. 5.000.000
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="tabungansimas5"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="tabungansimas5" style="display: none;">
							    <?php $i=1;foreach($tabungansimas5 as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				
				<?php if(!empty($samsung)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-samsung.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">60 Unit Samsung A50
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="samsung"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="samsung" style="display: none;">
							    <?php $i=1;foreach($samsung as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($samsunga10s)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian2/hadiah-60-samsung.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">50 Unit Samsung A10s
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="samsung"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="samsung" style="display: none;">
							    <?php $i=1;foreach($samsunga10s as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				
				<?php if(!empty($speaker)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-speaker.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">120 Unit Bluetooth Speaker Swarovsky
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="speaker"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="speaker" style="display: none;">
							    <?php $i=1;foreach($speaker as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($speakerjbl)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian2/hadiah-120-speaker.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">100 Unit Bluetooth Speaker JBL GO
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="speaker"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="speaker" style="display: none;">
							    <?php $i=1;foreach($speakerjbl as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				
				<?php if(!empty($voucher)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-voucher.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">300 Voucher Pulsa 500 Ribu
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="voucher"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="voucher" style="display: none;">
							    <?php $i=1;foreach($voucher as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($voucher300rb)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian2/hadiah-300-pulsa.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">300 Voucher Pulsa 300 Ribu
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="voucher"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="voucher" style="display: none;">
							    <?php $i=1;foreach($voucher300rb as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($voucher250rb)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian3/voucher.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">450 Voucher Pulsa 250RB
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="voucher250rb"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="voucher250rb" style="display: none;">
							    <?php $i=1;foreach($voucher250rb as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				
				<?php if(!empty($belanja)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-belanja.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">500 Voucher Belanja 200 Ribu
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="belanja"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="belanja" style="display: none;">
							    <?php $i=1;foreach($belanja as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($belanja200rb)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/winner-belanja.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">530 Voucher Belanja 200 Ribu
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="belanja"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="belanja" style="display: none;">
							    <?php $i=1;foreach($belanja200rb as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				<?php if(!empty($vouchertoped100)) :?>
				<div class="row">
					<div class="col-md-3">
						<div class="win-img small">
							<img src="<?php echo base_url()?>assets/img/undian3/Belanja.png" style="width: 35%">
						</div>
					</div>
					<div class="col-md-9">
						<div class="text-center head">1,000 Voucher Belanja Tokopedia 100RB
							<span class="block-arrow">
								<a href="javascript:;" class="arrow" rel="vouchertoped100"><i class="fa fa-angle-right"></i></a>
							</span>
						</div>
						<div class="table-responsive">
							<table class="table">
							  <thead>
							    <tr class="utama three">
							      <th scope="col"></th>
							      <th scope="col">Nomor MDN</th>
							      <th scope="col">Nomor Kupon</th>
							    </tr>
							  </thead>
							  <tbody class="vouchertoped100" style="display: none;">
							    <?php $i=1;foreach($vouchertoped100 as $p) :?>
								    <tr class="utama three">
								    	<td scope="row"><?php echo $i;?></td>
								      <td scope="row"><?php echo ccMasking($p->mdn);?></td>
								      <td><?php echo $p->coupon;?></td>
								    </tr>
								<?php $i++;endforeach;?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<?php endif;?>
				
				<div class="text-center font-18 pad-50 pad-t pad-b">
					<p style="color: #242325">SELURUH PEMENANG AKAN DIHUBUNGI SECARA LANGSUNG OLEH PIHAK SMARTREN MELALUI NOMOR RESMI 888 ATAU 0888 818 8888</p>
					<p style="color: #ff1659">HATI-HATI TERHADAP SEGALA BENTUK PENIPUAN YANG MENGATASNAMAKAN SMARTFREN!</p>
				</div>
			</div>
		
	</div>
</section>
<section class="bg-white">
	<div class="container">
		<p class="text-center color-hitam font-24">OFFICIAL PARTNER</p>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-2 text-center mar-10 mar-r mar-l">
				<img src="<?php echo base_url()?>assets/img/sinarmasland.png" class="" style="height: 50px">
			</div>
			<div class="col-md-2 text-center mar-10 mar-r mar-l">
				<img src="<?php echo base_url()?>assets/img/bsdcity.png" class="" style="height: 50px">
			</div>
			<!-- <div class="col-md-2 text-center mar-10 mar-r mar-l">
				<img src="<?php echo base_url()?>assets/img/blibli.png" class="" style="height: 50px">
			</div> -->
			<div class="col-md-4"></div>
			
		</div>
	</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$('a.arrow').each(function(){
			$(this).on("click", function(){
				var athis = $(this);
				var rel = $(this).attr('rel');
				console.log(rel);
				var tabler = $('.'+rel);
				tabler.slideToggle("fast", function(){
	                // check paragraph once toggle effect is completed
	                if($(tabler).is(":visible")){
	                    $(athis).html('<i class="fa fa-angle-down"></i>');
	                } else{
	                    $(athis).html('<i class="fa fa-angle-right"></i>');
	                }
	            });
			});
		});
		
	});
	

</script>
<style type="text/css">
/*.container{width: 980px;}*/

</style>