<h1 class="text-center color-merah-sf">Line Up</h1>
<div class="row">
	<?php foreach($lineup as $lu) :?>
		<div class="col-md-3 text-center">
			<div class="lu-img">
				<img src="<?php echo $lu['image']?>" class="img-responsive">
			</div>
			<p class="lu-name  color-merah-sf"><?php echo $lu['name']?></p>
		</div>
	<?php endforeach;?>
</div>