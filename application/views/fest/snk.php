<div class="color-putih">
<p>SYARAT &amp; KETENTUAN SMARTFREN WOW FEST.</p>
<ol start="1" type="1">
<li>Tiket Smartfren WOW Fest bisa didapatkan di:</li>
<ol start="1" type="a">
<li>KIOSTIX, www.kiostix.com</li>
<li>Galeri Smartfren Sabang, Mall Ambassador, Kelapa Gading, Mall Metropolitan Bekasi & Botani Square Bogor</li>
<li>Menukarkan 2,000 SmartPoin di aplikasi MySmartfren</li>
</ol>
</ol>
<ol start="2" type="1">
<li>Pengunjung akan mendapatkan e-ticket setelah berhasil melakukan pembelian online di Kiostix atau setelah menukarkan SmartPoin melalui aplikasi MySmartfren.</li>
</ol>
<ol start="3" type="1">
<li>e-ticket wajib ditukarkan dengan Wristband sebagai tanda masuk dan wajib dipakai selama Smartfren WOW Fest berlangsung dan berlaku untuk 1 hari (re-entry).</li>
<li>Disarankan untuk mengenakan Wristband&nbsp; sebelum memasuki area Event Smartfren WOW Fest.</li>
<li>Pastikan Wristband dipasang cukup ketat sehingga tidak mudah lepas dari tangan.</li>
<li>Panitia tidak bertanggung jawab apabila pengunjung melepas, merusak atau kehilangan Wristband, maka akan dianggap batal dan tidak akan digantikan dengan Wristband yang baru.</li>
<li>Pengunjung dibawah usia 10 tahun disarankan untuk didampingi oleh orang tua/wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku yang berlaku.</li>
<li>Pengunjung dibawah usia 5 tahun atau tinggi dibawah 110 cm bebas biaya masuk.</li>
<li>Pengunjung membebaskan Smartfren sebagai penyelenggara acara dari segala tuntutan yang ditimbulkan pada saat penyelenggaraan acara kecuali hal-hal yang menjadi tanggung jawab penyelenggaraan acara (Force Majeure).</li>
<li>Segala kerusakan dan kehilangan barang pribadi bukan menjadi tanggung jawab penyelenggara.</li>
<li>Dilarang membuat atau memicu hal yang dapat menimbulkan keributan, kekerasan atau yang sifatnya perusakan.</li>
<li>Pertunjukan sewaktu-waktu dapat diberhentikan oleh pemerintah atau negara tanpa pemberitahuan.</li>
<li>Dilarang membawa makanan &amp; minuman dari luar, kamera / video professional, drone, laser pen, obat obatan terlarang, senjata tajam &amp; barang-barang berbahaya lainnya.</li>
</ol>
</div>