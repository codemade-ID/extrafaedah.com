
<p>FAQ WOW CONCERT.</p>
<p>INFO TIKET</p>
<ol>
<li>Apa yang harus saya lakukan untuk mendapatkan tiket Smartfren WOW Fest ini?</li>
</ol>
<ul>
<li>Kamu bisa membeli tiket secara online melalui <a href="http://www.kiostix.com">www.kiostix.com</a></li>
<li>Membeli produk Smartfren min Rp 200.000 di Galeri Smartfren Sabang, Mall Ambassador &amp; Kelapa Gading</li>
<li>Tukarkan 2,000 SmartPoin pada aplikasi MySmartfren</li>
</ul>
<ol start="2">
<li>Berapa harga tiket Smartfren WOW Fest?</li>
</ol>
<ul>
<li>WOW Jomblo Rp 200.000 (1 Tiket)</li>
<li>WOW Couple Rp 350.000 (2 Tiket)</li>
<li>WOW Squad Rp 650.000 (4 Tiket)</li>
</ul>
<p>*Tiket sudah termasuk biaya masuk ancol per orang (terkecuali untuk pembelian langsung di venue (on the spot) &amp; tidak termasuk kendaraan)</p>
<ol start="3">
<li>Apakah saya tetap dapat membeli tiket jika saya tidak pengguna Smartfren ?</li>
</ol>
<p>Tentu bisa, Namun untuk pengguna Smartfren akan mendapatkan keuntungan lebih diacara ini. Pengunjung yang belum menggunakan produk Smartfren dapat membeli di booth Smartfren yang tersedia untuk mendapatkan kejutan dari Smartfren.</p>
<ol start="4">
<li>Kapan penukaran Wristband dilakukan?</li>
</ol>
<p>Wristband dapat ditukarkan pada tanggal 8 &amp; 9 November 2019 di Galeri Smartfren Sabang mulai pukul 08.00 &ndash; 17.00 WIB atau tanggal 10 November 2019 di Ecovention Ancol mulai pukul 09.00 WIB.</p>
<ol start="5">
<li>Apa saja yang diperlukan untuk penukaran Wristband?</li>
</ol>
<ul>
<li>Tunjukkan e-ticket &amp; Kartu Identitas (KTP/SIM/PASSPOR/KITAS)&nbsp; yang berlaku dan didaftarkan pada saat membeli tiket</li>
<li>Pengambilan Wristband hanya dapat diambil oleh orang yang terdaftar &amp; jika diwakilkan wajib disertai dengan surat kuasa asli disertakan dengan Kartu Identitas pembeli &amp; Kartu Identitas yang diwakilkan.</li>
</ul>
<ol start="6">
<li>Apakah ada batasan untuk jumlah pembelian tiket?</li>
</ol>
<p>Untuk 1 Kartu Identitas maksimal hanya dapat membeli 4 tiket.</p>
<ol start="7">
<li>Berapa jumlah SmartPoin yang harus ditukarkan untuk mendapatkan tiket masuk Smartfren WOW Fest?</li>
</ol>
<p>Kamu bisa menukarkan 2,000 SmartPoin.</p>
<ol start="8">
<li>Bagaimana cara menukarkan SmartPoin?</li>
</ol>
<p>Masuk ke aplikasi MySmartfren dan pilih SmartPoin, pilih tukar SmartPoin, pilih banner Smartfren WOW Fest dan pilih menu Kupon untuk melihan e-voucher/e-ticket.</p>
<ol start="9">
<li>Apakah e-voucher akan langsung saya terima di hari yang sama?</li>
</ol>
<p>Iya, voucher/e-ticket akan diterima pada hari yang sama maksimum 1x24 jam.</p>
<ol start="10">
<li>Apakah dengan voucher ini saya bisa langsung datang ke acaranya?</li>
</ol>
<p>Untuk voucher/e-ticket ini harus ditukarkan terlebih dahulu dengan Wristband pada tanggal 8 &amp; 9 November 2019 di Galeri Smartfren Sabang mulai pukul 08.00 &ndash; 17.00 WIB atau tanggal 10 November 2019 di Ecovention Ancol mulai pukul 09.00 WIB.</p>
<p></p>
<p>INFO GENERAL ACARA.</p>
<ol>
<li>Dimana acara ini akan diadakan?</li>
</ol>
<p>Acara akan diadakan di Ecovention Ancol.</p>
<ol start="2">
<li>Kapan acara ini akan dilangsungkan?</li>
</ol>
<p>Acara akan dilangsungkan pada Minggu, 10 November 2019.</p>
<ol start="3">
<li>Jam berapa pengunjung diperbolehkan memasuki area acara?</li>
</ol>
<p>Gate akan dibuka pada pukul 10.00 WIB</p>
<ol start="4">
<li>Apa saja persyaratan untuk masuk ke area acara?</li>
</ol>
<p>Membawa &amp; menggunakan Wristband yang sudah ditukarkan serta dilarang untuk membawa barang-barang seperti, makanan &amp; minuman dari luar, kamera / video professional, drone, laser pen, obat obatan terlarang, senjata tajam &amp; barang-barang berbahaya lainnya</p>
<ol start="5">
<li>Bagaimana jika saya tidak menggunakan nomor Smartfren?</li>
</ol>
<p>Kamu tetap bisa menikmati acara ini namun untuk pengunjung yang tidak memiliki nomor Smartfren, tidak bisa menikmati kejutan-kejutan WOW dari Smartfren.</p>
<ol start="6">
<li>Saya tidak menggunakan nomor Smartfren pada hape, namun saya menggunakan Modem WiFi Smartfren. Apakah bisa masuk ke dalam acara?</li>
</ol>
<p>Pengguna Modem WiFi Smartfren diperbolehkan masuk, selama dalam kondisi aktif dan bisa digunakan.</p>
<ol start="7">
<li>Apakah masuk ke dalam acara first come first serve?</li>
</ol>
<p>Ya, kamu akam masuk dengan cara first come first serve. Jadi kamu jangan datang terlambat.</p>
<ol start="8">
<li>Apakah ada batasan umur untuk masuk ke acara ini?</li>
</ol>
<p>Tidak ada, dihimbau untuk anak-anak dibawah umur 10 tahun wajib didampingi orang tua atau wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku</p>
<ol start="9">
<li>Apabila saya harus keluar dari area acara, apakah diperbolehkan masuk kembali?</li>
</ol>
<p>Kamu diperbolehkan masuk kembali ke area acara selama kamu tetap menggunakan Wristband tanpa kerusakan.</p>
<ol start="10">
<li>Apakah ada persyaratan pakaian yang akan dikenakan?</li>
</ol>
<p>Silahkan mengenakan pakaian senyaman mungkin selama dalam batasan kewajaran.</p>
<ol start="11">
<li>Bagaimana saya bisa mendapatkan informasi yang resmi pada hari menjelang acara?</li>
</ol>
<p>Kamu bisa mendapatkan informasi yang resmi dan paling baru dari situs <a href="http://www.smartfren.com/wowconcert" aria-invalid="true">www.smartfren.com/wow</a>/fest dan akun Instagram @smartfrenworld.</p>
<ol start="12">
<li>Apakah akan disediakan tempat penitipan barang?</li>
</ol>
<p>Kami tidak menyediakan penitipan barang, Jadi sebaiknya membawa barang-barang yang diperlukan saja.</p>
<ol start="13">
<li>Bagaimana cara menuju lokasi Acara?</li>
</ol>
<ul>
<li>Pengguna Transjakarta:</li>
</ul>
<ul>
<li style="list-style: none;">
<ul style="list-style-type: circle;">
<li>Halte Ancol, masuk melalui pintu Transjakarta kemudian dapat menggunakan shuttle / kereta wara wiri &amp; turun di Ecovention.</li>
</ul>
</li>
</ul>
<ul>
<li>Pengguna Kendaraan Pribadi:
<ul style="list-style-type: circle;">
<li>Kamu dapat masuk melewati semua akses pintu masuk Ancol yang tersedia.</li>
<li>Untuk akses masuk terdekat menuju ecovention, kamu dapat melewati pintu masuk utama.</li>
</ul>
</li>
</ul>
<p>INFO WRISTBAND.</p>
<ol>
<li>Bagaimana cara menggunakan Wristband?</li>
</ol>
<p>Tata cara penggunaan Wristband dapat kamu lihat dan pelajari di kemasan yang telah diberikan &amp; kamu diwajibkan menggunakan Wristband ini selama acara berlangsung karena kami berhak untuk mengeluarkan pengunjung yang tidak menggunakan Wristand di dalam area event.</p>
<ol start="2">
<li>Bagaimana jika Wristband saya hilang atau rusak?</li>
</ol>
<p>Wristband yang sudah diberikan kepada kamu sepenuhnya menjadi tanggung jawab kamu. Kami tidak bertanggung jawab atas kerusakan maupun kehilangan wristband tersebut. Kami sarankan untuk menjaga baik-baik wristband masing-masing sampai dengan acara berakhir</p>
<ol start="3">
<li>Saya memasang Wristband terlalu ketat, apakah penyelenggara bisa membantu untuk melonggarkannya?</li>
</ol>
<p>Wristband yang sudah diberikan kepada kamu maka akan menjadi tanggung jawab kamu sepenuhnya. Sebagai penyelenggara, kami tidak dapat melakukan apapun jika Wristband yang kamu kenakan terlalu ketat.</p>
<ol start="4">
<li>Bisakah Wristband saya dipakai orang lain?</li>
</ol>
<p>Bisa, selama kamu belum memasangkan Wristband ditangan kamu.</p>
<p>INFO BELONGINGS.</p>
<ol>
<li>Apakah diperbolehkan membawa kamera ke area konser?</li>
</ol>
<p>Foto dan video hanya bisa diambil melalui telepon genggam. Kamera professional dan alat perekam video dan suara (termasuk kamera dengan lensa panjang atau bisa dilepas/pasang/diganti, perangkat sejenis GoPro) dilarang dibawa ke area konser.</p>
<ol start="2">
<li>Apakah saya diperbolehkan membawa obat-obatan pribadi ke area acara?</li>
</ol>
<p>Hanya obat-obatan dengan resep dokter. Pastikan kamu membawa resep tersebut agar obat-obatan yang kamu bawa dapat di cek kepentingannya. Tidak diperbolehkan untuk membawa obat-obatan terlarang jenis apapun ke dalam area acara.</p>
<ol start="3">
<li>Barang apa saja yang tidak boleh saya bawa ke dalam area acara?</li>
</ol>
<ul>
<li>Bendera/Poster berukuran besar</li>
<li>Drone, pesawat dengan remot control, mainan dan sejenisnya</li>
<li>Obat-obatan terlarang dan sejenisnya</li>
<li>Benda mudah terbakar</li>
<li>Senjata/benda tajam</li>
<li>Makanan dan minuman dari luar</li>
<li>Binatang/hewan peliharaan</li>
<li>Rantai/asesoris tajam/sabuk besi</li>
<li>Terompet/alat musik/alat pembuat suara</li>
<li>Kamera dengan lensa professional</li>
</ul>
<ol start="4">
<li>Bagaimana jika saya membawa salah satu barang yang tidak diperbolehkan tersebut?</li>
</ol>
<p>Barang-barang yang dilarang tersebut akan langsung dibuang karena kami tidak menyediakan tempat penitipan barang. Jadi, tolong dipastikan untuk tidak membawa barang terlarang tersebut.</p>
<p>INFO TENANTS.</p>
<ol start="1" type="1">
<li>Tenant apa saja yang tersedia di dalam area acara?</li>
</ol>
<p>Tenant yang kami sediakan di dalam area acara:</p>
<ul>
<li>F&amp;B tenant</li>
<li>Bobba Tenant</li>
<li>Food Truck</li>
</ul>
<ol start="2" type="1">
<li>Bagaimana cara pembayaran untuk belanja pada tenant-tenant tersebut?</li>
</ol>
<p>Kamu dapat belanja menggunakan uang tunai atau menukarkan SmartPoin yang ada pada aplikasi MySmartfren kamu.</p>
<ol start="3" type="1">
<li>Bagaimana cara menggunakan SmartPoin untuk belanja pada tenant-tenant tersebut?</li>
</ol>
<p>Kamu masuk ke aplikasi MySmartfren dan pilih SmartPoin. Setelah itu kamu pilih pilih banner tenant yang kamu inginkan</p>
<ol start="4" type="1">
<li>Bagaimana jika saya tidak memiliki SmartPoin pada aplikasi MySmartfren saya?</li>
</ol>
<p>Jika kamu tidak memiliki SmartPoin, kamu bisa melalukan pembelian paket pada aplikasi MySmartfren atau kamu bisa melakukan pembayaran secara tunai dengan harga normal yang tertera pada tenant tersebut.</p>
<ol start="5" type="1">
<li>Berapa lama SmartPoin akan masuk ke aplikasi MySmartfren saya?</li>
</ol>
<p>SmartPoin akan kamu terima&nbsp; dalam jangka waktu 10 &ndash; 30 menit.</p>
<ol start="6" type="1">
<li>Apa kelebihan jika saya belanja menggunakan SmartPoin?</li>
</ol>
<p>Kamu akan mendapatkan potongan harga dan promo WOW lainnya.</p>
<ol start="7" type="1">
<li>Apakah ada batasan jumlah pembelanjaan saya menggunakan SmartPoin?</li>
</ol>
<p>Kamu diperbolehkan melakukan pembelanjaan menggunakan SmartPoin 1 kali untuk setiap tenant yang tersedia.</p>
<p>&nbsp;</p>