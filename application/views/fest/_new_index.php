<section id="video" class="fest bg-red-sf">
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">
				<img src="<?php echo base_url()?>assets/img/wow_fest_logo.png" class="pad-30">
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center pad-30 pad-t">
				<iframe width="100%" height="400" src="https://www.youtube.com/embed/ILZpDmMfb-c?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>		
</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	
	<div class="center">
		
	</div>
	<img src="<?php echo base_url()?>assets/img/wow_fest_bg_kv.png" class="bg-kv">
</section>
<section id="time" class="fest" style="display: block;">
	<div class="container">
		<div class="">
			<img src="<?php echo base_url()?>assets/img/wow_fest_ready.png" class="img-responsive center">
			<div id="demo" class="board">
				<div class="row">
					<div class="col-md-4 text-center" id="days"></div>
					<div class="col-md-4 text-center" id="hours"></div>
					<div class="col-md-4 text-center" id="minutes"></div>
				</div>
			</div>
			<script>
		        // Set the date we're counting down to
		        let countDownDate = new Date("Nov 10, 2019 00:00:00").getTime();

		        // Update the count down every 1 second
		        let x = setInterval(function () {

		          // Get today's date and time
		          let now = new Date().getTime();

		          // Find the distance between now and the count down date
		          let distance = countDownDate - now;

		          // Time calculations for days, hours, minutes and seconds
		          let days = Math.floor(distance / (1000 * 60 * 60 * 24));
		          days > 9 ? days : days = '0' + days;
		          let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		          hours > 9 ? hours : hours = '0' + hours;
		          let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		          minutes > 9 ? minutes : minutes = '0' + minutes;
		          let seconds = Math.floor((distance % (1000 * 60)) / 1000);

		          // Display the result in the element with id="demo"
		          // document.getElementById("demo").innerHTML = days + " " + hours + " " + minutes;
		          document.getElementById("days").innerHTML = days;
		          document.getElementById("hours").innerHTML = hours;
		          document.getElementById("minutes").innerHTML = minutes;


		          // If the count down is finished, write some text

		          if (distance < 0) {
		            clearInterval(x);
		            $('#time').hide();
		            // document.getElementById("demo").innerHTML = "WOW";
		          }
		        }, 1000);
		      </script>

		</div>
		
	</div>
</section>
<section id="lineup" class="fest">
	<script type="text/javascript">
			$(document).ready(function(){
				$('.multiple-items').slick({
					  dots: false,
					  infinite: false,
					  speed: 300,
					  slidesToShow: 4,
					  slidesToScroll: 1,
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 1,
					        infinite: true,
					        dots: true
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    },
					    {
					      breakpoint: 480,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    }
					    // You can unslick at a given breakpoint now by adding:
					    // settings: "unslick"
					    // instead of a settings object
					  ]
				});
			})
			
		</script>
		<div class="container">
			<div class="row">
				<h4 class="text-center">Line Up</h4>
				<div class="col-sm-6 text-left"></div>
				<div class="col-sm-6 text-right"><a href="javascript:;" class="lu"  data-toggle="modal" data-target="#myLU">View All</a></div>
				<div class="row multiple-items h text-center" id="lineup">
					<?php foreach($lineup as $lu) :?>
						<div class="item">
							<div class="lu-img text-center">
								<img src="<?php echo $lu['image']?>" class="" height="200">
							</div>
							<p class="lu-name"><?php echo $lu['name']?></p>
						</div>
					<?php endforeach;?>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="myLU" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <?php $this->load->view('fest/lineup');?>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
</section>
<section id="ticket" class="fest">
	<h1 class="text-center">Get Your Ticket Here!</h1>
	<div class="container">
		<div class="row">
			<div class="col-md-4 text-center">
				<img src="<?php echo base_url()?>assets/img/fest-produk.png">
				<p class="font-20">Buy Smartfren Products min 200K</p>
			</div>
			<div class="col-md-4 text-center">
				<img src="<?php echo base_url()?>assets/img/fest-smartpoin.png">
				<p class="font-20">Redeem Your 2.000 SmartPoin on MySmartfren Apps</p>
			</div>
			<div class="col-md-4 text-center">
				<img src="<?php echo base_url()?>assets/img/fest-kiostix.png">
				<p class="font-20">Buy Ticket on Kios Tix</p>
				<p> 
					<ul>
						<li class="bold">WOW Jomblo : 200K</li>
						<li class="bold">WOW Couple : 350K</li>
						<li class="bold">WOW Squad : 650K</li>
					</ul>
				</p>
				<a href="https://kiostix.com/id/event/715/smartfren-wow-fest" target="_blank" class="btn btn-info color-putih">Buy Now</a>
				<p></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 box">
				<div class="col-md-12 head text-center">
					<span>Wristband Redemption only at:</span>
				</div>
				<div class="col-md-6 text-center body">
					<p class="bold">Smartfren Gallery Sabang</p>
					<p>(8 - 9 November 2019, 08.00 - 17.00 WIB)</p>
				</div>
				<div class="col-md-6 text-center body">
					<p class="bold">Venue</p>
					<p>10 November 2019, 09.00 - 17.00</p>
				</div>
			</div>
			<div class="col-md-1"></div>
			
		</div>
	</div>
</section>

<section id="activity" class="fest">
	<h1 class="text-center">Activity</h1>
	<div class="container">
		<div class="row mar-40 mar-t mar-b">
			<?php foreach($activity as $act) :?>
				<div class="col-md-3 text-center">
					<div class="lu-img">
						<?php if($act['name'] == 'WOW Talks'):?>
						<a href="javascript" data-toggle="modal" data-target="#WOWTalks">
							<img src="<?php echo $act['image']?>" class="img-responsive">
						</a>
						<?php elseif($act['name'] == 'WOW Foodie'): ?>
						<a href="javascript" data-toggle="modal" data-target="#WOWfoodie">
							<img src="<?php echo $act['image']?>" class="img-responsive">
						</a>
						<?php else:?>
							<img src="<?php echo $act['image']?>" class="img-responsive">
						<?php endif;?>
					</div>
					<p class="lu-name"><?php echo $act['name']?></p>
				</div>
			<?php endforeach;?>
			
		</div>
		<div class="text-center others">
			Others WOW Experiences
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="WOWTalks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <h1 class="text-center color-merah-sf">WOW Talk</h1>
			<div class="row center">
				<div class="col-md-6">
					<img src="<?php echo base_url()?>assets/img/ATTA.png" class="img-responsive">
					<div class="text-center color-merah-sf font-16">Atta Halilintar</div>
				</div>
				<div class="col-md-6">
					<img src="<?php echo base_url()?>assets/img/SAAIH.png" class="img-responsive">
					<div class="text-center color-merah-sf font-16">Saaih Halilintar</div>
				</div>
				<div class="col-md-6">
					<img src="<?php echo base_url()?>assets/img/KAESANG.png" class="img-responsive">
					<div class="text-center color-merah-sf font-16">Kaesang Pangarep</div>
				</div>
				<div class="col-md-6">
					<!-- <img src="<?php echo base_url()?>assets/img/KAESANG.png" class="img-responsive"> -->
					<div class="text-center color-merah-sf font-16">Ernest Prakasa</div>
				</div>
				
			</div>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="WOWfoodie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<style type="text/css">
			#WOWfoodie .col-md-3 .lu-img{min-height: 144px;}
			#WOWfoodie .col-md-3 .lu-img img{border: 0}
			#WOWfoodie .col-md-3 p{text-transform: capitalize;}
		</style>
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <h1 class="text-center color-merah-sf">WOW Foodie</h1>
			<div class="row center">
				<?php foreach($tenants as $tenant) :?>
					<div class="col-md-3 text-center">
						<div class="lu-img">
							<img src="<?php echo str_replace(' ', '', $tenant['image'])?>" class="img-responsive">
						</div>
						<p class="lu-name color-merah-sf"><?php echo ucwords($tenant['name'])?></p>
					</div>
				<?php endforeach;?>
			</div>
	      </div>
	    </div>
	  </div>
	</div>

</section>
<section id="venue" class="fest">
	<h1 class="text-center">Venue</h1>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="uppercase">Ecovention Ancol</h3>
				<p>Jl. Lodan Timur No.7, RW.10, Ancol, Kec. Pademangan, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14430</p>
				<br />
				<h3>10 November 2019</h3>
				<p>10:00 - 21:00 WIB</p>
				<h3 class="font-20">Free Shuttle at entrance gates</h3>
				<a href="https://www.google.com/maps/place/Ecovention/@-6.127283,106.837166,16z/data=!4m5!3m4!1s0x0:0xd20135952a88088d!8m2!3d-6.1272829!4d106.8371664?hl=id" target="_blank" class="btn btn-danger color-putih">Detail Map</a>
			</div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3967.0254097943634!2d106.83497234996992!3d-6.127282895541696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1e19a1996b81%3A0xd20135952a88088d!2sEcovention!5e0!3m2!1sid!2sid!4v1572017607921!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</section>
<style type="text/css">
	.card{background: #fff; color: #595959}
</style>
<section id="home" class="bg-red-sf">
	<div class="container">
		<div class="accordion myaccordion" id="accordionExample" style="margin: 0;">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo">
		          FAQ WOW Fest
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol,.card-body ul{margin-left: 30px;}
		    	.myaccordion .card-body p{margin-left: 72px;}
		    </style>
		    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<?php $this->load->view('fest/faq');?>
		      </div>
		    </div>
		  </div>

		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		          S&K WOW Fest
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol,.card-body ul{margin-left: 30px;}
		    	.myaccordion .card-body p{margin-left: 72px;}
		    </style>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<?php $this->load->view('fest/snk');?>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>
<!-- <section id="collaboration" class="fest bg-fill">
	<h1 class="text-center color-hitam">Collaboration With</h1>
	<div class="container">
		<div class="row">
			<?php $i = 1;while($i<=12):?>
				<div class="col-md-2 text-center mar-10 mar-l mar-r">
					<div class="lu-img">
						<img src="<?php echo base_url()?>assets/img/lineupfest/marionjola.jpg" class="img-responsive">
					</div>
				</div>
			<?php $i++; endwhile;?>
		</div>
	</div>
</section> -->
<!-- <a href="<?php echo base_url()?>how-to-get" class="" style="font-size: 40px;display: block;position: fixed;bottom: 40%; right: 20px;color: #000; z-index: 9999">
	<img src="<?php echo base_url()?>assets/img/flying_button.png">
</a> -->