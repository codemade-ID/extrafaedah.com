<section class="bg-white">
	<div class="container">
		<div class="row mar-30">
			<div class="col-md-4"></div>	
			<div class="col-md-4">
				<img src="<?php echo base_url()?>assets/img/logo-smartfren-wow.png" class="img-responsive image-center mar-30 mar-l mar-r">
			</div>	
			<div class="col-md-4"></div>
		</div>
		<div class="row mar-30">
			<div class="col-md-6">
				<a href="<?php echo base_url()?>undian">
					<img src="<?php echo base_url()?>assets/img/bannerluckydraw.png" class="img-responsive mar-10 mar-r mar-l">
				</a>
			</div>
			<div class="col-md-6">
				<a href="<?php echo base_url()?>concert">
					<img src="<?php echo base_url()?>assets/img/concertbanner-2.png" class="img-responsive mar-10 mar-r mar-l">
				</a>
			</div>
		</div>
		
	</div>
</section>