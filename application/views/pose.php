<section id="gallery">
	<div class="container">
		<h1>Pose Challenge</h1>
		<div class="row">
			<div class="col-md-6">
				<p>Mechanism:</p>
				<ol>
					<li>Follow Instagram @smartfrenworld</li>
					<li>Download Aplikasi MySmartfren</li>
					<li>Share foto dengan gaya WOW (sesuai arahan) di Instagram</li>
					<li>Ceritakan alasan ingin nonton konser WOW di caption dengan #SmartfrenWOW #WOWposeChallenge #WOWconcert</li>
					<li>Tag 3 teman dan @smartfrenworld</li>
				</ol>
			</div>
			<div class="col-md-6">
				<p>Hadiah: <br />
				4 - 9 September 2019<br />
				24 pemenang dengan rincian hadiah :<br />
				<p>
				<ul>
					<li>10 Smartfren #WOWconcert Special Invitation - festival</li>
					<li>38 Smartfren #WOWconcert Special Invitation - tribun</li>
				</ul>
				<p>
				10 - 17 September 2019<br />
				24 pemenang dengan rincian hadiah :<br />
				<p>
				<ul>
					<li>20 Smartfren #WOWconcert Special Invitation - festival</li>
					<li>28 Smartfren #WOWconcert Special Invitation - tribun</li>
				</ul>
			</div>	
			<div class="col-md-12 mar-50 mar-l mar-r">
				<!-- <p class="lu-name">ATEEZ</p> -->
				<div class="pose multiple-items">
					<?php
                  			$dir = "./assets/img/pose/";
							
						    // Open a directory, and read its contents
						    if (is_dir($dir)){
						      if ($dh = opendir($dir)){
						      	$i = 0;
						        while (($file = readdir($dh)) !== false){
						          if ($file != "." && $file != ".." && $file !=".DS_Store") { ?>
						          	<div class="col-md-3 text-center">
											<img src="assets/img/pose/<?php echo $file;?>" class="img-responsive">
									</div>



						          <?php }
						      }
						  }
						}
					?>
				</div>
			</div>
			<script type="text/javascript">
				/**
				 * Created by chase.marcum on 11/13/2014.
				 */
				$(document).ready(function ($) {
				  
				  var gadgetCarousel = $(".multiple-items");
				  
				  gadgetCarousel.each(function() {
				    if ($(this).is(".pose")) {
						$(this).slick({
					          dots: false,
							  infinite: false,
							  speed: 300,
							  slidesToShow: 4,
							  slidesToScroll: 1,
							  responsive: [
							    {
							      breakpoint: 1024,
							      settings: {
							        slidesToShow: 3,
							        slidesToScroll: 3,
							        infinite: true,
							        dots: true
							      }
							    },
							    {
							      breakpoint: 600,
							      settings: {
							        slidesToShow: 2,
							        slidesToScroll: 2
							      }
							    },
							    {
							      breakpoint: 480,
							      settings: {
							        slidesToShow: 1,
							        slidesToScroll: 1
							      }
							    }
							    // You can unslick at a given breakpoint now by adding:
							    // settings: "unslick"
							    // instead of a settings object
							  ]
				      });
				    }else {
				      $(this).slick();
				    }
				  })
				  
				  
				  
				  
				}); // end of os-carousel.js
			</script>
		</div>
		<div class="text-center mar-50">
			<a href="/wow" class="btn btn-back">Back</a>
		</div>
	</div>
</section>

