<!DOCTYPE html>
<html>
<head>
	<title>Deal</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style type="text/css">
		body{padding: 20px;}
	</style>
</head>
<body>
<?php
	$logo = array(
		"-",
		"AceOnline",
		"ShopeeMan",
		"Shopee",
		"Vidio",
		"Informa",
		"Reddoorz",
	);
?>
<form action="" method="post">
  <div class="form-group">
  	<?php foreach($result as $r) :?>
    <label for="exampleFormControlSelect1">
    	<div class="row">
	    	<div class="col-md-3">
	    		<input type="text" name="result[<?php echo $r->id?>][day]" value="<?php echo $r->day;?>">
	    	</div>
	    	<div class="col-md-3">
	    		<input type="text" name="result[<?php echo $r->id?>][text_simple]" value="<?php echo $r->text_simple;?>">
	    	</div>
	    	<div class="col-md-3">
	    		<input type="text" name="result[<?php echo $r->id?>][text]" value="<?php echo $r->text;?>">
	    	</div>
	    </div>
    </label>
    <div class="row">
    	<div class="col-md-3">
    		<select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_1_1]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_1_1 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_1_2]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_1_2 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_1_3]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_1_3 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
    	</div>
    	<div class="col-md-3">
    		<select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_2_1]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_2_1 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_2_2]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_2_2 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_2_3]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_2_3 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
    	</div>
    	<div class="col-md-3">
    		<select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_3_1]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_3_1 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_3_2]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_3_2 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
		    <select class="form-control" id="exampleFormControlSelect1" name="result[<?php echo $r->id?>][row_3_3]">
    			<?php foreach($logo as $l) :?>
    					<option value="<?php echo $l;?>" <?php if($r->row_3_3 == $l) echo "selected";?>><?php echo $l;?></option>
    			<?php endforeach;?>
		    </select>
    	</div>
    	
    </div>
    <?php endforeach;?>
    <div class="row">
    	<br />
    	<button type="submit" class="btn btn-primary mb-2">Save</button>
    </div>
  </div>
</form>
</body>
</html>