<section id="gallery">
	<div class="container">
		<h1>Layanan Antar</h1>
       	<div class="row mar-20 mar-l mar-r">
			<div class="col-sm-12">
				<ul class="">
					<li>Produk dikirim langsung dari Smartfren (https://www.smartfren.com/id/delivery-order/)</li>
					<li>
						Produk dikirim oleh E-commerce Partner:
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-4">
										<a href="https://www.tokopedia.com/smartfren?nref=shphead">
											<img src="<?php echo base_url()?>assets/img/ico_tokped.png">
										</a>
									</div>
									<div class="col-md-4">
										<a href="https://www.bukalapak.com/u/smartfrenofficial">
											<img src="<?php echo base_url()?>assets/img/ico_BL.png">
										</a>
									</div>
									<div class="col-md-4">
										<a href="https://www.blibli.com/merchant/smartfren-official/SMO-56333">
											<img src="<?php echo base_url()?>assets/img/ico_bli.png">
										</a>
									</div>
									<div class="col-md-4">
										<a href="http://m.elevenia.co.id/store/smartfren-official-store">
											<img src="<?php echo base_url()?>assets/img/ico_elvenia.png">
										</a>
									</div>
									<div class="col-md-4">
										<a href="https://shopee.co.id/smartfrenstore?v=2f5&smtt=0.0.3">
											<img src="<?php echo base_url()?>assets/img/ico_shopee.png">
										</a>
									</div>
								</div>
							</div>
						</div>
						
					</li>
				</ul>
				<div class="list">
				</div>
			</div>
		</div>
		
	</div>
</section>
<style type="text/css">
	#gallery .col-md-4{margin: 20px 0}
</style>