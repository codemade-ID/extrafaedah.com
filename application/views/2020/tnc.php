
<div class="ticket">
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		          FAQ & SYARAT  KETENTUAN WOW CONCERT
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol,.card-body ul{margin-left: 0px;}
		    	.myaccordion .card-body p{margin-left: 30px;}
		    </style>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p><strong>FAQ SMARTFREN WOW VIRTUAL CONCERT 2020.</strong></p>
					<ol>
					<li><strong>Kapan acara Smartfren WOW Virtual Concert akan dilangsungkan?</strong></li>
					</ol>
					<p>Acara akan dilangsungkan pada Sabtu, 8 Agustus 2020.</p>
					<ol start="2">
					<li><strong>Dimana Saya dapat menyaksikan acara Smartfren WOW Virtual Concert?</strong></li>
					</ol>
					<p>Smartfren WOW Virtual Concert akan disiarkan secara live di:</p>
					<ul style="list-style-type: square;">
					<li>Vidio.com mulai pukul 18:00 - 20.00 WIB sebagai Pre-Show</li>
					<li>SCTV mulai pukul 20.00 - 22.00 WIB</li>
					</ul>
					<ol start="3">
					<li><strong>Apa Saya dapat datang ke acara Smartfren WOW Virtual Concert?</strong></li>
					</ol>
					<p>Mengikuti arahan pemerintah dan melawan penyebaran Covid-19, Smartfren WOW Virtual Concert tidak menghadirkan para penonton secara langsung. &nbsp;</p>
					<ol start="4">
					<li><strong>Siapa saja Artis International yang akan dihadirkan di Smartfren WOW Virtual Concert?</strong></li>
					</ol>
					<ul style="list-style-type: square;">
					<li>AGNEZ MO</li>
					<li>HONNE (Inggris</li>
					<li>EVERGLOW (K-POP)</li>
					</ul>
					<ol start="5">
					<li><strong>Siapa saja Artis Lokal akan dihadirkan di Smartfren WOW Virtual Concert?</strong></li>
					</ol>
					<ul style="list-style-type: square;">
					<li>ANDI RIANTO</li>
					<li>ARI LASSO</li>
					<li>DEWA 19</li>
					<li>DIPHA BARUS</li>
					<li>GADING MARTEN &amp; GEMPI</li>
					<li>ONCE MEKEL</li>
					<li>RAISA</li>
					<li>TIARA ANDINI</li>
					</ul>
					<p>Dan acara akan dipandu oleh DANIEL MANANTA, LUNA MAYA dan OKKY LUKMAN.</p>
		      </div>
		    </div>
		  </div>
		</div>
			</div>
			<div class="col-md-3"></div>
		</div>
		
	</div>
</div>