<section id="concert-2020" class="digitalactivity">
	<div class="top">
		<?php if($this->ua->is_mobile()) :?>
			<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-talent-m.png" class="" style="margin: 0 auto;max-width: 100%;">
		<?php endif;?>
		<div class="container">
			<div class="row head">
				<div class="col-md-12 text-center">
					<?php if($this->ua->is_mobile()) :?>
						
					<?php else:?>
						<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-talent-desktop.png" class="" style="margin: 0 auto;max-width: 100%;">
					<?php endif;?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-2 text-center">
					<?php if($this->ua->is_mobile()) :?>
						<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-join-m.png" class="" style="margin: 0 auto;max-width: 100%;">
					<?php else:?>
						<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-join.png" class="" style="margin: 0 auto;max-width: 100%;">
					<?php endif;?>
				</div>
				<div class="col-md-5"></div>
			</div>
		</div>
		<div class="container mekanisme">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4 teks">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-text-mekanisme.png" class="" style="margin: 0 auto;max-width: 100%;">
				</div>
				<div class="col-md-4"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-mekanisme.png" class="" style="margin: 0 auto;max-width: 100%;">
				</div>
			</div>
		</div>
		
	</div>
	<div class="middle">
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4 contohnya">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/img-contohnya.png" class="" style="margin: 0 auto;max-width: 100%;">
				</div>
				<div class="col-md-4"></div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/sample-1-1.png" class="img-responsive">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/sample-1-2.png" class="img-responsive">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/sample-2-1.png" class="img-responsive">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/sample-2-2.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
	<div class="foot">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 info">
				<p class="color-blue-2 text-center bold font-18 uppercase">
					Udah upload foto dengan gaya line up favorit kamu? Yuk Registrasi <a class="color-merah" href="https://forms.gle/eJrxi1xHr4aKGpqC8" target="_blank">di sini </a> buat tampil di layar interactive smartfren WOW Virtual Concert
				</p>
				<!-- <p class="text-center">
					<a href="https://forms.gle/eJrxi1xHr4aKGpqC8" class="btn btn-red" target="_blank">REGISTRASI SEKARANG</a>
				</p> -->
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row step">
			<div class="col-md-2">
			</div>
			<div class="col-md-2 text-center ctn">
				<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/step-1.png" class="img-responsive">
				<p>
					Lakukan Registrasi
				</p>
			</div>
			<div class="col-md-2 text-center ctn">
				<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/step-2.png" class="img-responsive">
				<p>
					Email Konfirmasi akan dikirimkan melalui wow@smartfren.com
				</p>
			</div>
			<div class="col-md-2 text-center ctn">
				<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/step-3.png" class="img-responsive">
				<p>
					Wajib Mengikuti Gladi Resik
				</p>
			</div>

			<div class="col-md-2 text-center ctn">
				<img src="<?php echo base_url()?>assets/img/2020/digitalactivity/step-4.png" class="img-responsive">
				<p>
					Selamat Datang di Layar Interaktif 
				</p>
			</div>
			<div class="col-md-2">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 info">
				<!-- <p class="color-blue-2 text-center bold font-18 uppercase">
					Udah upload foto dengan gaya line up favorit kamu? Yuk klik <a class="color-merah" href="https://forms.gle/eJrxi1xHr4aKGpqC8" target="_blank">di sini </a> buat tampil di layar interactive smartfren WOW Virtual Concert
				</p> -->
				<p class="text-center">
					<a href="https://forms.gle/eJrxi1xHr4aKGpqC8" class="btn btn-red" target="_blank">REGISTRASI SEKARANG</a>
				</p>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</section>
<section id="concert-2020-tnc">
	<?php $this->load->view('2020/snk');?>
</section>