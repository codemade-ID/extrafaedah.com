<section id="concert-2020" class="kustik">
	<div class="container top">
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-2 text-center">
				<img src="<?php echo base_url()?>assets/img/2020/kustik/logo.png" class="" style="margin: 0 auto;max-width: 100%;">
				<!-- <div class="date text-center">
					<p class="font-30 color-blue-2 TruenoBd">8 AGUSTUS 2020</p>
				</div> -->
			</div>
			<div class="col-md-5"></div>
		</div>
		<div class="row mar-20 mar-l mar-r">
			<div class="col-md-1"></div>
			<div class="col-md-5 color-blue-2 live TruenoBd">
				LIVE
			</div>
			<div class="col-md-5 live-at">
				<p class="color-blue-2">16:00 - 17:00 WIB</p>
				<p class="color-blue-2">at <a href="https://www.instagram.com/smartfrenworld/" target="_blank" class="color-blue-2">Instagram</a>  & <a href="https://www.facebook.com/smartfren" target="_blank" class="color-blue-2">Facebook</a> Smartfren</p>
				<p class="color-blue-2">at Vidio</p>
			</div>
			<div class="col-md-1"></div>
		</div>
		
	</div>
	<div class="container middle">
		<h2 class="color-merah TruenoBd text-center font-30">SPECIAL PERFORMANCE BY</h2>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_projectpop.png" class="img-responsive">
					<div class="name">
						PROJECT POP <br />
						<span>18 Juli 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_kamasean1.png" class="img-responsive">
					<div class="name  font-16">
						Kamasean, <br />
						Ghea Indrawari & <br />
						Reza Darmawangsa <br />
						<span>25 Juli 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="clear"></div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_armada.png" class="img-responsive">
					<div class="name">
						ARMADA <br />
						<span>1 Agustus 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_raisa.png?v=1" class="img-responsive">
					<div class="name">
						RAISA <br />
						<span>15 Agustus 2020</span>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="clear"></div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_lalahuta.png" class="img-responsive">
					<div class="name">
						LALAHUTA <br />
						<span>22 Agustus 2020</span>
					</div>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<div class="container middle-2">
		<h3 class="color-blue-2 TruenoBd text-center font-26 hosted">HOSTED BY</h3>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/kustik/_boywilliam.png" class="img-responsive">
					<div class="name">
						BOY WILLIAM
					</div>

				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</section>
<style type="text/css">
	.logo-center{display: none;}

</style>