<section id="concert-2020">
	<div class="container top">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<img src="<?php echo base_url()?>assets/img/2020/concert/logo_sf_vc.png" class="" style="margin: 0 auto;max-width: 100%;">
				<div class="date text-center">
					<p class="font-30 color-blue-2 TruenoBd">8 AGUSTUS 2020</p>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-2 color-blue-2 live TruenoBd">
				LIVE
			</div>
			<div class="col-md-4 live-at">
				<p class="color-blue-2">18.00 at Vidio.com - Exclusive Pre-Show Streaming</p>
				<p class="color-blue-2">20.00 at SCTV - National Grand Show Telecast</p>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 video">
				<div class="resp-container" style="width: 80%; margin: 0 auto;">
					<iframe class="resp-iframe" width="100%" height="642" src="https://www.youtube.com/embed/vdgX3smatEQ?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>

		
	</div>

	<div class="container middle">
		<h2 class="color-merah TruenoBd text-center font-30">SPECIAL PERFORMANCE BY</h2>
		<h3 class="color-blue-2 TruenoBd text-center font-26">INTERNATIONAL ARTIST</h3>
		<div class="row">
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_agnesmo.png" class="img-responsive">
					<div class="name">
						AGNEZ MO
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_honne.png" class="img-responsive">
					<div class="name">
						HONNE
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_everglow.png" class="img-responsive">
					<div class="name">
						EVERGLOW
					</div>
				</div>
			</div>
		</div>
		<h3 class="color-blue-2 TruenoBd text-center font-26 indonesian">INDONESIAN ARTIST</h3>
		<div class="row">
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/v1.png" class="vector v1">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_andirianto.png" class="img-responsive">
					<div class="name">
						Andi Rianto
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_arilasso.png" class="img-responsive">
					<div class="name">
						Ari Lasso
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_dewa19.png" class="img-responsive">
					<div class="name">
						Dewa 19
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/v2.png" class="vector v2">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_diphabarus.png" class="img-responsive">
					<div class="name">
						Dipa Barus
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_gadinggempi.png" class="img-responsive">
					<div class="name font-18">
						gading marten <br /> & gempi
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_oncemekel.png" class="img-responsive">
					<div class="name">
						once mekel
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_raisa.png" class="img-responsive">
					<div class="name">
						raisa
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_tiaraandini.png" class="img-responsive">
					<div class="name">
						tiara andini
					</div>
				</div>
			</div>
		</div>
		<h3 class="color-blue-2 TruenoBd text-center font-26 hosted">HOSTED BY</h3>
		<div class="row">
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/v3.png" class="vector v3">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_danielmananta.png" class="img-responsive">
					<div class="name">
						daniel mananta
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_lunamaya.png" class="img-responsive">
					<div class="name">
						luna maya
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/concert/v4.png" class="vector v4">
					<img src="<?php echo base_url()?>assets/img/2020/concert/people/_okklukman.png" class="img-responsive">
					<div class="name">
						okky lukman
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 info">
				<p class="color-blue-2 text-center bold font-18 uppercase">
					Mau tampil di layar interactive smartfren WOW Virtual Concert dan chitchat bareng artis? <br /> Klik <a class="color-merah" href="https://www.smartfren.com/wow/digital-activity"> di sini </a>
				</p>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</section>
<section id="concert-2020-tnc">
	<?php $this->load->view('2020/tnc');?>
</section>
<style type="text/css">
	.logo-center{display: none;}

</style>