
<div class="ticket">
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		          SYARAT DAN KETENTUAN LAYAR INTERACTIVE
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol,.card-body ul{margin-left: 0px;}
		    	.myaccordion .card-body p{margin-left: 30px;}
		    </style>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		      	<p>Konser interaktif smartfren WOW Virtual Concert dapat diikuti secara eksklusif oleh pelanggan smartfren.</p>
				<p>Pelanggan yang dapat menjadi calon Peserta konser interaktif harus terlebih dahulu melakukan registrasi dan mengikuti syarat dan ketentuan berikut:</p>
				<ol>
				<li>Pelanggan pernah melakukan pembelian program bundling undangan smartfren WOW Concert sebelumnya di galeri smartfren, Bukalapak, Creatours dan pemenang kuis dari media sosial smartfren.</li>
				<li>Pelanggan telah mengikuti program loyalty pelanggan yang memang ditujukan untuk dapat menjadi interaktif di smartfren WOW Virtual Concert.</li>
				</ol>
				<p>Selanjutnya, Pelanggan terpilih yang menjadi calon Peserta WAJIB mematuhi persyaratan berikut:</p>
				<ol>
				<li>Peserta WAJIB mengikuti tahapan yang sudah ditentukan di bawah.</li>
				<li>Peserta memastikan data yang dimasukkan dalam registrasi adalah benar dan sesuai.</li>
				<li>Peserta memastikan memiliki perangkat komputer/laptop/ponsel yang berfungsi baik (mikrofon dan kamera) dan tersambung dengan koneksi internet yang memadai demi kelancaran proses interaktif.</li>
				<li>Aplikasi interaktif yang digunakan adalah Zoom. Silakan unduh aplikasi di-link berikut:&nbsp;<a href="https://zoom.us/support/download">https://zoom.us/support/download</a>.</li>
				<li>Selama proses interaktif Peserta harus mengikuti tata krama dan tata acara sebagai berikut:</li>
				</ol>
				<ol style="list-style-type: lower-alpha;">
				<li>Peserta disarankan untuk berada di lokasi yang aman, sesuai dengan anjuran pemerintah selama proses PSBB berlaku.</li>
				<li>Peserta bertanggungjawab atas keselamatan dirinya selama layar interaktif aktif.</li>
				<li>Menggunakan busana yang sopan dan sesuai tema.</li>
				<li>Tema untuk acara live adalah <span style="text-decoration: underline;">B</span><span style="text-decoration: underline;">usana</span><span style="text-decoration: underline;"> </span><span style="text-decoration: underline;">N</span><span style="text-decoration: underline;">usantara </span><span style="text-decoration: underline;">dengan</span><span style="text-decoration: underline;"> </span><span style="text-decoration: underline;">sentuhan</span><span style="text-decoration: underline;"> </span><span style="text-decoration: underline;">Merah</span><span style="text-decoration: underline;"> </span><span style="text-decoration: underline;">dan</span><span style="text-decoration: underline;"> </span><span style="text-decoration: underline;">Putih</span>.</li>
				<li>Peserta mendengarkan dan mengikuti protokol selama acara secara seksama.</li>
				<li>Peserta WAJIB menggunakan nama yang sama dengan registrasi dan di aplikasi Zoom.</li>
				</ol>
				<ol start="6">
				<li>Panitia penyelenggara berhak dan berkekuatan penuh untuk memastikan siapa saja para peserta yang tampil di layar interaktif, demi memastikan kelancaran jalannya acara.</li>
				</ol>
				<p>Link registrasi dibuka sampai dengan 28 Juli 2020.</p>
				<p>Acara ini terselenggara atas kerjasama smartfren dan EMTEK Group. Operasional penyelenggaraan sepenuhnya menjadi tanggungjawab dari EMTEK Group.</p>
		      </div>
		    </div>
		  </div>
		</div>
			</div>
			<div class="col-md-3"></div>
		</div>
		
	</div>
</div>
