<section id="concert-2020" class="podcast">
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-2 text-center">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/logo.png" class="" style="margin: 0 auto;max-width: 100%;">
					<!-- <div class="date text-center">
						<p class="font-30 color-blue-2 TruenoBd">8 AGUSTUS 2020</p>
					</div> -->
				</div>
				<div class="col-md-5"></div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/img_header.png" class="" style="margin: 20px auto 0;max-width: 100%;">
				</div>
				<div class="clear"></div>
				<div class="col-md-12 text-center">
					<p class="color-putih font-18 TruenoBd">19:00 - 20:00 WIB at <a href="https://www.youtube.com/smartfrenworlds" target="_blank" class="color-putih">YouTube smartfren</a></p>
				</div>
				
			</div>
		</div>
		
	</div>
	<div class="container middle">
		<h2 class="color-merah TruenoBd text-center font-30">GUEST STAR</h2>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/_tiaraandini.png" class="img-responsive">
					<div class="name">
						Tiara Andini <br />
						<span>24 Juli 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/_anyageraldine.png" class="img-responsive">
					<div class="name">
						Anya Geraldine<br />
						<span>31 Juli 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="clear"></div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/_cokipardede.png" class="img-responsive">
					<div class="name">
						Coki Pardede <br />
						<span>14 Agustus 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/_rezaarap.png" class="img-responsive">
					<div class="name">
						Reza Arap <br />
						<span>21 Agustus 2020</span>
					</div>

				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="container middle-2">
		<h3 class="color-blue-2 TruenoBd text-center font-26 hosted">HOSTED BY</h3>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="artist">
					<img src="<?php echo base_url()?>assets/img/2020/podcast/_tretanmuslim.png" class="img-responsive">
					<div class="name">
						Tretan Muslim
					</div>

				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</section>
<style type="text/css">
	.logo-center{display: none;}

</style>