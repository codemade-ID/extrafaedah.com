<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php echo @$fb_share;?></title>
	<meta name="description" content="Jangan putus asa, apalagi putus asik! Tanggal 28 Oktober ini kita tunjukin semangat nonstop asik berkreasi! #NonstopKreasiq. More info please check this" />
	<!-- S:OG_FB -->
	<meta property="og:url" content="<?php echo current_url();?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo @$fb_share;?>" />
	<meta name="og:image" content="<?php echo base_url();?>img/pp.png" />
	<meta property="og:site_name" content="smartfren.com" />
	<meta property="og:description" content="Jangan putus asa, apalagi putus asik! Tanggal 28 Oktober ini kita tunjukin semangat nonstop asik berkreasi! #NonstopKreasiq. More info please check this" />
	<meta property="article:author" content="https://www.facebook.com/smartfren" />
	<meta property="article:publisher" content="https://www.facebook.com/smartfren" />
	<meta property="fb:app_id" content="525085630997607" />
	<meta property="fb:pages" content="62225654545" />
	<!-- E:OG_FB -->
	<!-- S:TC -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@smartfrenworld" />
	<meta name="twitter:creator" content="@smartfrenworld">
	<meta name="twitter:title" content="#KreatifTanpaTapi">
	<meta name="twitter:description" content="<?php echo @$tw_share;?>">
	<meta name="twitter:image" content="<?php echo base_url();?>img/pp.png" />
	<!-- E:TC -->

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Gudea:400,400italic,700'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/fonts/stylesheet.css?v=1" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css?v=1.8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/frontend.css?v=2.9" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css?v=2.8" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/grid-2.6/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo-smartfren.png"/>
    
	<link rel="shortcut icon" href="https://www.smartfren.com/static/assets/images/Icon.ico" type="image/x-icon" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21133138-6"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-21133138-6');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NM4WJW6');</script>
	<!-- End Google Tag Manager -->
	<style type="text/css">
		html { 
		  background: url(./assets/img/nonstopkreasiq/bg_teaser.jpg) no-repeat center center fixed; 
		  -webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
		  background-size: cover;
		}
		.outer {
		  display: table;
		  position: absolute;
		  top: 0;
		  left: 0;
		  height: 100%;
		  width: 100%;
		}

		.middle {
		  display: table-cell;
		  vertical-align: middle;
		}

		.inner {
		  margin-left: auto;
		  margin-right: auto;
		  text-align: center;
		  /*whatever width you want*/
		}
		.inner p{line-height: 20px;}
		.logo{width: 150px; margin: 0 auto 20px;}
		.logo-hashtag{width: 500px; margin: 20px auto 0;}
		.countdown{width: 500px;margin: 20px auto; border: 2px solid #fff;border-radius: 10px;font-size: 75px;background: rgba(0,0,0,0.6);padding: 10px 20px;letter-spacing: 0px}
		.countdown span{margin: -5px;display: block;}
		.countdown span.note{margin: 0; font-size: 16px;clear: both;}
		.share p a img{width: 32px;margin: 0 5px}
		@media(max-width: 800px){
			.logo{width: 150px;}
			.logo-hashtag{width: 100%;padding: 0 20px;}
			.countdown{width: 80%;font-size: 45px;margin: 0 auto;}
			.countdown .col-md-3{width: 25%;float: left;}
			.countdown span.note{font-size: 12px;}
			.font-30{font-size: 26px;}
			.font-24{font-size: 20px;}
		}
		
	</style>
	
</head>
<body id="header">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NM4WJW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="outer">
  <div class="middle">
    <div class="inner">
		<div class="logo-hashtag text-center">
			<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/logo_hashtag.png"class="img-responsive">
		</div>
		<div class="text">
			<p class="font-30 TruenoSBd">Sebentar Lagi!</p>
			<p class="font-24 TruenoLt">Tunjukkin Kreasi Asik Kalian Untuk Hadiah Puluhan Juta!</p>
		</div>
		<div class="countdown text-center TruenoExBd" id="clockdiv">
			<div class="row">
				<div class="col-md-3">
					<span class="days">00</span>
					<span class="note">Days</span>
				</div>
				<div class="col-md-3">
					<span class="hours">00</span>
					<span class="note">Hours</span>
				</div>
				<div class="col-md-3">
					<span class="minutes">00</span>
					<span class="note">Minutes</span>
				</div>
				<div class="col-md-3">
					<span class="seconds">00</span>
					<span class="note">Seconds</span>
				</div>
			</div>
			<!-- <span class="days">00</span>
			<span>:</span>
			<span class="hours">00</span>
			<span>:</span>
			<span class="minutes">00</span>
			<span>:</span>
			<span class="seconds">00</span> -->
		</div>

		<div class="share text-center">
			<p>Share ke sosial media biar ga ketinggalan:</p>
			<p>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url();?>" class="facebook">
					<img src="<?php echo base_url();?>assets/img/facebook.png">
				</a>
				<a href="https://twitter.com/intent/tweet?text=Jangan+putus+asa%2C+apalagi+putus+asik%21+Tanggal+28+Oktober+ini+kita+tunjukin+semangat+nonstop+asik+berkreasi%21+%23NonstopKreasiq+%40smartfrenworld%0AMore+info+please+check+this+www.nonstopkreasiq.com+" class="twitter"><img src="<?php echo base_url();?>assets/img/twitter.png"></a>
			</p>
		</div>
		<div class="logo text-center">
			<p>Powered by</p>
			<img src="<?php echo base_url();?>assets/img/LogoNew_Smartfren-white.png" class="img-responsive">
		</div>
    </div>
  </div>
</div>
<script type="text/javascript">
		function getTimeRemaining(endtime) {
		  const total = Date.parse(endtime) - Date.parse(new Date());
		  const seconds = Math.floor((total / 1000) % 60);
		  const minutes = Math.floor((total / 1000 / 60) % 60);
		  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
		  const days = Math.floor(total / (1000 * 60 * 60 * 24));
		  
		  return {
		    total,
		    days,
		    hours,
		    minutes,
		    seconds
		  };
		}

		function initializeClock(id, endtime) {
		  const clock = document.getElementById(id);
		  const daysSpan = clock.querySelector('.days');
		  const hoursSpan = clock.querySelector('.hours');
		  const minutesSpan = clock.querySelector('.minutes');
		  const secondsSpan = clock.querySelector('.seconds');

		  function updateClock() {
		    const t = getTimeRemaining(endtime);

		    daysSpan.innerHTML = t.days;
		    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
		    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
		    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

		    if (t.total <= 0) {
		      clearInterval(timeinterval);
		    }
		  }

		  updateClock();
		  const timeinterval = setInterval(updateClock, 1000);
		}

		// const deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
		const deadline = '28 October 2020 15:00:00 GMT+0700';
		initializeClock('clockdiv', deadline);
	</script>
</body>
</html>
