<!-- Modal -->
<div class="modal fade" id="tnc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 dir="ltr">Ketentuan Umum</h4>
        <ol>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">#NonstopKreasiq merupakan sebuah kegiatan berbasis digital dari smartfren yang dapat diikuti oleh semua kalangan melalui media sosial.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">#NonstopKreasiq diperuntukkan untuk bakat individu bukan kelompok.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Peserta wajib mengunggah di sosial media pribadi dalam bentuk foto, video dan cerita di caption.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Seluruh peserta wajib follow akun Instagram smartfren @smartfrenworld dan menggunakan hashtag #NonstopKreasiq dan #AntiPutusNyambungTerus</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Activity #NonstopKreasiq terbagi menjadi kategori yakni musik dan quotes. Pemenang dari kategori musik berhak untuk mendapatkan hadiah berupa talent partnership bersama Indomusikgram, dan juga uang tunai senilai Rp.7,500,000,- (Juara 1), Rp.5,000,000,- (Juara 2), Rp.2,500,000 (Juara 3), dan Rp.1,000,000,- (Juara Harapan.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Pemenang dari kategori quotes berhak mendapatkan hadiah sebagai berikut, iPhone XS (Juara 1), Samsung A50 (Juara 2, dua orang pemenang), Redmi Note 9 Pro (Juara 3), Redmi 9C (Juara Favorit, 10 orang pemenang), dan Speaker Swarovski (Juara Harapan, 10 orang Pemenang).</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Seluruh submission yang diunggah dengan hashtag #NonstopKreasiq dan #AntiPutusNyambungTerus menjadi hak milik smartfren dan dapat digunakan untuk kebutuhan materi promosi.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Seluruh materi yang diunggah tidak boleh mengandung unsur SARA, vandalisme, kekerasan, politik, dan pornografi.</p>
        </li>
        </ol>
        <p><strong>&nbsp;</strong></p>
        <p dir="ltr">Lain-Lain</p>
        <ol>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Penyelenggara tidak bertanggung jawab atas segala resiko yang terjadi dalam proses pembuatan konten yang dilakukan oleh masing-masing peserta.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Penyelenggara berhak untuk mengungkapkan data diri pemenang dan/atau menggunakan data diri pemenang untuk kepentingan kegiatan atau pun untuk kebutuhan publikasi jika dianggap perlu.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Program ini tidak berlaku untuk karyawan PT. Smartfren Telecom Tbk, agency dan seluruh pihak yang terkait dengan penyelenggaraan program ini.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Penyelenggara berhak mendiskualifikasi Peserta apabila tidak memenuhi ketentuan dan/atau melanggar ketentuan dan/atau dicurigai melakukan kecurangan dalam mengikuti kegiatan.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Apabila salah satu pernyataan dari ketentuan ini ternyata batal, melanggar hukum, dan/atau tidak dapat dilaksanakan, maka pernyataan tersebut dianulir tanpa mempengaruhi pernyataan-pernyataan lainnya yang terdapat dalam ketentuan ini</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Program ini tidak dipungut biaya apapun. Hati-hati terhadap penipuan yang mengatasnamakan PT. Smartfren Telecom Tbk.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Keputusan Penyelenggara bersifat final, mutlak, dan tidak dapat diganggu gugat.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">Materi yang diunggah sepenuhnya akan menjadi milik&nbsp; PT. Smartfren Telecom Tbk dan dapat digunakan tanpa batasan apa pun.</p>
        </li>
        <li dir="ltr" style="list-style-type: decimal;">
        <p dir="ltr" role="presentation">PT. Smartfren Telecom Tbk berhak untuk menayangkan materi iklan tersebut pada media apapun (TV, Radio, Billboard dan Digital).</p>
        </li>
        </ol>
      </div>
    </div>
  </div>
</div>