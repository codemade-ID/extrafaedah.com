<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title><?php echo @$fb_share;?></title>
	<meta name="description" content="Smartfren WOW is Coming Now!" />
	<!-- S:OG_FB -->
	<meta property="og:url" content="<?php echo current_url();?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo @$fb_share;?>" />
	<meta name="og:image" content="<?php echo base_url();?>img/pp.png" />
	<meta property="og:site_name" content="smartfren.com" />
	<meta property="og:description" content="Smartfren WOW is Coming Now!" />
	<meta property="article:author" content="https://www.facebook.com/smartfren" />
	<meta property="article:publisher" content="https://www.facebook.com/smartfren" />
	<meta property="fb:app_id" content="525085630997607" />
	<meta property="fb:pages" content="62225654545" />
	<!-- E:OG_FB -->
	<!-- S:TC -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@smartfrenworld" />
	<meta name="twitter:creator" content="@smartfrenworld">
	<meta name="twitter:title" content="#KreatifTanpaTapi">
	<meta name="twitter:description" content="<?php echo @$tw_share;?>">
	<meta name="twitter:image" content="<?php echo base_url();?>img/pp.png" />
	<!-- E:TC -->

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugin/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Gudea:400,400italic,700'>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/fonts/stylesheet.css?v=1" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css?v=1.8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/frontend.css?v=2.13" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css?v=2.14" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/vertical-nav.css?v=2.11" />
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/bootstrap-3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/grid-2.6/modernizr.custom.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo-smartfren.png"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick-theme.css"/>
	<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/slick-1.8.1/slick/slick.min.js"></script>

	<link rel="shortcut icon" href="https://www.smartfren.com/static/assets/images/Icon.ico" type="image/x-icon" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21133138-6"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-21133138-6');
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NM4WJW6');</script>
	<!-- End Google Tag Manager -->

</head>
<body id="header">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NM4WJW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="main">

<section id="kvtop">
<video id="videobcg" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
        <source src="<?php echo base_url()?>assets/img/nonstopkreasiq/videotop.mp4" type="video/mp4">
        <source src="movie.webm" type="video/webm">Sorry, your browser does not support HTML5 video.</video>
<div class="container" style="position: absolute;top: 0;width: 100%;">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <?php if($this->ua->is_mobile()) :?>
		            <a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/logo_hashtag.png" class="logo logo-left" width="150">
					</a>
				<?php endif;?>
	        </div>
	        <?php if($this->ua->is_mobile()) :?>
		        <div id="navbar" class="navbar-collapse collapse">
		            <ul class="nav navbar-nav navbar-right">
		                <li><a href="#kvtop" class="scrollTos">HOME</a></li>
		                <li><a href="#kreasi" class="scrollTos">JOIN</a></li>
		                <li><a href="#prize" class="scrollTos">PRIZE</a></li>
		                <li><a href="#gallery" class="scrollTos">GALLERY</a></li>
		            </ul>
		        </div>
	        <?php else:?>
	        	<div class="navbar-center">
		        	<div class="row">
		        		<div class="col-md-2"><a href="#kvtop" class="scrollTos">HOME</a></div>
		        		<div class="col-md-2"><a href="#kreasi" class="scrollTos">JOIN</a></div>
		        		<div class="col-md-4"><a href="/">
		        			<a href="<?php echo base_url();?>">
								<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/logo_hashtag.png" class="logo logo-left" width="150">
							</a>
		        		</div>
		        		<div class="col-md-2"><a href="#prize" class="scrollTos">PRIZE</a></div>
		        		<div class="col-md-2"><a href="#gallery" class="scrollTos">GALLERY</a></div>
		        	</div>
		        </div>
	        <?php endif;?>
	        
	    </nav>
	</div>	
<div id="kv-top" class="cd-section">
	
	<div class="container text-center kv_text">
		<img src="/assets/img/nonstopkreasiq/kv_text.png" class="img-responsive logo-kv">
		<!-- <p>
			Nonstop Tunjukkin Kreasi Asik Kamu <br />Dan Menangkan Hadiah Total Puluhan Juta!
		</p> -->
	</div>
	<div class="cta text-center">
		<!-- <p>Ikutan Sekarang</p> -->
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-3 acts">
				<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/logo_hashtag.png" class="logo logo-left" width="150">
				<br />
				<a href="#kreasi" class="sbmt scrollTos">
					<i class="fa fa-chevron-down"></i>
				</a>
			</div>
			<div class="col-md-3 acts">
				<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/makeover.png" class="logo logo-left" width="150">
				<br />
				<a href="#makeover" class="sbmt scrollTos">
					<i class="fa fa-chevron-down"></i>
				</a>
			</div>
			<div class="col-md-3"></div>
		</div>
		
	</div>
	
</div>
<div id="slide">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/text-rotate.png" class="img-responsive" width="80%">
			</div>
			<div class="col-md-7">
				<div class="slick">
					<div class="item">
						<div class="number">1.</div>
						<p>Situasi memang lagi gak asik, tapi jangan sampai kamu jadi putus asik!</p>
					</div>
					<div class="item">
						<div class="number">2.</div>
						<p>Dengan akses nonstop, bisa terus nonstop asik berkarya & berkreasi!</p>
					</div>
					<div class="item">
						<div class="number">3.</div>
						<p>Jadi tunjukkin semangat asikmu dengan kreasi asik di #NonstopKreasiq</p>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				
					$('.slick').slick({
					  infinite: true,
					  slidesToShow: 1,
					  slidesToScroll: 1,
					  autoplay: true,
		  			  autoplaySpeed: 3000,
					});
			})
		</script>
	</div>
</div>

</section>


<section id="kreasi" class="cd-section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 text-center">
				<!-- <h3 class="stroke">KREASI LAGU ASIK</h3> -->
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/kreasi_hl_1.png" class="hl-img">
				<p>Bareng Indomusikgram</p>
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/cewek.png" class="img-responsive ch">
				<div class="box box-outside box-left">
					<div class="icon-kreasi lagu"></div>
					<p><span class="bg-w">KATEGORI</span><span class="bg-b">LAGU</span></p>
					<div id="slider1" class="carousel slide" data-ride="carousel" data-interval="false">
						<div class="carousel-inner">
							<div class="item active">
								<ul>
									<li><div class="vidio"></div>Dengerin lagu Nonstop Kreasiq <a href="https://drive.google.com/drive/folders/1Fs4ACNyHcd2ZbDAkZmNBiAJ4B939bdUZ?usp=sharing" target="_blank" class="btn btn-white btn-text"><span>DI SINI</span></a> dan bikin Kreasi Lagu video cover versi kamu</li>
									<li><div class="upload"></div>Upload di instagram & youtube dengan hashtag <a href="https://www.instagram.com/explore/tags/NonstopKreasiq" class="color-putih">#NonstopKreasiq</a> <a href="https://www.instagram.com/explore/tags/AntiPutusNyambungTerus" class="color-putih">#AntiPutusNyambungTerus</a></li>
									<li><div class="mention"></div>Mention <a href="https://www.instagram.com/smartfrenworld" class="color-putih">@smartfrenworld</a>  dan 3 orang teman <br /> di caption </li>
								</ul>
							</div>
							<div class="item prizes">
								<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/menangkan-hadiah.png" class="img-responsive" style="width: 50%;margin: 10px auto 0px;display: block!important;">
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-6">
										<div class="box talent">
											<div class="prize-img talent">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_talent.png">
											</div>
											<div class="partnership">
											<p class="TruenoExBd">Talent Partnership</p>
											<p><img src="<?php echo base_url()?>assets/img/nonstopkreasiq/icon_img_sf.png" width="32"> smartfren</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="box">
											<p class="TruenoExBd">Juara 1</p>
											<p>Rp 7.500.000</p>
										</div>
									</div>
									<div class="col-md-1"></div>
									<div class="clear"></div>
									<div class="col-md-4">
										<div class="box">
											<p class="TruenoExBd">Juara 2</p>
											<p>Rp 5.000.000</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="box">
											<p class="TruenoExBd">Juara 3</p>
											<p>Rp 2.500.000</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="box">
											<p class="TruenoExBd">15 Juara Harapan</p>
											<p>Rp 1.000.000</p>
										</div>
									</div>
									
								</div>

							</div>
						</div>
						<a class="carousel-control-prev carousel-control" href="#slider1" role="button" data-slide="prev">
					    <i class="fa fa-caret-left" aria-hidden="true"></i>
					  </a>
					  <a class="carousel-control-next carousel-control" href="#slider1" role="button" data-slide="next">
					    <i class="fa fa-caret-right" aria-hidden="true"></i>
					  </a>
					</div>
					<a href="https://drive.google.com/drive/folders/1Fs4ACNyHcd2ZbDAkZmNBiAJ4B939bdUZ?usp=sharing" target="_blank" class="btn btn-white">LAGU & LIRIK + CHORD</a>
				</div>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#slider131 .carousel-inner').slick({
						  infinite: true,
						  slidesToShow: 1,
						  slidesToScroll: 1,
						  autoplay: false,
			  			  autoplaySpeed: 3000,
			  			  centerMode: true,
						});
					})
				</script>
			</div>
			<div class="col-md-6 text-center">
				<!-- <h3 class="stroke">KREASI QUOTES ASIK</h3> -->
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/kreasi_hl_2.png" class="hl-img">
				<p>Untuk Terus Semangat Nonstop</p>
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/cowok.png" class="img-responsive ch">
				<div class="box box-outside">
					<div class="icon-kreasi quotes"></div>
					<p><span class="bg-w">KATEGORI</span><span class="bg-b">QUOTES</span></p>
					<div id="slider2" class="carousel sslide" data-ride="casrousel" data-interval="false">
						<div class="carousel-inner">
							<div class="item active">
								<ul>
									<li><div class="quote"></div>Bikin Kreasi Quotes asik versi kamu melalui foto atau video cerita monolog</li>
									<li><div class="upload"></div>Upload di instagram & youtube dengan hashtag <a href="https://www.instagram.com/explore/tags/NonstopKreasiq" class="color-putih">#NonstopKreasiq</a> <a href="https://www.instagram.com/explore/tags/AntiPutusNyambungTerus" class="color-putih">#AntiPutusNyambungTerus</a></li>
									<li><div class="mention"></div>Mention <a href="https://www.instagram.com/smartfrenworld" class="color-putih">@smartfrenworld</a>  dan 3 orang teman <br /> di caption </li>
								</ul>
							</div>
							<div class="item prizes">
								<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/menangkan-hadiah.png" class="img-responsive" style="width: 50%;margin: 10px auto 0px;display: block!important;">
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-5">
										<div class="box">
											<div class="prize-img">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_iphonexs_1.png">
											</div>
											<p class="TruenoExBd">Juara 1</p>
											<p>iPhone XS</p>
										</div>
									</div>
									<div class="col-md-5">
										<div class="box">
											<div class="prize-img right">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_samsunga50_1.png">
											</div>
											<p class="TruenoExBd">2 Juara 2</p>
											<p>Samsung A50</p>
										</div>
									</div>
									<div class="col-md-1"></div>
									<div class="clear"></div>
									<div class="col-md-4">
										<div class="box">
											<div class="prize-img">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_redmi9_1.png">
											</div>
											<p class="TruenoExBd">Juara 3</p>
											<p>Redmi Note 9 Pro</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="box">
											<div class="prize-img">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_redmi9c_1.png">
											</div>
											<p class="TruenoExBd">10 Juara Favorit</p>
											<p>Redmi  9C</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="box">
											<div class="prize-img right">
												<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_speaker_1.png?v=1">
											</div>
											<p class="TruenoExBd">10 Juara Harapan</p>
											<p>Swarovski Speaker</p>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<a class="carousel-control-prev carousel-control" href="#slider2" role="button" data-slide="prev">
						    <i class="fa fa-caret-left" aria-hidden="true"></i>
						  </a>
						  <a class="carousel-control-next carousel-control" href="#slider2" role="button" data-slide="next">
						    <i class="fa fa-caret-right" aria-hidden="true"></i>
						  </a>
						<a href="https://www.instagram.com/explore/tags/nonstopkreasiq/" target="_blank" class="btn btn-white">JOIN SEKARANG</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</section>
<?php /*
<section id="prize" class="cd-section">
	<div class="container">
		<!-- <h2 class="stroke TruenoExBd">Menangkan Total Hadiah Puluhan Juta</h2> -->
		<?php if($this->ua->is_mobile) :?>
			<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_hl_m.png" class="hl-img">
		<?php else:?>
			<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_hl.png" class="hl-img">
		<?php endif;?>
		<p class="subtitle text-center">Dan Exclusive Talent Partnership</p>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="box talent">
					<div class="prize-img talent">
						<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_talent.png">
					</div>
					<p class="TruenoExBd">Talent Partnership</p>
					<p><img src="<?php echo base_url()?>assets/img/nonstopkreasiq/icon_img_sf.png" width="32"> smartfren</p>
				</div>
			</div>
			<div class="col-md-3"></div>
			<div class="clear"></div>
			<p class="text-center"><span class="bg-p">KATEGORI</span><span class="bg-b">LAGU</span></p>
			<div class="clear"></div>
			<div class="col-md-3">
				<div class="box">
					<p class="TruenoExBd">Juara 1</p>
					<p>Rp 7.500.000</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box">
					<p class="TruenoExBd">Juara 2</p>
					<p>Rp 5.000.000</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box">
					<p class="TruenoExBd">Juara 3</p>
					<p>Rp 2.500.000</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box">
					<p class="TruenoExBd">15 Juara Harapan</p>
					<p>Rp 1.000.000</p>
				</div>
			</div>
			<div class="clear"></div>
			<p class="text-center"><span class="bg-p">KATEGORI</span><span class="bg-b">QUOTES</span></p>
			<div class="clear"></div>
			<div class="three-row">
				<div class="row">
					<div class="col-md-4">
						<div class="box">
							<div class="prize-img">
								<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_iphonexs.png">
							</div>
							<p class="TruenoExBd">Juara 1</p>
							<p>iPhone XS</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="box">
							<div class="prize-img">
								<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_samsunga50.png">
							</div>
							<p class="TruenoExBd">2 Juara 2</p>
							<p>Samsung A50</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="box">
							<div class="prize-img right">
								<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_redmi9.png">
							</div>
							<p class="TruenoExBd">Juara 3</p>
							<p>Redmi Note 9 Pro</p>
						</div>
					</div>
					<?php if($this->ua->is_mobile()):?>
						<div class="col-md-4">
							<div class="box">
								<div class="prize-img">
									<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_redmi9c.png">
								</div>
								<p class="TruenoExBd">10 Juara Favorit</p>
								<p>Redmi  9C</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box">
								<div class="prize-img right">
									<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_speaker.png?v=1">
								</div>
								<p class="TruenoExBd">10 Juara Harapan</p>
								<p>Swarovski Speaker</p>
							</div>
						</div>
					<?php endif;?>
				</div>
			</div>
			<div class="clear"></div>
			<?php if(!$this->ua->is_mobile()):?>
			<div class="two-row">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3 f">
						<div class="box">
							<div class="prize-img">
								<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_redmi9c.png">
							</div>
							<p class="TruenoExBd">10 Juara Favorit</p>
							<p>Redmi  9C</p>
						</div>
					</div>
					<div class="col-md-3 f">
						<div class="box">
							<div class="prize-img right">
								<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_speaker.png?v=1">
							</div>
							<p class="TruenoExBd">10 Juara Harapan</p>
							<p>Swarovski Speaker</p>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
			</div>
			<?php endif;?>
			
		</div>
	</div>
</section>
*/ ?>
<section id="gallery" class="cd-section">
		<!-- <h2 class="stroke TruenoExBd text-center">Inilah <span>1000</span> Orang</h2> -->
		<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/gallery_hl.png" class="hl-img">
		<p class="subtitle text-center">Yang Sudah Berkreasi Asik</p>
		<?php /* 
		<div class="slick-gallery" id="gallery-post">
			<?php if($this->ua->is_mobile()):?>
				<?php if(!empty($submission)) :?>
						<?php $i=0; foreach($submission as $s):?>
							<?php if($i%2==0):?>
					<div class="item">
						<div class="row">
							<?php endif;?>
								<div class="col-md-2 post <?php echo $i;?>">
									<a href="<?php echo $s->instagramlink;?>" target="_blank">
										<img src="<?php echo $s->thumbnail;?>" class="img-responsive">
									</a>
								</div>
							<?php if($i%2==1|| $i == (count($submission)-1)):?>
									</div>
								</div>
							<?php endif;?>
						<?php $i++;endforeach;?>
				<?php endif;?>
			<?php else:?>
				<?php if(!empty($submission)) :?>
					<?php $i=0; foreach($submission as $s):?>
						<?php if($i%12==0):?>
						<div class="item">
							<div class="row">
								<?php endif;?>
									<div class="col-md-2 post <?php echo $i;?>">
										<a href="<?php echo $s->instagramlink;?>" target="_blank">
											<img src="<?php echo $s->thumbnail;?>" class="img-responsive">
										</a>
									</div>
								<?php if($i%12==11|| $i == (count($submission)-1)):?>
										</div>
									</div>
								<?php endif;?>
							<?php $i++;endforeach;?>
					<?php endif;?>
			<?php endif;?>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.slick-gallery').slick({
				  dots: true,
				  infinite: true,
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  autoplay: false,
	  			  autoplaySpeed: 3000,
				});
			})
		</script>
		*/?>
		<div class="multiple-items">
			<?php if(!empty($submission)) :?>
				<?php foreach($submission as $s):?>
					<div class="item">
						<a href="<?php echo $s->instagramlink;?>" target="_blank">
				    	<img src="<?php echo $s->thumbnail;?>" class="img-responsive"></a>
				    	<!-- <div class="name  font-16">
							<?php echo $s->namalengkap;?>
						</div> -->
						<div class="account">
							<a href="https://www.instagram.com/<?php echo $s->email;?>" target="_blank">@<?php echo $s->email;?></a>
						</div>
				    </div>
				<?php endforeach;?>
			<?php endif;?>
		</div>
		<script type="text/javascript">
			$('.multiple-items').slick({
			  infinite: true,
			  slidesToShow: 5,
			  slidesToScroll: 1,
			  dots: false,
			  // centerMode: true,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3,
			        infinite: true,
			        dots: true
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			    // You can unslick at a given breakpoint now by adding:
			    // settings: "unslick"
			    // instead of a settings object
			  ]
			});
		</script>
		
		<?php /*
		<div class="slick-gallery">
			<script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
			<link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
			<ul class="juicer-feed" data-feed-id="barutahukan"></ul>
		</div>
		<style type="text/css">
			.juicer-feed h1.referral,li.juicer-ad{display: none!important;}
		</style>
		*/?>
		<p class="text-center"><a href="javascript:;" class="font-14 color-putih underline"  data-toggle="modal" data-target="#tnc">Syarat & Ketentuan berlaku</a></p>
</section>
<section id="makeover">
	<div class="container">
		<div class="row">
			<div class="col-md-6 side-left">
				<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/makeover.png" class="img-responsive logo-hashtag">
				<div id="kreasi">
					<div class="box box-outside box-left">
						<div id="slider3" class="carousel slide" data-ride="carousel" data-interval="false">
							<div class="carousel-inner">
								<div class="item active">
									<ul>
										<li><div class="follow"></div>Follow TikTok @smartfrenwolrd</li>
										<li><div class="upload makeovers"></div>
											Upload kreasi makeover kamu yang paling asik dengan filter dan lagu challenge sebanyak-banyaknya</li>
										<li><div class="mention"></div>Sertakan hashtag <a href="https://www.instagram.com/explore/tags/NonstopKreasiq" class="color-putih">#NonstopKreasiq</a> dan mention ke @smartfrenworld</li>
										<li><div class="shareig"></div>Sebarkan juga di instagram kamu dengan hashtag <a href="https://www.instagram.com/explore/tags/NonstopKreasiq" class="color-putih">#NonstopKreasiq</a></li>
									</ul>
								</div>
								<div class="item prizes">
									<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/menangkan-hadiah.png" class="img-responsive menangkan-hadiah">
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-6">
											<div class="box talent">
												<div class="prize-img talent">
													<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/prize_talent.png">
												</div>
												<div class="partnership">
												<p class="TruenoExBd">Talent Partnership</p>
												<p><img src="<?php echo base_url()?>assets/img/nonstopkreasiq/icon_img_sf.png" width="32"> smartfren</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="box">
												<p class="TruenoExBd">Juara 1</p>
												<p>Rp 7.500.000</p>
											</div>
										</div>
										<div class="col-md-1"></div>
										<div class="clear"></div>
										<div class="col-md-4">
											<div class="box">
												<p class="TruenoExBd">Juara 2</p>
												<p>Rp 5.000.000</p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="box">
												<p class="TruenoExBd">Juara 3</p>
												<p>Rp 2.500.000</p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="box">
												<p class="TruenoExBd">15 Juara Harapan</p>
												<p>Rp 1.000.000</p>
											</div>
										</div>
										
									</div>

								</div>
							</div>
							<a class="carousel-control-prev carousel-control" href="#slider3" role="button" data-slide="prev">
						    <i class="fa fa-caret-left" aria-hidden="true"></i>
						  </a>
						  <a class="carousel-control-next carousel-control" href="#slider3" role="button" data-slide="next">
						    <i class="fa fa-caret-right" aria-hidden="true"></i>
						  </a>
						</div>
						<div class="text-center">
							<a href="javascript:;" target="_blank" class="btn btn-white">JOIN SEKARANG</a>
						</div>
						
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$('#slider12 .carousel-inner').slick({
							  infinite: true,
							  slidesToShow: 1,
							  slidesToScroll: 1,
							  autoplay: false,
				  			  autoplaySpeed: 3000,
				  			  centerMode: true,
							});
						})
					</script>
				</div>
			</div>
			<div class="col-md-6 side-right">
				<img src="<?php echo base_url();?>assets/img/nonstopkreasiq/kreasi-makeover.png" class="img-responsive text-title">
				<p class="subtitle text-center">Di TikTok Official Challenge</p>
				<div class="box-makeover">
					<video id="makeovervid" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
			        <source src="<?php echo base_url()?>assets/img/nonstopkreasiq/videomakeover.mp4" type="video/mp4">
			        <source src="movie.webm" type="video/webm">Sorry, your browser does not support HTML5 video.</video>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contactus">
	<div class="container products">
		<div class="row">
			<div class="col-md-6">
				<br />
				<br />
				<img src="<?php echo base_url()?>assets/img/kuota_nonstop.png?v=1" class="img-responsive">
			</div>
			<div class="col-md-6">
				<!-- <h3 class="stroke TruenoExBd">YUK NONSTOP  KREASIQ BARENG KUOTA NONSTOP</h3> -->
				<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/product_hl.png" class="img-responsive">
				<p>Dengan Kuota Besar 24 Jam <br /> Di Semua Aplikasi & Anti Sedot Pulsa</p>
				<div class="row text-center commerce">
					<div class="col-md-3">
						<img src="/assets/img/icon_shopee.png" width="80%">
						<br /><br />
						<a target="_blank" href="https://shopee.co.id/smartfrenstore?v=5e1&smtt=0.0.3" class="btn btn-red color-putih">
							BELI DI SINI
						</a>
					</div>
					<div class="col-md-3">
						<img src="/assets/img/icon_tokopedia.png" width="80%">
						<br /><br />
						<a target="_blank" href="https://tokopedia.link/fI5BkQBUx1" class="btn btn-red color-putih">
							BELI DI SINI
						</a>
					</div>
					<div class="col-md-3">
						<img src="/assets/img/icon_blibli.png" width="80%">
						<br /><br />
						<a target="_blank" href="https://www.blibli.com/merchant/smartfren-official/SMO-56333#commerce" class="btn btn-red color-putih">
							BELI DI SINI
						</a>
					</div>
					<div class="col-md-3">
						<img src="/assets/img/icon_bukalapak.png" width="80%">
						<br /><br />
						<a target="_blank" href="https://www.bukalapak.com/u/smartfrenofficial" class="btn btn-red color-putih">
							BELI DI SINI
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
	  <div class="footer-top">
	    <!-- <div class="container"> -->
	      <div class="row">
	        <div class="col-md-3 segment-one text-center">
	          	<p>Powered by</p>
	          	<img src="<?php echo base_url()?>assets/img/LogoNew_Smartfren-white.png" class="img-responsive center">
	        </div>
	        <div class="col-md-5 segment-two">
	          	<p>
	          		Nonstop Kreasiq adalah wadah di mana
					kita semua bisa asik berkreasi dan berkarya
					untuk menyebarkan keasikan dan semangat
					terus membuka peluang dan membuat
					karya positif di sekitar.
	          	</p>
	        </div>
	        <div class="col-md-4 segment-three text-left">
	        	<h2 class="text-left">Download Aplikasi MySmartfren</h2>
	          	<a href="https://play.google.com/store/apps/details?id=com.smartfren" target="_blank"><img src="<?php echo base_url();?>assets/img/googleplay.png" width="130" height="40"></a>
	          	<a href="https://apps.apple.com/id/app/mysmartfren/id1209898190" target="_blank"><img src="<?php echo base_url();?>assets/img/applestore.png" width="130" height="40"></a>
		        <div class="clear"></div>
		        <div class="row">
		        	<div class="col-md-5"><h2>Follow Us</h2></div>
		        	<div class="col-md-7">
		        		<a href="https://www.facebook.com/smartfren/" target="_blank"><i class="facebook"><img src="<?php echo base_url();?>assets/img/fb-red.png" width="40"
	                		height="40"></i></a>
			          	<a href="https://twitter.com/smartfrenworld?s=09" target="_blank"><i class="twitter"><img src="<?php echo base_url();?>assets/img/tw-red.png" width="40"
			                height="40"></i></a>
			          	<a href="https://instagram.com/smartfrenworld?igshid=ndapwdekp6li" target="_blank"><i class="instagram"><img src="<?php echo base_url();?>assets/img/ig-red.png" width="40"
			                height="40"></i></a>
		        	</div>
		        	<div class="clear"></div>
		        	<div class="col-md-5">
		        		<h2>Contact Us</h2>
		        	</div>
		        	<div class="col-md-7">
		        		<b>Smartfren User:</b> 888 <br>
	            		<b>Other:</b> 08888 1 88888</br>
		        	</div>
		        </div>
	        </div>
	      </div>
	    <!-- </div> -->
	  </div>
	</footer>
</section>
<?php //echo @$content;?>
</div>
<!-- Footer -->
<!-- 
<nav id="cd-vertical-nav">
    <ul>
        <li>
            <a href="#kvtop" data-number="1">
            <span class="cd-dot"></span>
            <span class="cd-label">Home</span>
            </a>
        </li>
        <li>
            <a href="#kreasi" data-number="2">
            <span class="cd-dot"></span>
            <span class="cd-label">Join</span>
            </a>
        </li>
        <li>
            <a href="#prize" data-number="3">
            <span class="cd-dot"></span>
            <span class="cd-label red">Prize</span>
            </a>
        </li>
        <li>
            <a href="#gallery" data-number="4">
            <span class="cd-dot"></span>
            <span class="cd-label">Gallery</span>
            </a>
        </li>
    </ul>
</nav> -->
<a href="javascript:;" class="totop" style="font-size: 40px;display: block;position: fixed;bottom: 20%; right: 20px;color: #000; display: none;z-index: 9999">
	<i class="fa fa-arrow-circle-up"></i>
</a>
<a href="https://www.smartfren.com/id/shop/starter-pack/prepaid/Starter-Pack-NonSTOP" target="_blank" class="sticky-prod" style="font-size: 40px;display: block;position: fixed;bottom: 20%; left: -30px;z-index: 9999;width: 150px;">
	<img src="<?php echo base_url()?>assets/img/nonstopkreasiq/sticky_button.png" class="img-responsive">
</a>
<?php $this->load->view('nonstopkreasiq/tnc');?>
<script type="text/javascript">
	$('.totop').click(function(){
		$('html,body').animate({scrollTop:0},500);
	});
	$(window).scroll(function() {
	    if ($(this).scrollTop()) {
	        $('.totop:hidden').stop(true, true).fadeIn();
	    } else {
	        $('.totop').stop(true, true).fadeOut();
	    }
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('a.scrollTos').click(function(){
			// var href = $(this).attr('href');
			// $('html, body').animate({
		 //        scrollTop: $(href).offset().top
		 //    }, 2000);

		    var target = $(this.hash);
		    // var target = $(this).data("hashed");
		    // console.log(target);
		    $('body,html').animate({
	            'scrollTop': $(target).offset().top
	        }, 600);


	    });

	});
	jQuery(document).ready(function($) {
	    var contentSections = $('.cd-section'),
	        navigationItems = $('#cd-vertical-nav a'),
	        navigationItems2 = $('.navbar-center a');
	    updateNavigation();
	    $(window).on('scroll', function() {
	        updateNavigation();
	    });
	    navigationItems.on('click', function(event) {
	        data_number = $(this).data('number');
	        if(Number.isInteger(data_number)){
	            event.preventDefault();
	            smoothScroll($(this.hash));
	        }
	        console.log($(this.hash));
	        
	    });

	    navigationItems2.on('click', function(event) {
	        data_number = $(this).data('number');
	        if(Number.isInteger(data_number)){
	            event.preventDefault();
	            smoothScroll($(this.hash));
	        }
	        // console.log(data_number);
	        
	    });

	    $('.cd-scroll-down').on('click', function(event) {
	        event.preventDefault();
	        smoothScroll($(this.hash));
	    });
	    $('.touch .cd-nav-trigger').on('click', function() {
	        $('.touch #cd-vertical-nav').toggleClass('open');
	    });
	    $('.touch #cd-vertical-nav a').on('click', function() {
	        $('.touch #cd-vertical-nav').removeClass('open');
	    });

	    function updateNavigation() {
	    	var contentSections = $('.cd-section'),
	        navigationItems = $('#cd-vertical-nav a');
	        contentSections.each(function() {
	            $this = $(this);
	            var activeSection = $('#cd-vertical-nav a[href="#' + $this.attr('id') + '"]').data('number') - 1;
	            if (($this.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ($this.offset().top + $this.height() - $(window).height() / 2 > $(window).scrollTop())) {
	                navigationItems.eq(activeSection).addClass('is-selected');
	            } else {
	                navigationItems.eq(activeSection).removeClass('is-selected');
	            }
	        });
	    }

	    function smoothScroll(target) {
	        $('body,html').animate({
	            'scrollTop': target.offset().top
	        }, 600);
	    }

	    $('a.smoothscroll').click(function(){
	        var target = $(this.hash);
	        smoothScroll(target);
	    });
	});
</script>

<?php if($this->ua->is_mobile()):?>
	<?php /*
<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/onepagescroll/jquery.onepage-scroll.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/onepagescroll/onepage-scroll.css"/>	
<script type="text/javascript">
	$(document).ready(function(){
		$(".main").onepage_scroll({
		   sectionContainer: "section",     // sectionContainer accepts any kind of selector in case you don't want to use section
		   easing: "ease",                  // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in",
		                                    // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
		   animationTime: 1000,             // AnimationTime let you define how long each section takes to animate
		   pagination: false,                // You can either show or hide the pagination. Toggle true for show, false for hide.
		   updateURL: false,                // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
		   beforeMove: function(index) {},  // This option accepts a callback function. The function will be called before the page moves.
		   afterMove: function(index) {
		   		var contentSections = $('.cd-section'),
		        navigationItems = $('#cd-vertical-nav a');
		        contentSections.each(function() {
		            $this = $(this);
		            var activeSection = $('#cd-vertical-nav a[href="#' + $this.attr('id') + '"]').data('number') - 1;
		            if (($this.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ($this.offset().top + $this.height() - $(window).height() / 2 > $(window).scrollTop())) {
		                navigationItems.eq(activeSection).addClass('is-selected');
		            } else {
		                navigationItems.eq(activeSection).removeClass('is-selected');
		            }
		        });
		   },   // This option accepts a callback function. The function will be called after the page moves.
		   loop: false,                     // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
		   keyboard: true,                  // You can activate the keyboard controls
		   responsiveFallback: false,        // You can fallback to normal page scroll by defining the width of the browser in which
		                                    // you want the responsive fallback to be triggered. For example, set this to 600 and whenever
		                                    // the browser's width is less than 600, the fallback will kick in.
		   direction: "vertical"            // You can now define the direction of the One Page Scroll animation. Options available are "vertical" and "horizontal". The default value is "vertical".  
		});
	});
	
</script>
*/?>
<?php else:?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/plugin/fullpage/jquery.fullPage.css" />
<script src="<?php echo base_url()?>assets/plugin/fullpage/jquery.easings.min.js"></script>
<script src="<?php echo base_url()?>assets/plugin/fullpage/scrolloverflow.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/plugin/fullpage/jquery.fullPage.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="https://rawgit.com/alvarotrigo/fullPage.js/dev/src/fullpage.js"></script> -->
<script type="text/javascript">
	$(document).ready(function() {
		$('.main').fullpage({
			sectionSelector: 'section',
			//options here
			// autoScrolling:true,
			navigation: true,
	        navigationPosition: 'right',
	        navigationTooltips: ['HOME', 'JOIN', 'PRIZE', 'GALLERY', ''],
	        // anchors: ['kvtop', 'kreasi', 'prize', 'gallery',''],
	        onLeave: function(index, nextIndex, direction){
		        var leavingSection = $(this);
		        console.log(leavingSection);
		    }
		});
		fullpage_api.setAllowScrolling(false);
	});
	
</script>
<?php endif;?>
</body>
</html>
