<section id="video">
	<!-- <div class="resp-container">
		<iframe class="resp-iframe" width="100%" height="642" src="https://www.youtube.com/embed/jaSlynS_fHA?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div> -->
	<a href="https://www.vidio.com">
			<img src="<?php echo base_url()?>assets/img/Carousel-WOWSquad2.png" width="100%">
		</a>
</section>
<section id="time" style="display: none;">
	<div class="container">
		<div>
			<img src="assets/img/get-ready-for-concert.png" class="img-responsive center">
			<div id="demo" class="board">
				<div class="row">
					<div class="col-md-4 text-center" id="days"></div>
					<div class="col-md-4 text-center" id="hours"></div>
					<div class="col-md-4 text-center" id="minutes"></div>
				</div>
			</div>
			<script>
		        // Set the date we're counting down to
		        let countDownDate = new Date("Sep 20, 2019 20:00:00").getTime();

		        // Update the count down every 1 second
		        let x = setInterval(function () {

		          // Get today's date and time
		          let now = new Date().getTime();

		          // Find the distance between now and the count down date
		          let distance = countDownDate - now;

		          // Time calculations for days, hours, minutes and seconds
		          let days = Math.floor(distance / (1000 * 60 * 60 * 24));
		          days > 9 ? days : days = '0' + days;
		          let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		          hours > 9 ? hours : hours = '0' + hours;
		          let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		          minutes > 9 ? minutes : minutes = '0' + minutes;
		          let seconds = Math.floor((distance % (1000 * 60)) / 1000);

		          // Display the result in the element with id="demo"
		          // document.getElementById("demo").innerHTML = days + " " + hours + " " + minutes;
		          document.getElementById("days").innerHTML = days;
		          document.getElementById("hours").innerHTML = hours;
		          document.getElementById("minutes").innerHTML = minutes;


		          // If the count down is finished, write some text

		          if (distance < 0) {
		            clearInterval(x);
		            // document.getElementById("demo").innerHTML = "WOW";
		          }
		        }, 1000);
		      </script>

		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.multiple-items').slick({
					  dots: false,
					  infinite: false,
					  speed: 300,
					  slidesToShow: 4,
					  slidesToScroll: 1,
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 1,
					        infinite: true,
					        dots: true
					      }
					    },
					    {
					      breakpoint: 600,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    },
					    {
					      breakpoint: 480,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1
					      }
					    }
					    // You can unslick at a given breakpoint now by adding:
					    // settings: "unslick"
					    // instead of a settings object
					  ]
				});
			})
			
		</script>
		<div class="row">
			<div class="col-sm-6 text-left"><span class="lu">Line Up</span></div>
			<div class="col-sm-6 text-right"><a href="javascript:;" class="lu"  data-toggle="modal" data-target="#myLU">View All</a></div>
			<div class="row multiple-items h text-center" id="lineup">
				<?php foreach($lineup as $lu) :?>
					<div class="item">
						<div class="lu-img">
							<img src="<?php echo $lu['image']?>" class="img-responsive">
						</div>
						<p class="lu-name"><?php echo $lu['name']?></p>
					</div>
				<?php endforeach;?>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myLU" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <?php $this->load->view('lineup');?>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</section>
<!-- <a href="<?php echo base_url()?>how-to-get">
<img src="<?php echo base_url()?>assets/img/Inside-Banner.jpg" class="img-responsive"  style="margin: 0 auto; width: 100%;">
</a> -->
<section id="home"><?php /*
	<div class="activity">
		
		<div class="container">
			<h3 class="TruenoBlk">Activity</h3>
			<div class="row">
				<div class="col-md-4">
					<div class="act text-center">
						<a href="<?php echo base_url();?>index.php/ticket">
							<img src="assets/img/act-liveperfomance.png">
							<!-- <p>LIVE PERFORMANCES</p> -->
							<p>GET YOUR SPECIAL INVITATION</p>
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="act text-center">
						<a href="javascript:;" class="lu"  data-toggle="modal" data-target="#FT">
						<img src="assets/img/act-foodtruck.png">
						<p>FOOD TRUCKS <br /> &nbsp;</p>
					</a>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="FT" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      	<h1 class="text-center">FOOD TRUCKS</h1>
					        <div class="row foodtruck">
								<?php foreach($foodtruck as $ft) :?>
									<div class="col-md-4 text-center">
										<div class="lu-img">
											<img src="<?php echo $ft['image']?>" class="img-responsive">
										</div>
										<p class="lu-name"><?php echo $ft['name']?></p>
									</div>
								<?php endforeach;?>
							</div>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="act text-center">
						<a href="javascript:;" class="lu"  data-toggle="modal" data-target="#VM">
							<img src="assets/img/act-wowexperience.png">
							<p>VENUE MAP <br /> &nbsp;</p>
						</a>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="VM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      		<?php $this->load->view('map');?>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<h1 class="text-center color-hitam">4 cara mendapatkan special invitation
			</h1>
			<div class="row pad-30 pad-t pad-b mar-50 mar-l mar-r cir">
				<div class="col-sm-3">
					<div class="circle">
						<a href="#tickets" class="scrollTos">
							<img src="<?php echo base_url()?>assets/img/ico-diva-gallery.png" class="img-responsive">
						</a>
					</div>
					<p class="text-center color-hitam">Kunjungi Gallery Smartfren, Booth Smartfren di <!-- FX, --> Senayan City</p>
				</div>
				<div class="col-sm-3">
					<div class="circle">
						<a href="#layanantar" class="scrollTos">
							<img src="<?php echo base_url()?>assets/img/ico-diva-deliver.png" class="img-responsive">
						</a>
					</div>
					<p class="text-center color-hitam">Buat yang #Mager bisa cobain Layan Antar dari Smartfren</p>
				</div>
				<div class="col-sm-3">
					<div class="circle">
						<a href="#poin" class="scrollTos">
							<img src="<?php echo base_url()?>assets/img/ico-diva-spoin.png" class="img-responsive">
						</a>
					</div>
					<p class="text-center color-hitam">Tukarkan SmartPoin kamu di aplikasi MySmartfren</p>
				</div>
				<div class="col-sm-3">
					<div class="circle">
						<a href="#digq" class="scrollTos">
							<img src="<?php echo base_url()?>assets/img/ico-diva-radio.png" class="img-responsive">
						</a>
					</div>
					<p class="text-center color-hitam">Ikutin quiz #WOWconcert di Media Sosial & Radio</p>
				</div>
				
			</div>
		</div>
		
	</div>*/ ?>
	<?php /*
	<div class="box-activity" id="tickets">
		<div class="container">
			<h1>Get Your Special Invitation at</h1>
            <h3>Special invitation yang resmi hanya berlaku di galeri Smartfren<!-- , FX Sudirman dan --> Senayan City (Khusus Senayan City mulai dari tanggal 9 September)</h3>
			<div class="row mar-20 mar-l mar-r">
				<div class="col-sm-12">
					<div class="list"><p><a href="<?php echo base_url()?>galeri" class="color-white" style="color: #fff">All Gallery Smartfren &nbsp;<i class="fa fa-caret-right"></i></a> </p></div>
				</div>
			</div>
			
			<div class="row mar-20 mar-l mar-r">
				<!-- <div class="col-sm-6">
					<div class="list"><p>FX Senayan</p></div>
					<div class="row list-gallery">
						<div class="col-sm-12">
							<p>Lantai F1, Jl. Jend. Sudirman Jl. Pintu Satu Senayan, Gelora, Tanahabang, Jakarta, Daerah Khusus Ibukota Jakarta 10270</p>
							<p><a href="http://maps.google.com/maps?q=-6.2248477,106.8017093" target="_blank" class="location"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
						</div>
					</div>
				</div> -->
				<div class="col-sm-6">
					<div class="list"><p>Senayan City (mulai 9 september)</p></div>
					<div class="row list-gallery">
						<div class="col-sm-12">
							<!-- <p>Galeri Smartfren Sabang</p> -->
							<p>Lower Ground,  Jl. Asia Afrika No.3, RT.1/RW.3, Gelora, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10270 </p>
							<!-- <p>Buka : Tutup pukul 18.30</p> -->
							<p><a href="http://maps.google.com/maps?q=-6.2273633,106.7949765" target="_blank" class="location"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-activity" id="layanantar">
		<div class="container">
			<h3 class="color-hitam  font-26">Layanan Antar</h3>
	       	<div class="row mar-20 mar-l mar-r">
				<div class="col-sm-12">
					<ul class="commerce">
						<!-- <li class="text-center" style="list-style: none;">
							Produk dikirim langsung dari Smartfren <br /> <br />
							<a href="https://www.smartfren.com/id/delivery-order/"  target="_blank" class="btn btn-warning font-22 color-white">Klik Di Sini</a> <br /> <br />
						</li> -->
						<li class="text-center"style="list-style: none;">
							Produk dikirim oleh E-commerce Partner: <br /> <br />
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-2">
											<a href="https://www.tokopedia.com/smartfren?nref=shphead">
												<img src="<?php echo base_url()?>assets/img/ico_tokped.png">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://www.bukalapak.com/u/smartfrenofficial">
												<img src="<?php echo base_url()?>assets/img/ico_BL.png">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://www.blibli.com/merchant/smartfren-official/SMO-56333">
												<img src="<?php echo base_url()?>assets/img/ico_bli.png">
											</a>
										</div>
										<div class="col-md-2">
											<a href="http://m.elevenia.co.id/store/smartfren-official-store">
												<img src="<?php echo base_url()?>assets/img/ico_elvenia.png">
											</a>
										</div>
										<div class="col-md-2">
											<a href="https://shopee.co.id/smartfrenstore?v=2f5&smtt=0.0.3">
												<img src="<?php echo base_url()?>assets/img/ico_shopee.png">
											</a>
										</div>
										<div class="col-md-1"></div>
									</div>
								</div>
							</div>
							
						</li>
					</ul>
					<div class="list">
					</div>
					<div class="accordion myaccordion" id="accordionExample">
					  <div class="card">
					    <div class="card-header" id="headingOne">
					      <h2 class="mb-0">
					        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          Syarat & Ketentuan
					        </button>
					      </h2>
					    </div>
					    <style type="text/css">
					    	.card-body p, .card-body ol li{color: #595959}
					    	.card-body ol{margin-left: 30px;}
					    </style>
					    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
					      <div class="card-body">
					        	<p>Deskripsi</p>
								<p>[Limited] Special Invitation!</p>
								<p>Smartfren WOW Concert merupakan persembahan Smartfren kepada pelanggan setia. Smartfren WOW Concert akan dimeriahkan oleh artis international ATEEZ (K-POP), CHUNG HA (K-POP), NIKI, DEWA 19, ROSSA, ONCE, DUL ZAELANI, AALIYAH MASSAID, DIPHA BARUS, KALLULA dan masih banyak lagi. Acara akan diselenggarakan pada 20 September 2019 di Istora Senayan mulai pukul 15:00 WIB.</p>
								<p>Syarat &amp; Ketentuan</p>
								<ol>
								<li>Pelanggan akan mendapatkan Special Invitation dengan melakukan pembelian produk Smartfren sbb
								<ul>
								<li>Tribune seharga min Rp. 500,000</li>
								<li>Festival seharga min Rp. 700,000</li>
								</ul>
								</li>
								<li>Periode pembelian berlaku 10 September &ndash; 17 September 2019</li>
								<li>Pembelian produk berlaku di blilbli, Bukalapak, Elevenia, Tokopedia dan Shopee;</li>
								<li>Pelanggan akan mendapatkan wristband dengan menunjukkan bukti pembelian dalam bentuk email, SMS ataupun bukti kirim pada hari H di booth yang sudah disediakan mulai pukul 13:00 WIB;</li>
								<li>Setiap pengunjung/penonton yang hadir dalam acara harus menggunakan nomor atau produk Smartfren dan memakai wristband;</li>
								</ol>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="box-activity" id="poin">
		<div class="container">
			<h3 class="TruenoBlk font-26">Redeem SmartPoin</h3>
			<div class="text-center">
			<p class="text-center font-20">Tukarkan 5000 SmartPoin dengan Spesial Invitation di aplikasi MySmartfren</p>
			<a href="https://mysf.id"  target="_blank" class="btn btn-warning font-22 color-white">Klik Di Sini</a> <br /> <br />
			</div>
		</div>
	</div>*/ ?>
	<!-- <div class="bg-white mar-50 mar-l mar-r">&nbsp;</div> -->
	<div class="box-activity" id="digq">
		<div class="container">
			<!-- <h3 class="TruenoBlk font-26">Digital Quiz</h3>
			<div class="row">
				<div class="col-sm-6">
					<div class="box">
						 <a href="/wow/pose-challenge">
							<img src="assets/img/DQ-pose.png">
						</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box">
						 <a href="/wow/lipsync-battle">
							<img src="assets/img/DQ-lipsync.png">
						</a>  
					</div>
				</div>
			</div> -->
			<img src="<?php echo base_url()?>assets/img/FULLY-BOOKED.jpg" width="100%">
		</div>
	</div>
	<div class="activity ticket">
		<div class="container">
			<div class="accordion myaccordion" id="accordionExample" style="margin: 0;">
			  <div class="card">
			    <div class="card-header" id="headingOne">
			      <h2 class="mb-0">
			        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			          FAQ & SYARAT  KETENTUAN WOW CONCERT
			        </button>
			      </h2>
			    </div>
			    <style type="text/css">
			    	.card-body p, .card-body ol li{color: #595959}
			    	.card-body ol,.card-body ul{margin-left: 30px;}
			    	.myaccordion .card-body p{margin-left: 72px;}
			    </style>
			    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
			      <div class="card-body">
			        	<p class="font-20">SYARAT &amp; KETENTUAN WOW CONCERT.</p>
						<ol start="1" type="1">
						<li>Setiap pengunjung/penonton yang hadir dalam acara harus menggunakan nomor Smartfren dan menunjukkan undangan atau memakai wristband.</li>
						<li>Pengunjung/penonton yang tidak menggunakan nomor Smartfren bisa membeli nomor Smartfren pada counter yang sudah disediakan.</li>
						<li>Smartfren tidak menjual tiket untuk acara ini. Undangan bisa didapatkan dengan membeli produk Smartfren atau mengiktui kuis di Instagram dan Radio yang sudah bekerjasama dengan Smartfren.</li>
						<li>Smartfren tidak bertanggung jawab apabila ada pihak-pihak yang menjual tiket diluar dari ketentuan yang sudah ditetapkan.</li>
						<li>Pastikan kamu membawa wristband sebelum datang ke acara. Kerusakan dan kehilangan wristband di luar tanggung jawab Smartfren.</li>
						<li>Pengunjung/penonton dibawah usia 14 tahun disarankan untuk didampingi oleh orang tua/wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku yang berlaku.</li>
						<li>Pengunjung/penonton membebaskan Smartfren sebagai penyelenggara acara dari segala tuntutan yang ditimbulkan pada saat penyelenggaraan acara kecuali hal-hal yang menjadi tanggung jawab penyelenggaraan acara (Force Majeure).</li>
						</ol>
						<p>&nbsp;</p>
						<p class="font-20">FAQ WOW CONCERT.</p>
						<p class="font-20">INFO UNDANGAN.</p>
						<ol>
						<li>Apa yang harus saya lakukan untuk mendapatkan undangan acara ini?</li>
						</ol>
						<p>Kamu bisa membeli produk - produk Smartfren di Galeri Smartfren, Booth Smartfren yang ada di FX di F1 #A4 dan Senayan City di LG le03. Atau kamu bisa mengikuti kuis yang ada di Instagram dan juga Radio (Virgin, Gen, Prambors). Atau kamu bisa menukarkan SmartPoin kamu pada aplikasi MySmartfren.</p>
						<ol start="2">
						<li>Berapa harga produk Smartfren yang harus di beli?</li>
						</ol>
						<ul>
						<li>Tribune seharga min Rp. 500,000</li>
						<li>Festival seharga min Rp. 700,000</li>
						</ul>
						<ol start="3">
						<li>Berapa jumlah undangan yang disediakan?</li>
						</ol>
						<ul>
						<li>Tribune sebanyak 1,200</li>
						<li>Festival sebanyak 500</li>
						</ul>
						<ol start="4">
						<li>Apa perbedaan antara undangan tribune dan festival?</li>
						</ol>
						<p>Untuk tribune undangan akan mendapatkan tempat duduk sedangkan festival akan berdiri selama acara namun lebih dekat ke panggung.</p>
						<ol start="5">
						<li>Mulai kapan undangan bisa didapatkan?</li>
						</ol>
						<p>Undangan bisa didapatkan mulai 4 &ndash; 17 September 2019.</p>
						<ol start="6">
						<li>Apakah undangan bisa didapatkan di semua Galeri Smartfren?</li>
						</ol>
						<p>Tidak, undangan hanya tersedia di 12 Galeri Smartfren antara lain Sabang, Ambassador, Botani Square Bogor, Metropolitan Mall Bekasi, ITC Fatmawati, Karawaci, ITC Roxy, Kelapa Gading, BSD, Bintaro, Depok, New Cibubur.</p>
						<ol start="7">
						<li>Apakah ada batasan jumlah pembelian produk Smartfren?</li>
						</ol>
						<p>Tidak ada batasan untuk pembelian produk, hanya setiap orang hanya diperbolehkan mendapatkan 1 undangan.</p>
						<ol start="8">
						<li>Apakah undangan akan langsung diberikan setelah pembelian?</li>
						</ol>
						<p>Ya, undangan akan langsung diberikan setelah pembelian dalam bentuk wristband.</p>
						<ol start="9">
						<li>Berapa jumlah SmartPoin yang harus ditukarkan untuk mendapatkan undangan?</li>
						</ol>
						<p>Untuk mendapatkan undangan kamu bisa menukarkan 5,000 SmartPoin.</p>
						<ol start="10">
						<li>Bagaimana cara menukarkan SmartPoin menjadi undangan?</li>
						</ol>
						<p>Kamu bisa masuk ke aplikasi MySmartfren dan pilih SmartPoin, pilih tukar SmartPoin, pilih banner WOW Concert dan temukan voucher kamu di halaman voucher saya.</p>
						<ol start="11">
						<li>Apakah voucher akan langsung saya terima di hari yang sama?</li>
						</ol>
						<p>Iyah, voucher akan diterima pada hari yang sama maksimum 1x24 jam</p>
						<ol start="12">
						<li>Apakah dengan voucher ini saya bisa langsung datang ke acaranya?</li>
						</ol>
						<p>Untuk voucher ini harus ditukarkan terlebih dahulu dengan wristband pada hari H acara, pada counter yang sudah disediakan.</p>
						<p>&nbsp;</p>
						<p class="font-20">INFO GENERAL ACARA.</p>
						<ol>
						<li>Dimana acara ini akan diadakan?</li>
						</ol>
						<p>Acara akan diadakan di Istora Senayan, Jakarta.</p>
						<ol start="2">
						<li>Kapan acara ini akan dilangsungkan?</li>
						</ol>
						<p>Acara akan dilangsungkan pada Jumat, 20 September 2019.</p>
						<ol start="3">
						<li>Jam berapa pengunjung diperbolehkan memasuki area acara?</li>
						</ol>
						<ul>
						<li>Off Air, gerbang akan dibuka mulai pukul 15:00. Kamu bisa menikmati dan keseruan yang ada di acara ini. Ada banyak stand makanan &amp; minuman, kamu bisa foto dengan Art Installation kekinian dan keseruan lainnya.</li>
						<li>On Air, gerbang akan dibuka mulai pukul 19:00. Acara akan disiarkan secara langsung di SCTV mulai pukul 20:00 &ndash; 22:00.</li>
						</ul>
						<ol start="4">
						<li>Apa saja persyaratan untuk masuk ke area acara?</li>
						</ol>
						<p>Semua yang masuk ke area acara harus menggunakan nomor Smartfren dan jangan lupa bawa wristband yang sudah kamu dapatkan.</p>
						<ol start="5">
						<li>Bagaimana jika saya tidak menggunakan nomor Smartfren?</li>
						</ol>
						<p>Kamu bisa membeli nomor Smartfren pada counter yang sudah disediakan.</p>
						<ol start="6">
						<li>Saya tidak menggunakan nomor Smartfren pada hape, namun saya menggunakan Modem WiFi Smartfren. Apakah bisa masuk ke dalam acara?</li>
						</ol>
						<p>Pengguna Modem WiFi Smartfren diperbolehkan masuk, selama dalam kondisi aktif dan bisa digunakan.</p>
						<ol start="7">
						<li>Apakah masuk ke dalam acara first come first serve?</li>
						</ol>
						<p>Ya, kamu akam masuk dengan cara first come first serve. Jadi kamu jangan datang terlambat.</p>
						<ol start="8">
						<li>Apakah ada batasan umur untuk masuk ke acara ini?</li>
						</ol>
						<p>Tidak ada, dihimbau untuk anak-anak dibawah umur 14 tahun wajib didampingi orang tua atau wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku yang berlaku.</p>
						<ol start="9">
						<li>Apabila saya harus keluar dari area acara, apakah diperbolehkan masuk kembali?</li>
						</ol>
						<p>Kamu diperbolehkan masuk kembali ke area acara selama kamu masuk melalui gerbang yang sesuai dengan wristband-mu.</p>
						<ol start="10">
						<li>Apakah diperbolehkan membawa makanan dari luar ke area acara ini?</li>
						</ol>
						<p>Makanan dan minuman dari luar dilarang untuk dibawa ke area acara. Stand makanan dan minuman yang kekinian akan disediakan di dalam area acara.</p>
						<ol start="11">
						<li>Apakah ada persyaratan pakaian yang akan dikenakan?</li>
						</ol>
						<p>Silahkan mengenakan pakaian senyaman mungkin selama dalam batasan kewajaran.</p>
						<ol start="12">
						<li>Bagaimana saya bisa mendapatkan informasi yang resmi pada hari menjelang acara?</li>
						</ol>
						<p>Kamu bisa mendapatkan informasi yang resmi dan paling baru dari situs <a href="http://www.smartfren.com/wowconcert" aria-invalid="true">www.smartfren.com/wow</a> dan akun Instagram @smartfrenworld.</p>
						<ol start="13">
						<li>Saya berhalangan datang ke acara tersebut, apakah undangan bisa saya berikan kepada orang lain?</li>
						</ol>
						<p>Undangan dapat dipindahtangankan selama memenuhi persyaratan untuk masuk ke area acara.</p>
						<ol start="14">
						<li>Apakah akan disediakan tempat penitipan barang?</li>
						</ol>
						<p>Tidak, penitipan barang tidak disiapkan. Jadi sebaiknya membawa barang-barang yang diperlukan saja.</p>
						<ol start="15">
						<li>Saya pengguna kursi roda dan ingin menghadiri konser ini. Apakah disediakan area khusus pengguna kursi roda?</li>
						</ol>
						<p>Ya, pengguna kursi roda memiliki area khusus dan fasilitas (toilet) di area acara yang dapat digunakan.</p>
						<ol start="16">
						<li>Apakah disediakan lapangan parkir di area acara?</li>
						</ol>
						<p>Lapangan parker tersedia di:</p>
						<p>&nbsp;</p>
						<p><img src="<?php echo base_url();?>assets/img/venue_map.jpeg" width="100%" border="0" /></p>
						<p>&nbsp;</p>
						<ol start="17">
						<li>Bagaimana cara menuju lokasi Acara?</li>
						</ol>
						<ul>
						<li>Pengguna Transjakarta:</li>
						</ul>
						<ul>
						<li style="list-style: none;">
						<ul style="list-style-type: circle;">
						<li>Halte Transjakarta GBK, masuk melalui pintu 5 (samping FX Sudirman)</li>
						<li>Halte Transjakarta JCC Senayan, masuk melalui pintu 8</li>
						</ul>
						</li>
						</ul>
						<ul>
						<li>Pengguna MRT:
						<ul style="list-style-type: circle;">
						<li>Pintu 6 samping Halte Transjakarta GBK</li>
						<li>Pintu 7 di Jl. Sudirman sebelum Hotel Sultan</li>
						</ul>
						</li>
						</ul>
						
						<p>&nbsp;</p>
						<p class="font-20">INFO WRISTBAND.</p>
						<ol>
						<li>Bagaimana cara menggunakan wristband?</li>
						</ol>
						<p>Tata cara penggunaan wristband dapat kamu lihat dan pelajari di kemasan wristband yang diberikan.</p>
						<ol start="2">
						<li>Bagaimana jika wristband saya hilang atau rusak?</li>
						</ol>
						<p>Wristband yang sudah diberikan kepada kamu sepenuhnya menjadi tanggung jawab kamu. Kami tidak bertanggung jawab atas kerusakan maupun kehilangan wristband tersebut. Kami sarankan untuk menjaga baik-baik wristband masing-masing sampai hari H.</p>
						<ol start="3">
						<li>Saya memasang wristband terlalu ketat, apakah penyelenggara bisa membantu untuk melonggarkannya?</li>
						</ol>
						<p>Wristband yang sudah diberikan kepada kamu maka akan menjadi tanggung jawab kamu sepenuhnya. Sebagai penyelenggara, kami tidak bisa melakukan apapun jika wristband yang kamu kenakan terlalu ketat.</p>
						<ol start="4">
						<li>Bisakah wristband saya dipakai orang lain?</li>
						</ol>
						<p>Bisa, selama kamu belum memasangkan wristband ditangan kamu.</p>
						<p>&nbsp;</p>
						<p class="font-20">INFO BELONGINGS.</p>
						<ol>
						<li>Apakah diperbolehkan membawa kamera ke area konser?</li>
						</ol>
						<p>Foto dan video hanya bisa diambil melalui telepon genggam. Kamera professional dan alat perekam video dan suara (termasuk kamera dengan lensa panjang atau bisa dilepas/pasang/diganti, perangkat sejenis GoPro) dilarang dibawa ke area konser.</p>
						<ol start="2">
						<li>Apakah saya diperbolehkan membawa obat-obatan pribadi ke area acara?</li>
						</ol>
						<p>Hanya obat-obatan dengan resep dokter. Pastikan kamu membawa resep tersebut agar obat-obatan yang kamu bawa dapat di cek kepentingannya. Tidak diperbolehkan untuk membawa obat-obatan terlarang jenis apapun ke dalam area acara.</p>
						<ol start="3">
						<li>Barang apa saja yang tidak boleh saya bawa ke dalam area acara?</li>
						</ol>
						<ul>
						<li>Bendera/Poster berukuran besar</li>
						<li>Drone, pesawat dengan remot control, mainan dan sejenisnya</li>
						<li>Obat-obatan terlarang dan sejenisnya</li>
						<li>Benda mudah terbakar</li>
						<li>Senjata/benda tajam</li>
						<li>Makanan dan minuman dari luar</li>
						<li>Binatang/hewan peliharaan</li>
						<li>Rantai/dompet berantai</li>
						<li>Terompet/alat musik/alat pembuat suara</li>
						<li>Kamera dengan lensa professional</li>
						</ul>
						<ol start="4">
						<li>Bagaimana jika saya membawa salah satu barang yang tidak diperbolehkan tersebut?</li>
						</ol>
						<p>Barang-barang yang dilarang tersebut akan langsung dibuang karena kami tidak menyediakan tempat penitipan barang. Jadi, tolong dipastikan untuk tidak membawa barang terlarang tersebut.</p>
						<ol start="5">
						<li>Apakah diperbolehkan membawa makanan atau minuman ke dalam area On Air?</li>
						</ol>
						<p>Kamu hanya diperbolehkan membawa 1 botol air mineral maks ukuran 600ml dan makanan dilarang tidak diperbolehkan masuk.</p>
						<ol start="6" type="1">
						<li>Apakah akan ada kotak pengumpulan hadiah untuk para artis?</li>
						</ol>
						<p>Kami tidak menyediakan kotak pengumpulan hadiah untuk para artis.</p>
						<ol start="7" type="1">
						<li>Apakah kami dapat membawa sign atau banner atau lightsticks?</li>
						</ol>
						<p>Sign/banner diperbolehkan selama ukuran masih dalam batas normal dan tidak menutupi pandangan penonton lainnya. Lightstick diperbolehkan selama tidak lebih dari 30cm.</p>
						<p class="font-20">INFO TENANTS.</p>
						<ol start="1" type="1">
						<li>Tenant apa saja yang tersedia di dalam area acara?</li>
						</ol>
						<p>Tenant yang kami sediakan di dalam area acara:</p>
						<ul>
						<li>Bagoja</li>
						<li>Tuku</li>
						<li>Doner Kebab</li>
						<li>Chattime</li>
						<li>Eatlah</li>
						<li>Phitix</li>
						<li>Pizza Mangkok</li>
						<li>Chocban</li>
						<li>Master Squid</li>
						<li>Sang Pisang</li>
						</ul>
						<ol start="2" type="1">
						<li>Bagaimana cara pembayaran untuk belanja pada tenant-tenant tersebut?</li>
						</ol>
						<p>Kamu dapat belanja menggunakan SmartPoin yang ada pada aplikasi MySmartfren kamu.</p>
						<ol start="3" type="1">
						<li>Bagaimana cara menggunakan SmartPoin untuk belanja pada tenant-tenant tersebut?</li>
						</ol>
						<p>Kamu masuk ke aplikasi MySmartfren dan pilih SmartPoin. Setelah itu kamu pilih WOW Concert dan pilih tenant yang akan kamu gunakan.</p>
						<ol start="4" type="1">
						<li>Bagaimana jika saya tidak memiliki SmartPoin pada aplikasi MySmartfren saya?</li>
						</ol>
						<p>Jika kamu tidak memiliki SmartPoin, kamu bisa melalukan pembelian paket pada aplikasi MySmartfren atau kamu bisa melakukan pembayaran dengan harga normal yang tertera pada tenant tersebut.</p>
						<ol start="5" type="1">
						<li>Berapa lama SmartPoin akan masuk ke aplikasi MySmartfren saya?</li>
						</ol>
						<p>SmartPoin akan kamu terima &nbsp;dalam jangka waktu 10 &ndash; 30 menit.</p>
						<ol start="6" type="1">
						<li>Apa kelebihan jika saya belanja menggunakan SmartPoin?</li>
						</ol>
						<p>Kamu akan dapat belanja di semua tenant makanan dan minuman hanya dengan 1 SmartPoin.</p>
						<ol start="7" type="1">
						<li>Apakah ada batasan jumlah pembelanjaan saya menggunakan SmartPoin?</li>
						</ol>
						<p>Kamu diperbolehkan melakukan pembelanjaan menggunakan SmartPoin 1 kali untuk setiap tenant yang tersedia.</p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<div class="container">
			<div class="home-venue row pad-50 pad-t pad-b">
				<!-- <div class="col-sm-3">
					<h1>Get Your Special Invitation at</h1>
					<div class="list"><p>Smartfren Galleries</p></div>
					<div class="list"><p>FX Sudirman</p></div>
					<div class="list"><p>Senayan City</p></div>
				</div> -->
				<div class="col-sm-4">
					<h1>Venue and Time</h1>
					<div class="venue">
						<div class="venue-img">
							<img src="assets/img/venue-dark.png">
						</div>
						<div class="venue-desc">
							<p style="font-size: 24px">Istora Senayan (JKT)<br />
								start from 3 PM till late</p>
						</div>
					</div>
					<div class="text-center">
						<a href="http://maps.google.com/maps?q=-6.220215,106.805717" target="_blank" class="btn btn-back" style="font-size: 20px!important;">Get Your Direction</a>
					</div>
				</div>
				<div class="col-md-4 mar-20 mar-r mar-l">
					<div class="act text-center">
						<a href="javascript:;" class="lu"  data-toggle="modal" data-target="#FT">
						<img src="assets/img/act-foodtruck.png" class="img-responsive">
						<p>FOOD TRUCKS <br /> &nbsp;</p>
					</a>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="FT" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      	<h1 class="text-center color-white">FOOD TRUCKS</h1>
					        <div class="row foodtruck">
								<?php foreach($foodtruck as $ft) :?>
									<div class="col-md-4 text-center">
										<div class="lu-img">
											<img src="<?php echo $ft['image']?>" class="img-responsive">
										</div>
										<p class="lu-name"><?php echo $ft['name']?></p>
									</div>
								<?php endforeach;?>
							</div>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
				<div class="col-md-4 mar-20 mar-r mar-l">
					<div class="act text-center">
						<a href="javascript:;" class="lu"  data-toggle="modal" data-target="#VM">
							<img src="assets/img/act-wowexperience.png" class="img-responsive">
							<p>VENUE MAP <br /> &nbsp;</p>
						</a>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="VM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					      		<?php $this->load->view('map');?>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<!-- <a href="<?php echo base_url()?>how-to-get" class="" style="font-size: 40px;display: block;position: fixed;bottom: 40%; right: 20px;color: #000; z-index: 9999">
	<img src="<?php echo base_url()?>assets/img/flying_button.png">
</a> -->