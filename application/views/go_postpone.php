<section id="concert-prod">
	<?php /*
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/wow_concert_postpone_mobile.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/wow_concert_postpone_desktop.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
	*/ ?>
	<img src="<?php echo base_url()?>assets/img/content_postpone.jpg" class="img-responsive" style="margin: 0 auto; width: 100%;">

	<div class="ticket">
		<div class="container">
			<div class="accordion myaccordion" id="accordionExample">
			  <div class="card">
			    <div class="card-header" id="headingOne">
			      <h2 class="mb-0">
			        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			          FAQ WOW CONCERT 2020 POSTPONED
			        </button>
			      </h2>
			    </div>
			    <style type="text/css">
			    	.card-body p, .card-body ol li{color: #595959}
			    	.card-body ol,.card-body ul{margin-left: 0px;}
			    	.myaccordion .card-body p{margin-left: 30px;}
			    </style>
			    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
			      <div class="card-body">
			      		<p></p>
						<ol>
						<li><strong>Kenapa WOW Concert di tunda pelaksanaanya?</strong></li>
						</ol>
						<p>WOW Concert 2020 di tunda untuk mencegah hal-hal yang tidak diinginkan terkait dengan wabah Virus Corona (COVID-19) yang saat ini sedang terjadi. Smartfren juga mengutamakan keselamatan para pelanggan.</p>
						<ol start="2">
						<li><strong>Kapan jadwal terbaru WOW Concert?</strong></li>
						</ol>
						<p>Kami akan menginfokan jadwal terbaru WOW Concert setelah keadaan lebih kondusif, namun untuk saat ini kami belum memutuskan jadwal pastinya.</p>
						<ol start="3">
						<li><strong>Saya sudah melakukan pembelian produk di Galeri Smartfren untuk mendapatkan <em>Special Invitation</em>, apakah itu tetap masih berlaku untuk jadwal WOW Concert selanjutnya?</strong></li>
						</ol>
						<p>Sebagai bentuk rasa terima kasih bagi pelanggan yang sudah melakukan transaksi di Galeri Smartfren akan diberikan <em>reward</em> dalam bentuk pulsa sebesar Rp 100.000. Pelanggan dapat mengunjungi Galeri Smartfren Sabang, Mal Ambassador, Kelapa Gading, Mal Metropolitan Bekasi dan BSD untuk klaim <em>reward</em> tersebut dengan membawa bukti pembelian/kuitansi. Dan untuk <em>Special Invitation</em> sudah <strong>tidak berlaku</strong> lagi untuk WOW Concert selanjutnya.</p>
						<ol start="4">
						<li><strong>Saya sudah melakukan pembelian produk di Bukalapak untuk mendapatkan <em>Special Invitation</em>, apakah itu tetap masih berlaku untuk jadwal WOW Concert selanjutnya?</strong></li>
						</ol>
						<p>Sebagai bentuk rasa terima kasih bagi pelanggan yang sudah melakukan transaksi di Bukalapak akan diberikan <em>reward</em> dalam bentuk pulsa sebesar Rp 100.000. Pelanggan dapat mengunjungi Galeri Smartfren yang sudah ditunjuk melalui email yang diterima untuk klaim <em>reward</em> tersebut dengan membawa bukti email. Dan untuk <em>Special Invitation</em> sudah <strong>tidak berlaku</strong> lagi untuk WOW Concert selanjutnya.</p>
						<ol start="5">
						<li><strong>Berapa lama <em>reward</em> tersebut akan diterima?</strong></li>
						</ol>
						<p><em>Reward</em> akan diproses dalam waktu 7 hari kerja ke nomor Smartfren yang sama saat melakukan pembelian.</p>
						<ol start="6">
						<li><strong>Saya melakukan pembelian lebih dari 1 produk Smartfren, berapa jumlah reward yang saya dapatkan?</strong></li>
						</ol>
						<p><em>Reward</em> akan diproses berdasarkan jumlah bukti pembelian/kuitansi. Apabila pelanggan melakukan transaksi lebih dari 1 produk tetap akan mendapatkan 1 <em>reward</em> saja.</p>
						<ol start="7">
						<li><strong>Sampai kapan batas klaim reward ini? </strong></li>
						</ol>
						<p>Periode klaim <em>reward</em> mulai 16 Maret &ndash; 31 Maret 2020.</p>
						<ol start="8">
						<li><strong>Saya tidak menginginkan <em>reward</em> tersebut dan tetap ingin mendapatkan <em>Special Invitation</em> untuk WOW Concert selanjutnya?</strong></li>
						</ol>
						<p>Smartfren meminta pengertian dari para pelanggan setia dikarenakan WOW Concert ditunda hingga waktu yang belum ditentukan. Oleh karena itu,&nbsp; Smartfren tetap akan memberikan <em>reward </em>dalam bentu pulsa Rp 100.000 sesuai dengan poin-poin diatas.</p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</section>