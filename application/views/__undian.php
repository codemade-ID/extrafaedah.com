<section class="bg-white">
	<img src="<?php echo base_url()?>assets/img/banner-top-undian.png" class="img-responsive" style="margin: 0 auto">
	<div class="container">
		<p class="text-center color-hitam font-24 mar-30 mar-l mar-r TruenoBd">Hadiah Undian Smartfren WOW Tahap I</p>
		<div class="row">
			<?php foreach($hadiah as $h) :?>
				<div class="col-md-4 text-center">
					<div class="lu-img">
						<img src="<?php echo $h['image']?>" class="img-responsive">
					</div>
				</div>
			<?php endforeach;?>
		</div>
	</div>
	<div class="container">
		<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          FAQ PROGRAM UNDIAN SMARTFREN WOW
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol{margin-left: 30px;}
		    </style>
		    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p>Q: Bagaimana cara bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan akan mendapatkan Kupon Undian melalui:</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Aktifasi Kartu Perdana baru minimal Rp.30.000,- akan mendapatkan 1 Kupon Undian;</li>
					<li>Pertama kali mengunduh dan registrasi aplikasi MySmartfren akan mendapatkan 2 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di luar aplikasi MySmartfren akan mendapatkan 1 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di aplikasi MySmartfren akan mendapatkan 3 Kupon Undian.</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Siapa saja yang bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Semua pelanggan Smartfren baik Pascabayar maupun Prabayar dapat mengikuti Program Undian Smartfren WOW , kecuali:&rdquo;</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Transaksi auto renewal;</li>
					<li>Aktifasi Kartu Perdana baru dibawah Rp.30.000,-;</li>
					<li>Pembelian paket layanan Smartfren dibawah Rp.30,000,-</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Berapa banyak pemenang dan apa saja hadiah pada Program Undian Smartfren WOW ini?</p>
					<p>A: Jumlah pemenang pada Program Undian Smartfren WOW adalah 1,000 Pemenang. Dengan Hadiah Utama berupa 1 Rumah bernilai milyaran, 1 Mobil Innova Tipe G A/T, 3 Vespa tipe LS 125 dan Hadiah Undian lainnya berupa 5 iPhone XS (64GB), 10 Paket Liburan Bali (3H2M), 60 Samsung A50, 120 Bluetooth Swarovski, 300 Voucher Pulsa senilai 500 ribu dan Voucher Belanja senilai 250 ribu.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang dimaksud dengan Kupon Undian?</p>
					<p>A: Kupon adalah bentuk kepemilikan nomor undian yang akan digunakan untuk pengundian hadiah.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan periode Program Undian Smartfren WOW berlangsung?</p>
					<p>A: Periode Undian Smartfren WOW berlaku mulai 10 September 2019 &ndash; 31 Oktober 2019. Program Undian Smartfren WOW dibagi menjadi 2 Tahapan:</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Tahapan I : 10 September 2019 &ndash; 30 September 2019</li>
					<li>Tahapan II : 01 Oktober 2019 &ndash; 31 Oktober 2019</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Kapan Saya menerima Kupon Undian setelah melakukan pengaktifan nomor/pembelian paket?</p>
					<p>A: Kupon Undian akan diterima paling lama 3 jam.</p>
					<p>&nbsp;</p>
					<p>Q: Dimana Saya dapat melihat Kupon Undian?</p>
					<p>A: Pelanggan dapat melihat Kupon Undian di halaman Smartpoint lalu klik &ldquo;Voucher Saya&rdquo; pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian tersebut memiliki masa kadaluarsa?</p>
					<p>A: Ya,&nbsp; Kupon memiliki masa kadaluarsa yang akan tertera pada masing-masing kupon sesuai dengan periode Program Undian Smartfren WOW.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian yang sudah kadaluarsa bisa diikutsertakan kembali pada tahapan berikutnya?</p>
					<p>A: Tidak, Kupon Undian yang sudah kadaluarsa akan hangus. Pelanggan harus melakukan transaksi kembali untuk mendapatkan Kupon Undian pada tahapan berikutnya.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa dipindahkan atau dibagikan ke nomor Smartfren lain?</p>
					<p>A: Tidak, Kupon Undian yang Anda miliki tidak bisa dipindahkan atau dibagikan ke nomor Smartfren lain.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa saya tukarkan dengan uang?</p>
					<p>A: Kupon Undian tidak dapat ditukarkan dengan uang. Kupon Undian hanya digunakan sebagai bukti kepemilikan nomor undian unik yang tertera pada setiap Kupon Undian yang ada pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang terjadi jika saya mengganti nomor Smartfren saya?</p>
					<p>A: Kupon Undian yang terdapat pada nomor sebelumnya akan hangus apabila nomor Anda sudah tidak aktif lagi.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah 1 pelanggan dapat memenangkan lebih dari 1 hadiah?</p>
					<p>A: Untuk Hadiah Utama pelanggan hanya mendapatkan 1 jenis hadiah selama periode Program Undian Smartfren WOW berlangsung. Namun, pelanggan berhak untuk mendapatkan kesempatan untuk memenangkan hadiah undian lainnya di setiap tahapan.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan pengundian akan dilakukan?</p>
					<p>A: Pengundian akan dilakukan setiap bulan pada tanggal 5 setiap bulannya atau selambat-lambatnya 14 hari setelah periode per tahapan selesai.</p>
					<p>&nbsp;</p>
					<p>Q: Bagaimana cara mengetahui pemenang Program Undian Smartfren WOW?</p>
					<p>A: Untuk pemenang Hadiah Utama, pelanggan akan dihubungi langsung pada saat pengundian berlangsung, sedangkan untuk pemenang hadiah undian lainnya bisa akan diinformasikan pada aplikasi MySmartfren masing-masing pemenang dan website Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Saya dikenakan biaya untuk mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan tidak akan dikenakan biaya untuk mengikuti Program Undian Smartfren WOW, hanya untuk pajak Hadiah Utama ditanggung oleh masing-masing pemenang sedangkan hadiah undian lainnya akan ditanggung oleh Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah hadiah yang saya menangkan bisa ditukar atau diuangkan?</p>
					<p>A: Hadiah yang sudah dimenangkan tidak dapat ditukar ataupun diuangkan, kecuali untuk Hadiah Utama Rumah dengan mengikuti syarat &amp; ketentuan yang berlaku.</p>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>

<div class="container">
		<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          S&K PROGRAM UNDIAN SMARTFREN WOW
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol{margin-left: 30px;}
		    </style>
		    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p>Q: Bagaimana cara bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan akan mendapatkan Kupon Undian melalui:</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Aktifasi Kartu Perdana baru minimal Rp.30.000,- akan mendapatkan 1 Kupon Undian;</li>
					<li>Pertama kali mengunduh dan registrasi aplikasi MySmartfren akan mendapatkan 2 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di luar aplikasi MySmartfren akan mendapatkan 1 Kupon Undian;</li>
					<li>Melakukan pembelian paket layanan Smartfren di aplikasi MySmartfren akan mendapatkan 3 Kupon Undian.</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Siapa saja yang bisa mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Semua pelanggan Smartfren baik Pascabayar maupun Prabayar dapat mengikuti Program Undian Smartfren WOW , kecuali:&rdquo;</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Transaksi auto renewal;</li>
					<li>Aktifasi Kartu Perdana baru dibawah Rp.30.000,-;</li>
					<li>Pembelian paket layanan Smartfren dibawah Rp.30,000,-</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Berapa banyak pemenang dan apa saja hadiah pada Program Undian Smartfren WOW ini?</p>
					<p>A: Jumlah pemenang pada Program Undian Smartfren WOW adalah 1,000 Pemenang. Dengan Hadiah Utama berupa 1 Rumah bernilai milyaran, 1 Mobil Innova Tipe G A/T, 3 Vespa tipe LS 125 dan Hadiah Undian lainnya berupa 5 iPhone XS (64GB), 10 Paket Liburan Bali (3H2M), 60 Samsung A50, 120 Bluetooth Swarovski, 300 Voucher Pulsa senilai 500 ribu dan Voucher Belanja senilai 250 ribu.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang dimaksud dengan Kupon Undian?</p>
					<p>A: Kupon adalah bentuk kepemilikan nomor undian yang akan digunakan untuk pengundian hadiah.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan periode Program Undian Smartfren WOW berlangsung?</p>
					<p>A: Periode Undian Smartfren WOW berlaku mulai 10 September 2019 &ndash; 31 Oktober 2019. Program Undian Smartfren WOW dibagi menjadi 2 Tahapan:</p>
					<ol style="list-style-type: lower-alpha;">
					<li>Tahapan I : 10 September 2019 &ndash; 30 September 2019</li>
					<li>Tahapan II : 01 Oktober 2019 &ndash; 31 Oktober 2019</li>
					</ol>
					<p>&nbsp;</p>
					<p>Q: Kapan Saya menerima Kupon Undian setelah melakukan pengaktifan nomor/pembelian paket?</p>
					<p>A: Kupon Undian akan diterima paling lama 3 jam.</p>
					<p>&nbsp;</p>
					<p>Q: Dimana Saya dapat melihat Kupon Undian?</p>
					<p>A: Pelanggan dapat melihat Kupon Undian di halaman Smartpoint lalu klik &ldquo;Voucher Saya&rdquo; pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian tersebut memiliki masa kadaluarsa?</p>
					<p>A: Ya,&nbsp; Kupon memiliki masa kadaluarsa yang akan tertera pada masing-masing kupon sesuai dengan periode Program Undian Smartfren WOW.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian yang sudah kadaluarsa bisa diikutsertakan kembali pada tahapan berikutnya?</p>
					<p>A: Tidak, Kupon Undian yang sudah kadaluarsa akan hangus. Pelanggan harus melakukan transaksi kembali untuk mendapatkan Kupon Undian pada tahapan berikutnya.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa dipindahkan atau dibagikan ke nomor Smartfren lain?</p>
					<p>A: Tidak, Kupon Undian yang Anda miliki tidak bisa dipindahkan atau dibagikan ke nomor Smartfren lain.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Kupon Undian bisa saya tukarkan dengan uang?</p>
					<p>A: Kupon Undian tidak dapat ditukarkan dengan uang. Kupon Undian hanya digunakan sebagai bukti kepemilikan nomor undian unik yang tertera pada setiap Kupon Undian yang ada pada aplikasi MySmartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apa yang terjadi jika saya mengganti nomor Smartfren saya?</p>
					<p>A: Kupon Undian yang terdapat pada nomor sebelumnya akan hangus apabila nomor Anda sudah tidak aktif lagi.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah 1 pelanggan dapat memenangkan lebih dari 1 hadiah?</p>
					<p>A: Untuk Hadiah Utama pelanggan hanya mendapatkan 1 jenis hadiah selama periode Program Undian Smartfren WOW berlangsung. Namun, pelanggan berhak untuk mendapatkan kesempatan untuk memenangkan hadiah undian lainnya di setiap tahapan.</p>
					<p>&nbsp;</p>
					<p>Q: Kapan pengundian akan dilakukan?</p>
					<p>A: Pengundian akan dilakukan setiap bulan pada tanggal 5 setiap bulannya atau selambat-lambatnya 14 hari setelah periode per tahapan selesai.</p>
					<p>&nbsp;</p>
					<p>Q: Bagaimana cara mengetahui pemenang Program Undian Smartfren WOW?</p>
					<p>A: Untuk pemenang Hadiah Utama, pelanggan akan dihubungi langsung pada saat pengundian berlangsung, sedangkan untuk pemenang hadiah undian lainnya bisa akan diinformasikan pada aplikasi MySmartfren masing-masing pemenang dan website Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah Saya dikenakan biaya untuk mengikuti Program Undian Smartfren WOW?</p>
					<p>A: Pelanggan tidak akan dikenakan biaya untuk mengikuti Program Undian Smartfren WOW, hanya untuk pajak Hadiah Utama ditanggung oleh masing-masing pemenang sedangkan hadiah undian lainnya akan ditanggung oleh Smartfren.</p>
					<p>&nbsp;</p>
					<p>Q: Apakah hadiah yang saya menangkan bisa ditukar atau diuangkan?</p>
					<p>A: Hadiah yang sudah dimenangkan tidak dapat ditukar ataupun diuangkan, kecuali untuk Hadiah Utama Rumah dengan mengikuti syarat &amp; ketentuan yang berlaku.</p>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>



<section class="bg-red">
	<p class="text-center font-24 mar-30 mar-l mar-r TruenoBd">Cara Mudah Ikutan Undian Smartfren WOW</p>
	<div class="row pad-30 text-center">
		<div class="col-md-4">
			<img src="<?php base_url()?>assets/img/ikutan-1.png" class="img-responsive">
			<p class="lu-name">Aktifkan Kartu Perdana <br /> Smartfren min 30RB</p>
		</div>
		<div class="col-md-4">
			<a href="https://linktr.ee/Smartfren" target="_blank">
				<img src="<?php base_url()?>assets/img/ikutan-2.png" class="img-responsive">
			</a>
			<p class="lu-name">Download <br /> Aplikasi MySmartfren</p>
		</div>
		<div class="col-md-4">
			<img src="<?php base_url()?>assets/img/ikutan-3.png" class="img-responsive">
			<p class="lu-name">Beli Paket Layanan <br /> Smartfren min 30RB</p>
		</div>
	</div>
</section>
<section class="bg-white">
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="resp-container">
				<iframe class="resp-iframe" width="100%" height="642" src="https://www.youtube.com/embed/aMF6YGvhlMw?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</section>
<section class="bg-white">
	<p class="text-center color-hitam font-24 TruenoBd">Cara Cek Kupon Undian Smartfren WOW</p>
	<div class="row text-center">
		<div class="col-md-6">
			<a href="https://linktr.ee/Smartfren">
			<img src="<?php base_url()?>assets/img/mysf-phone.png" class="img-responsive img-sfid">
			</a>
		</div>
		<div class="col-md-6">
			<ul class="check-kupon">
				<li>
					<img src="<?php echo base_url()?>assets/img/check-1.png">
					<div class="text">
						<p class="head">Download aplikasi MySmartfren</p>
						<p>Di <a href="apps.apple.com/id/app/mysmartfren/id1209898190" target="_blank">App Store</a> atau <a href="https://play.google.com/store/apps/details?id=com.smartfren" target="_blank">Play Store</a></p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-2.png">
					<div class="text">
						<p class="head">Daftar atau Masuk di aplikasi MySmartfren</p>
						<p>Bisa menggunakan email atau nomor Smartfren</p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-3.png">
					<div class="text">
						<p class="head">Pilih Smartpoin</p>
						<p>Cari lambang koin pada halaman</p>
					</div>
				</li>
				<li>
					<img src="<?php echo base_url()?>assets/img/check-4.png">
					<div class="text">
						<p class="head">Temukan Kupon</p>
						<p>Temukan kupon pada halaman "Voucher Saya"</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
</section>