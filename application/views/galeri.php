<section id="gallery">
	<div class="container">
		<div class="row list-gallery">
			<div class="col-sm-6">
				<p>Galeri Smartfren sabang</p>
				<p>Jl. H. Agus Salim No.45 Sabang Jakarta Pusat 10340</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.185294,106.82627" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Ambasador</p>
				<p>Mall Ambasador Lt. 3 No.35 Jl. Prof. Dr.Satrio No.14, Kuningan Setiabudi, Jakarta Selatan</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.223915,106.82731" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Botani square</p>
				<p>Botani Square lantai GF, Bogor</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.601404,106.80766" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Metropolitan Mall Bekasi</p>
				<p>Mal Metropolitan II Lt.2 No.05 Jl. KH. Noer Alie Bekasi Selatan 17148</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.248813,106.99037" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri ITC Fatmawati</p>
				<p>Komp. Ruko ITC Fatmawati Blok A2 No. 1 Jakarta Selatan</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.2631,106.797" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Karawaci</p>
				<p>Supermall Karawaci Lt LG Unit E2</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.3,106.61255" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri ITC Roxy mas</p>
				<p>ITC Roxy Mas Lt.1 No.14-15 Gd. ITC Roxymas Jl. K.H. Hasyim Ashari Jakarta 10150</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.166359,106.80339" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Kelapa Gading</p>
				<p>JL.Boulevard Raya Blok LA 4 No.5 Kelapa Gading Jakarta Utara</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.159543,106.90577" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri BSD</p>
				<p>Jl.Pahlawan Seribu Lot 12A CBD BSD Tangerang Selatan</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.29641,106.67083" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Depok</p>
				<p>Ruko ITC Depok No.12 RT 04/12 JL Margonda Raya, Kel Pancoran Mas Depok</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.393389,106.82291" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri Bintaro</p>
				<p>JL. BINTARO UTAMA SEKTOR 3A BLOK E NO.53 JAKARTA</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.2722684,106.7427914" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p>Galeri New Cibubur</p>
				<p>JL. Transyogi No. 9C RT 005 RW 09 Kel. Harjamukti, Kec. Cimanggis, Depok - Jawa Barat 16454 ( Samping Cibubur Point ).</p>
				<!-- <p>Buka : Tutup pukul 18.30</p> -->
				<p><a href="http://maps.google.com/maps?q=-6.37584,106.89844" class="location" target="_blank"><img src="<?php echo base_url();?>assets/img/location.png"> See Location</a></p>
			</div>
			<div class="col-sm-6">
				<p><a href="https://www.smartfren.com/id/galeri/" class="color-white" target="_blank" style="color: #fff">Other Gallery &nbsp;<i class="fa fa-caret-right"></i></a> </p>
			</div>
			
		</div>
	</div>
</section>