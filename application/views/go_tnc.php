
<div class="ticket">
	<div class="container">
		<div class="accordion myaccordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header" id="headingOne">
		      <h2 class="mb-0">
		        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
		          FAQ & SYARAT  KETENTUAN WOW CONCERT
		        </button>
		      </h2>
		    </div>
		    <style type="text/css">
		    	.card-body p, .card-body ol li{color: #595959}
		    	.card-body ol,.card-body ul{margin-left: 0px;}
		    	.myaccordion .card-body p{margin-left: 30px;}
		    </style>
		    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        	<p><strong>SYARAT &amp; KETENTUAN SMARTFREN WOW CONCERT 2020.</strong></p>
					<ol start="1" type="1">
					<li>Buat kamu yang hadir harus pakai nomor Smartfren, download aplikasi MySmartfren dan jangan lupa untuk menukarkan undangan dengan wristband.</li>
					<li>Kamu yang belum pakai Smartfren bisa membeli nomor Smartfren pada counter yang tersedia.</li>
					<li><strong>Smartfren tidak menjual tiket</strong> untuk acara ini.</li>
					<li>Kamu bisa dapat undangan dengan membeli produk Smartfren di Galeri Smartfren dan di Bukalapak. Atau bisa dengan cara menukarkan SmartPoin pada aplikasi MySmartfren dan juga mengikuit kuis di media sosial atau rekanan yang bekerjasama dengan Smartfren.</li>
					<li>Smartfren tidak bertanggung jawab apabila ada pihak-pihak yang menjual tiket diluar dari ketentuan yang sudah ditetapkan.</li>
					<li>Mekanisme penukaran wristband akan diinfokan melalui email.</li>
					<li>Pastikan kamu menggunakan wristband sebelum datang ke acara. Kerusakan dan kehilangan wristband di luar tanggung jawab Smartfren.</li>
					<li>Pengunjung dibawah usia 14 tahun disarankan untuk didampingi oleh orang tua/wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku yang berlaku.</li>
					<li>Pengunjung membebaskan Smartfren sebagai penyelenggara acara dari segala tuntutan yang ditimbulkan pada saat penyelenggaraan acara kecuali hal-hal yang menjadi tanggung jawab penyelenggaraan acara (<em>Force Majeure</em>).<strong>&nbsp;</strong></li>
					</ol>
					<p>&nbsp;</p>
					<p><strong>FAQ SMARTFREN WOW CONCERT 2020.</strong></p>
					<p><strong>INFO UNDANGAN.</strong></p>
					<ol>
					<li><strong>Bagaimana caranya buat dapat undangannya?</strong></li>
					</ol>
					<p>Gini caranya;</p>
					<ul>
					<li>Beli produk bundling yang tersedia di Galeri Smartfren atau di Bukalapak.</li>
					<li>Bisa juga tukar SmartPoin di aplikasi MySmartfren.</li>
					<li>Atau kamu bisa ikut kuis di media sosial atau rekanan yang bekerjasama dengan Smartfren.</li>
					</ul>
					<ol start="2">
					<li><strong>Apa aja jenis produk bundlingnya?</strong></li>
					</ol>
					<ul>
					<li>Kalau kamu beli di Galeri Smartfren Jabodetabek tersedia paket bundling Modem WiFi M6x seharga Rp 499.000,- dan akan mendapatkan 2 Undangan.</li>
					<li>Kalau kamu beli di Bukalapak ada 2 pilihan paket bundling;
					<ul style="list-style-type: circle;">
					<li>Paket Bundling Modem WiFi M6x seharga Rp 499.000,- akan mendapatkan 2 Undangan.</li>
					<li>Paket Bundling Kartu Perdana Unlimited + Pulsa Rp 150.000 seharga Rp 225.000,- &nbsp;akan mendapatkan benefit Vidio Premiere Premium 3 bulan serta 1 Undangan.</li>
					</ul>
					</li>
					</ul>
					<ol start="3">
					<li><strong>Apa perbedaan antara undangan Tribune dan Festival?</strong></li>
					</ol>
					<p>Untuk tribune kamu bisa duduk dan kalau festival kamu akan berdiri selama acara tapi lebih dekat ke panggung.</p>
					<ol start="4">
					<li><strong>Kapan undangan bisa didapatkan?</strong></li>
					</ol>
					<p>Undangan bisa didapatkan mulai 25 Februari 2020.</p>
					<ol start="5">
					<li><strong>Apakah undangan bisa didapatkan di semua Galeri Smartfren?</strong></li>
					</ol>
					<p>Tidak, undangan hanya tersedia di 5 Galeri Smartfren diantaranya Gallery Smartfren Sabang, Ambassador, Kelapa Gading, Metropolitan Mall Bekasi, BSD.</p>
					<ol start="6">
					<li><strong>Apakah ada batasan jumlah pembelian produk Smartfren?</strong></li>
					</ol>
					<ul>
					<li>Untuk pembelian di Galeri Smartfren, 1 KTP hanya diperbolehkan melakukan pembelian maksimal 2 unit produk bundling dan akan mendapatkan maksimal 4 undangan.</li>
					<li>Untuk pembelian di Bukalapak, 1 akun hanya diperbolehkan membeli maksimal 2 unit produk bundling dan akan mendapatkan maksimal 4 undangan.</li>
					</ul>
					<ol start="7">
					<li><strong>Apakah undangan akan langsung diberikan setelah pembelian?</strong></li>
					</ol>
					<p>Undangan akan diterima dalam bentuk email konfirmasi yang berisikan detail penukaran wristband.</p>
					<ol start="8">
					<li><strong>Berapa jumlah SmartPoin yang harus ditukarkan untuk mendapatkan undangan?</strong></li>
					</ol>
					<p>Untuk mendapatkan undangan kamu bisa menukarkan 5,000 SmartPoin.</p>
					<ol start="9">
					<li><strong>Kapan penukaran SmartPoin akan dibuka?</strong></li>
					</ol>
					<p>Penukaran SmartPoin akan mulai 9 Maret 2020.</p>
					<ol start="10">
					<li><strong>Bagaimana cara menukarkan SmartPoin menjadi undangan?</strong></li>
					</ol>
					<p>Kamu bisa masuk ke aplikasi MySmartfren dan pilih SmartPoin, pilih tukar SmartPoin, pilih banner WOW Concert dan temukan voucher kamu di halaman voucher saya.</p>
					<ol start="11">
					<li><strong>Apakah voucher akan langsung akan diterima di hari yang sama?</strong></li>
					</ol>
					<p>Iyah, voucher akan diterima pada hari yang sama maksimum 1x24 jam</p>
					<ol start="12">
					<li><strong>Apakah dengan voucher ini langsung bisa datang ke acaranya?</strong></li>
					</ol>
					<p>Untuk voucher ini harus ditukarkan terlebih dahulu dengan wristband pada hari H acara, pada counter yang tersedia.</p>
					<p>&nbsp;</p>
					<p><strong>INFO GENERAL ACARA.</strong></p>
					<ol>
					<li><strong>Dimana acara ini akan diadakan?</strong></li>
					</ol>
					<p>Acara akan diadakan di ICE BSD HALL 3 &ndash; 3A.</p>
					<p>Jl. BSD Grand Boulevard Maxwell Raya No. 1, Padegangan, Tangerang</p>
					<ol start="2">
					<li><strong>Kapan acara ini akan dilangsungkan?</strong></li>
					</ol>
					<p>Acaranya akan dilangsungkan pada Jumat, 17 APRIL 2020.</p>
					<ol start="3">
					<li><strong>Jam berapa pengunjung diperbolehkan memasuki area acara?</strong></li>
					</ol>
					<p>Kamu bisa memasuki area mulai pukul 15:00 WIB.</p>
					<ol start="4">
					<li><strong>Apa aja persyaratan untuk masuk ke area acara?</strong></li>
					</ol>
					<p>Kamu yang mau masuk ke area acara harus pakai nomor Smartfren dan mendownload aplikasi MySmartfren serta jangan lupa menggunakan <strong>wristband</strong> yang sudah kamu dapatkan.</p>
					<ol start="5">
					<li><strong>Kalau aku gak pakai Nomor Smartfren, gak bisa masuk donk?</strong></li>
					</ol>
					<p>Bisa donk. Nanti kamu bisa beli aja nomor Smartfren pada counter yang tersedia di lokasi acara.</p>
					<ol start="6">
					<li><strong>Aku gak pakai nomor Smartfren di hape, tapi ada MiFi. Boleh masuk gak?</strong></li>
					</ol>
					<p>Boleh donk. Pengguna Modem WiFi Smartfren diperbolehkan masuk tapi dalam kondisi aktif dan bisa digunakan.</p>
					<ol start="7">
					<li><strong>Apakah masuk ke dalam acara <em>first come first serve</em>?</strong></li>
					</ol>
					<p>Ya, kamu akam masuk dengan cara <em>first come first serve</em>. Jadi kamu jangan datang terlambat.</p>
					<ol start="8">
					<li><strong>Apakah ada batasan umur untuk masuk ke acara ini?</strong></li>
					</ol>
					<p>Tidak ada, dihimbau untuk anak-anak dibawah umur 14 tahun wajib didampingi orang tua atau wali yang sudah dewasa dan memiliki kartu identitas (KTP/SIM/PASPOR/KITAS) yang berlaku yang berlaku.</p>
					<ol start="9">
					<li><strong>Apabila harus keluar dari area acara, apakah nanti boleh masuk kembali?</strong></li>
					</ol>
					<p>Kamu diperbolehkan masuk kembali ke area acara selama kamu masuk melalui gerbang yang sesuai dengan wristband-mu.</p>
					<ol start="10">
					<li><strong>Nanti boleh bawa makanan/minuman dari luar gak?</strong></li>
					</ol>
					<p>Tenang aja. Buat kamu yang setia pakai Smartfren akan mendapatkan Voucher F&amp;B yang bisa dinikmati dengan 1 Makanan dan 1 Minuman.</p>
					<ol start="11">
					<li><strong>Apakah ada persyaratan pakaian yang akan dikenakan?</strong></li>
					</ol>
					<p>Silahkan mengenakan pakaian senyaman mungkin selama dalam batasan kewajaran.</p>
					<ol start="12">
					<li><strong>Informasi yang resmi pada hari menjelang acara bisa dicek dimana?</strong></li>
					</ol>
					<p>Kamu bisa mendapatkan informasi yang resmi dan paling baru dari situs <a href="http://www.smartfren.com/wowconcert" aria-invalid="true">www.smartfren.com/wow</a>/concert dan akun Instagram @smartfrenworld.</p>
					<ol start="13">
					<li><strong>Aku berhalangan datang ke acara tersebut, apakah undangan bisa diberikan kepada orang lain?</strong></li>
					</ol>
					<p>Undangan dapat dipindahtangankan, yang penting memenuhi persyaratan untuk masuk ke area acara.</p>
					<ol start="14">
					<li><strong>Apakah akan disediakan tempat penitipan barang?</strong></li>
					</ol>
					<p>Maaf banget, penitipan barang tidak disiapkan. Jadi sebaiknya kamu bawa barang-barang yang perlu aja.</p>
					<ol start="15">
					<li><strong>Aku pengguna kursi roda dan ingin menghadiri konser ini. Apakah disediakan area khusus pengguna kursi roda?</strong></li>
					</ol>
					<p>Ada kok. Pengguna kursi roda nanti memiliki area khusus dan fasilitas (toilet) di area acara yang dapat digunakan.</p>
					<p>&nbsp;</p>
					<p><strong>INFO WRISTBAND.</strong></p>
					<ol>
					<li><strong>Bagaimana cara menggunakan wristband?</strong></li>
					</ol>
					<p>Tata cara penggunaan wristband dapat kamu lihat dan pelajari di kemasan wristband yang diberikan.</p>
					<ol start="2">
					<li><strong>Bagaimana jika wristband hilang atau rusak?</strong></li>
					</ol>
					<p>Wristband yang sudah diberikan kepada kamu sepenuhnya menjadi tanggung jawab kamu. Kami tidak bertanggung jawab atas kerusakan maupun kehilangan wristband tersebut. Kami sarankan untuk menjaga baik-baik wristband masing-masing sampai hari H.</p>
					<ol start="3">
					<li><strong>Aku pasang wristband terlalu ketat, apakah penyelenggara bisa membantu untuk melonggarkannya?</strong></li>
					</ol>
					<p>Wristband yang sudah diberikan kepada kamu maka akan menjadi tanggung jawab kamu sepenuhnya. Sebagai penyelenggara, kami tidak bisa melakukan apapun jika wristband yang kamu kenakan terlalu ketat.</p>
					<p><strong>&nbsp;</strong></p>
					<p><strong>INFO BELONGINGS.</strong></p>
					<ol>
					<li><strong>Apakah diperbolehkan membawa kamera ke area konser?</strong></li>
					</ol>
					<p>Foto dan video hanya bisa diambil melalui telepon genggam. Kamera professional dan alat perekam video dan suara (termasuk kamera dengan lensa panjang atau bisa dilepas/pasang/diganti, perangkat sejenis GoPro) dilarang dibawa ke area konser.</p>
					<ol start="2">
					<li><strong>Apakah diperbolehkan membawa obat-obatan pribadi ke area acara?</strong></li>
					</ol>
					<p>Hanya obat-obatan dengan resep dokter. Pastikan kamu membawa resep tersebut agar obat-obatan yang kamu bawa dapat di cek kepentingannya. Tidak diperbolehkan untuk membawa obat-obatan terlarang jenis apapun ke dalam area acara.</p>
					<ol start="3">
					<li><strong>Barang apa saja yang tidak boleh dibawa ke dalam area acara?</strong></li>
					</ol>
					<ul>
					<li>Bendera/Poster berukuran besar</li>
					<li>Drone, pesawat dengan remot control, mainan dan sejenisnya</li>
					<li>Obat-obatan terlarang dan sejenisnya</li>
					<li>Benda mudah terbakar</li>
					<li>Senjata/benda tajam</li>
					<li>Makanan dan minuman dari luar</li>
					<li>Binatang/hewan peliharaan</li>
					<li>Rantai/dompet berantai</li>
					<li>Terompet/alat musik/alat pembuat suara</li>
					<li>Kamera dengan lensa professional</li>
					</ul>
					<ol start="4">
					<li><strong>Bagaimana jika membawa salah satu barang yang tidak diperbolehkan tersebut?</strong></li>
					</ol>
					<p>Barang-barang yang dilarang tersebut akan langsung dibuang karena kami tidak menyediakan tempat penitipan barang. Jadi, tolong dipastikan untuk tidak membawa barang terlarang tersebut.</p>
					<ol start="5" type="1">
					<li><strong>Apakah akan ada kotak pengumpulan hadiah untuk para artis?</strong></li>
					</ol>
					<p>Kami tidak menyediakan kotak pengumpulan hadiah untuk para artis.</p>
					<ol start="6" type="1">
					<li><strong>Apakah kami dapat membawa sign atau banner atau lightsticks?</strong></li>
					</ol>
					<p>Sign/banner diperbolehkan selama ukuran masih dalam batas normal dan tidak menutupi pandangan penonton lainnya. Lightstick diperbolehkan selama tidak lebih dari 30cm.</p>
					<p>&nbsp;</p>
					<p><strong>&nbsp;</strong></p>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>