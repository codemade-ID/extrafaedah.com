<?php if(isset($script_captcha)){
        echo $script_captcha;
        } ?>
<section class="bg-white concert_is_back pad-b">
	<?php if($this->ua->is_mobile()) :?>
		<img src="<?php echo base_url()?>assets/img/concert_is_back_m.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php else:?>
		<img src="<?php echo base_url()?>assets/img/concert_is_back.png" class="img-responsive" style="margin: 0 auto; width: 100%;">
	<?php endif;?>
	<div class="container hide">
		<div class="row color-hitam">
			<p class="bold text-center font-20 mar-30 mar-l mar-r">Be the first to know</p>
			<form method="post" action="<?php echo current_url();?>" class="form-inline">
			<div class="col-md-1">
			</div>
			
			 <div class="col-md-4">
				<div class="form-group">
				    <label for="exampleInputEmail1">Name</label>
				    <input type="text" class="form-control" id="exampleInputName" placeholder="" name="name" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				    <label for="exampleInputEmail1">Email</label>
				    <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="" name="email" required="required">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
				    <button type="submit" class="btn btn-red">Remind Me</button>
				</div>
			</div>
			<div class="col-md-1">
			</div>
			</form>
		</div>
	</div>
</section>
<style type="text/css">
	.form-inline .form-control{width: 80%;}
	.form-inline .form-group{width: 100%;margin: 10px 0;}
	@media(max-width: 1200px){
		.form-inline .form-control{width: 70%;}
		.form-inline .form-group{width: 100%;}
	}
	@media(max-width: 980px){
		.form-inline .form-control{width: 100%;}
		.form-inline .form-group{width: 100%;}
	}
</style>
<script type="text/javascript">
	// $('button.btn-red').click(function(e){
	// 	var name =  $("input[name=name]").val();
	// 	var email =  $("input[name=email]").val();
	// 	$.post("<?php echo base_url();?>concert-isback", {name: name, email: email}, function(result){
	// 	    if(result == 'success'){
	// 	    	alert('success');
	// 	    	$('form.form-inline').submit();
	// 	    }
	// 	});

	// });
</script>
<?php if($this->session->flashdata('success') ==  'true') :?>

<!-- Modal -->
<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih"><?php echo $this->session->flashdata('message')?></h1>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(window).on('load',function(){
		$('#success').modal('show');
	});
</script>
<?php endif;?>