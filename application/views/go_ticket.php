<section class="go-ticket">
	<h1 class="text-center TruenoSBd"><span class="back-arrow"><a href="/concert"><i class="fa fa-arrow-left"></i></a></span>GET YOUR SPECIAL INVITATION</h1>
	<div class="container">
		<h2 class="text-center TruenoSBd">PAKET BUNDLING</h2>
		<div class="row">
			<div class="col-md-6">
				<div class="text-box text-center TruenoSBd">MiFi M6x</div>
				<img src="<?php echo base_url()?>assets/img/InvitationMiFi.png" class="img-responsive">
			</div>
			<div class="col-md-6">
				<div class="text-box text-center TruenoSBd">Kartu Perdana UNLIMITED</div>
				<img src="<?php echo base_url()?>assets/img/InvitationSP.png" class="img-responsive">
			</div>
		</div>
		<div class="row mar-30 mar-l mar-r">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-3 text-center">
				<div>
					<img src="<?php echo base_url()?>assets/img/buy-at.png" class="img-responsive center">
				</div>
			</div>
			<div class="col-md-7 text-center">
				<div class="list-btn">
					<p>
						<a href="javascript:;" data-toggle="modal" data-target="#SFGaleri" class="btn btn-ticket">  
							Smartfren Galeri
						</a>
					</p>
					<p>
						<a href="https://m.bukalapak.com/exclusive/smartfren-wow-concert" class="btn btn-ticket" target="_blank">  
							Bukalapak
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('go_tnc');?>
</section>
<!-- Modal -->
<div class="modal fade" id="SFGaleri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close color-merah-sf" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1 class="text-center color-putih">Smartfren Galeri</h1>
        <ul>
			<li>Sabang</li>
			<li>Ambassador</li>
			<li>Kelapa Gading</li>
			<li>Metropolitan Mall Bekasi</li>
			<li>BSD</li>
        </ul>
      </div>
    </div>
  </div>
</div>