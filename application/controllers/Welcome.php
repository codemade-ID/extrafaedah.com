<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('general_model');
		
		$this->data['lineup'] = array(
			array(
				"name"		=>	"Chung Ha",
				"image"		=>	"assets/img/lineup/Lineup01_Chungha.png"
			),
			array(
				"name"		=>	"Niki",
				"image"		=>	"assets/img/lineup/Lineup02_Niki.png"
			),
			array(
				"name"		=>	"ATEEZ",
				"image"		=>	"assets/img/lineup/Lineup03_Ateez.png"
			),
			array(
				"name"		=>	"Dewa 19",
				"image"		=>	"assets/img/lineup/Lineup_ahmaddhani.png"
			),
			array(
				"name"		=>	"Once Mekel",
				"image"		=>	"assets/img/lineup/Lineup_once.png"
			),
			array(
				"name"		=>	"Dul Jaelani",
				"image"		=>	"assets/img/lineup/Lineup05_Dul.png"
			),
			array(
				"name"		=>	"Jaz",
				"image"		=>	"assets/img/lineup/Lineup11_Jaz.png"
			),
			array(
				"name"		=>	"Via Vallen",
				"image"		=>	"assets/img/lineup/Lineup12_ViaVallen.png"
			),
			array(
				"name"		=>	"Rossa",
				"image"		=>	"assets/img/lineup/Lineup06_Rossa.png"
			),
			array(
				"name"		=>	"Rizky Febian",
				"image"		=>	"assets/img/lineup/Lineup10_Rizky.png"
			),
			array(
				"name"		=>	"Kallula",
				"image"		=>	"assets/img/lineup/Lineup08_Kallula.png"
			),
			array(
				"name"		=>	"Dipha Barus",
				"image"		=>	"assets/img/lineup/Lineup07_Diphabarus.png"
			),
			array(
				"name"		=>	"Alffy Rev",
				"image"		=>	"assets/img/lineup/Lineup09_Alffy.png"
			),
			array(
				"name"		=>	"Aaliyah Massaid",
				"image"		=>	"assets/img/lineup/Lineup_aaliyah.png"
			),
			
		);

		$this->data['foodtruck'] = array(
			array(
				"name"		=>	"BAGOJA",
				"image"		=>	"assets/img/foodtruck/MerchantList_Bagoja.png",
			),
			array(
				"name"		=>	"KOPI TUKU",
				"image"		=>	"assets/img/foodtruck/MerchantList_Tuku.png",
			),
			array(
				"name"		=>	"DONER KEBAB",
				"image"		=>	"assets/img/foodtruck/MerchantList_DonerKebab.png",
			),
			array(
				"name"		=>	"CHATIME",
				"image"		=>	"assets/img/foodtruck/MerchantList_Chatime.png",
			),
			array(
				"name"		=>	"EATLAH",
				"image"		=>	"assets/img/foodtruck/MerchantList_Eatlah.png",
			),
			array(
				"name"		=>	"PHITIX",
				"image"		=>	"assets/img/foodtruck/MerchantList_Phitix.png",
			),
			array(
				"name"		=>	"PIZZA MANGKOK",
				"image"		=>	"assets/img/foodtruck/MerchantList_PizzaMangkok.png",
			),
			array(
				"name"		=>	"CHOCBAN",
				"image"		=>	"assets/img/foodtruck/MerchantList_Chocban.png",
			),
			array(
				"name"		=>	"MASTER SQUID",
				"image"		=>	"assets/img/foodtruck/MerchantList_MasterSquid.png",
			),
			array(
				"name"		=>	"SANG PISANG",
				"image"		=>	"assets/img/foodtruck/MerchantList_SangPisang.png",
			),
			
		);

		$this->data['hadiah'] = array(
			array(
				"image"		=>	base_url()."assets/img/hadiah-1-rumah.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-1-mobil.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-3-motor.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-5-iphone-xs.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-10-bali.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-60-samsung.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-120-speaker.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-300-pulsa.png",
			),
			array(
				"image"		=>	base_url()."assets/img/hadiah-500-voucher.png",
			),
		);

		$this->data['hadiah2'] = array(
			array(
				"name"		=>	"1 Rumah",
				"image"		=>	base_url()."assets/img/undian2/hadiah-1-rumah.png",
			),
			array(
				"name"		=>	"1 Mobil Kijang Innova",
				"image"		=>	base_url()."assets/img/undian2/hadiah-1-mobil.png",
			),
			array(
				"name"		=>	"3 Motor Vespa",
				"image"		=>	base_url()."assets/img/undian2/hadiah-3-motor.png",
			),
			array(
				"name"		=>	"5 iPhone XR",
				"image"		=>	base_url()."assets/img/undian2/hadiah-5-iphone-xs.png",
			),
			array(
				"name"		=>	"10 Paket Liburan",
				"image"		=>	base_url()."assets/img/undian2/hadiah-10-bali.png",
			),
			array(
				"name"		=>	"50 Samsung A10s",
				"image"		=>	base_url()."assets/img/undian2/hadiah-60-samsung.png",
			),
			array(
				"name"		=>	"100 Bluetooth Speaker JBL GO",
				"image"		=>	base_url()."assets/img/undian2/hadiah-120-speaker.png",
			),
			array(
				"name"		=>	"300 Voucher Pulsa 300RB",
				"image"		=>	base_url()."assets/img/undian2/hadiah-300-pulsa.png",
			),
			array(
				"name"		=>	"530 Voucher Belanja 200RB",
				"image"		=>	base_url()."assets/img/undian2/hadiah-500-voucher.png",
			),
		);








		$this->data['hadiah3'] = array(
			array(
				"name"		=>	"1 Unit Rumah <br />",
				"image"		=>	base_url()."assets/img/undian3/Rumah.png",
			),
			array(
				"name"		=>	"1 Unit Mobil Innova Tipe G A/T <br />",
				"image"		=>	base_url()."assets/img/undian3/Mobil.png",
			),
			array(
				"name"		=>	"3 Unit Vespa Tipe LX 125 <br />",
				"image"		=>	base_url()."assets/img/undian3/Vespa.png",
			),
			array(
				"name"		=>	"5 Unit Samsung Note 10 <br />",
				"image"		=>	base_url()."assets/img/undian3/Note-01.png",
			),
			array(
				"name"		=>	"10 Tabungan Sinarmas Rp.5.000.000,-",
				"image"		=>	base_url()."assets/img/undian3/Simas.png",
			),
			array(
				"name"		=>	"30 Unit Samsung A10s <br /> <br />",
				"image"		=>	base_url()."assets/img/undian3/A10.png",
			),
			array(
				"name"		=>	"450 Voucher Pulsa 250RB",
				"image"		=>	base_url()."assets/img/undian3/voucher.png",
			),
			array(
				"name"		=>	"1,000 Voucher Belanja Tokopedia 100RB",
				"image"		=>	base_url()."assets/img/undian3/Belanja.png",
			),
			// array(
			// 	"name"		=>	"530 Voucher Belanja 200RB",
			// 	"image"		=>	base_url()."assets/img/undian2/hadiah-500-voucher.png",
			// ),
		);

		$this->data['hadiah4'] = array(
			array(
				"name"		=>	"1 Unit Rumah <br />",
				"image"		=>	base_url()."assets/img/undian3/Rumah.png",
			),
			array(
				"name"		=>	"1 Unit Mobil Xpander Cross A/T<br />",
				"image"		=>	base_url()."assets/img/undian4/Xpander-Cross.png",
			),
			array(
				"name"		=>	"3 Unit Motor Yamaha NMAX<br />",
				"image"		=>	base_url()."assets/img/undian4/Yamaha-NMAX.png",
			),
		);

		$this->data['fb_share']	= "#SmartfrenWOW";
		$this->data['tw_share'] = "#SmartfrenWOW";
	}

	public function index()
	{
		$this->data['content'] = $this->load->view("home",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function home()
	{
		$this->data['content'] = $this->load->view("home_new",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function lineup(){
		$this->data['content'] = $this->load->view("lineup",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function ticket(){
		$this->data['content'] = $this->load->view("ticket",true, true);
		$this->load->view('theme', $this->data);
	}

	public function galeri(){
		$this->data['content'] = $this->load->view("galeri",true, true);
		$this->load->view('theme', $this->data);
	}

	public function pose(){
		$this->data['fb_share']	= "Ikutan #WOWposeChallenge yuk, kamu dan teman kamu bisa dapetin Special Invitation #WOWconcert. Buruan yaaaa! @smartfrenworld #SmartfrenWOW. Cek di sini";
		$this->data['tw_share'] = "Yuk buruan ikutan #WOWposeChallenge dan dapetin Special Invitation #WOWconcert buat kamu dan teman kamu! #SmartfrenWOW. Cek di sini ".base_url()."wow/pose-challenge";
		$this->data['content'] = $this->load->view("pose",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function lipsync(){
		$this->data['fb_share']	= "Menangkan Exclusive Photo Session bareng Chung Ha dan ATEEZ. Yuk buruan ikutan #WOWlipsyncBattle! #SmartfrenWOW. Cek di sini";
		$this->data['tw_share'] = "Mau dapetin Exclusive Photo Session bareng Chung Ha dan ATEEZ? Gampang banget guys, ikutan aja #WOWlipsyncBattle! @smartfrenworld #SmartfrenWOW. Cek di sini ".base_url()."/wow/lipsync-battle	";
		$this->data['content'] = $this->load->view("lipsync",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function map(){
		$this->data['content'] = $this->load->view("map",true, true);
		$this->load->view('theme', $this->data);
	}

	public function undian2(){
		$this->data['gallery'] = array(
			array(
				"name"		=>	"Chung Ha",
				"image"		=>	base_url()."assets/img/IMG_0082.JPG"
			),
			array(
				"name"		=>	"Niki",
				"image"		=>	base_url()."assets/img/IMG_0174.JPG"
			),
			array(
				"name"		=>	"ATEEZ",
				"image"		=>	base_url()."assets/img/IMG_0210.JPG"
			),
			array(
				"name"		=>	"Dewa 19",
				"image"		=>	base_url()."assets/img/IMG_0218.JPG"
			),
			array(
				"name"		=>	"Once Mekel",
				"image"		=>	base_url()."assets/img/IMG_0223.JPG"
			),
			
		);
		$this->data['fb_share']	= "yuk ikuti program undian #smartfrenWOW dapatkan rumah, mobil , vespa & ratusan hadiah lainnya, Total 1000 pemenang di undi tiap bulan ".base_url()."";
		$this->data['tw_share'] = "yuk ikuti program undian #smartfrenWOW dapatkan rumah, mobil , vespa & ratusan hadiah lainnya, Total 1000 pemenang di undi tiap bulan ".base_url()."";
		$this->data['content'] = $this->load->view("undian",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function undian(){
		// $this->data['gallery'] = array(
		// 	array(
		// 		"name"		=>	"Chung Ha",
		// 		"image"		=>	base_url()."assets/img/IMG_0082.JPG"
		// 	),
		// 	array(
		// 		"name"		=>	"Niki",
		// 		"image"		=>	base_url()."assets/img/IMG_0174.JPG"
		// 	),
		// 	array(
		// 		"name"		=>	"ATEEZ",
		// 		"image"		=>	base_url()."assets/img/IMG_0210.JPG"
		// 	),
		// 	array(
		// 		"name"		=>	"Dewa 19",
		// 		"image"		=>	base_url()."assets/img/IMG_0218.JPG"
		// 	),
		// 	array(
		// 		"name"		=>	"Once Mekel",
		// 		"image"		=>	base_url()."assets/img/IMG_0223.JPG"
		// 	),
			
		// );
		$this->data['photos'] = $this->general_model->get_galeri_undian(5,'photo');

		$this->data['fb_share']	= "yuk ikuti program undian #smartfrenWOW dapatkan rumah, mobil , vespa & ratusan hadiah lainnya, Total 1000 pemenang di undi tiap bulan ".base_url()."";
		$this->data['tw_share'] = "yuk ikuti program undian #smartfrenWOW dapatkan rumah, mobil , vespa & ratusan hadiah lainnya, Total 1000 pemenang di undi tiap bulan ".base_url()."";
		$this->data['content'] = $this->load->view("undian2",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function layanantar(){
		$this->data['content'] = $this->load->view("layanantar",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function howtoget(){
		$this->data['content'] = $this->load->view("howtoget",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	private function _get_winner($tahap = 1, $prize = ''){
		$this->load->model('general_model');
		$return = $results =  $this->general_model->get_winners($tahap, $prize);
		// $param['where_clause'] = array(
		// 	'tahap' 	=> $tahap,
		// 	'prize'		=> $prize
		// );
		// $param['table'] = "sfweb_wow_winner";
		// $data = $this->general_model->get_table(0,500,'mdn','asc',$param);
		// $return = @$data['results'];

		return $return;
	}

	public function pemenang($tahap = 1){
		$realm = 'Restricted area';

		//user => password
		$users = array('ngapak' => 'kepenak');


		// if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
		//     header('HTTP/1.1 401 Unauthorized');
		//     header('WWW-Authenticate: Digest realm="'.$realm.
		//            '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

		//     // die('Text to send if user hits Cancel button');
		// }

		
		$this->data['rumah'] = $this->_get_winner($tahap, 'House');
		$this->data['mobil'] = $this->_get_winner($tahap, 'Car');
		$this->data['xpander'] = $this->_get_winner($tahap, 'Car_Xpander');
		$this->data['vespa'] = $this->_get_winner($tahap, 'MotorCycle');
		$this->data['nmax'] = $this->_get_winner($tahap, 'MotorCycle_NMAX');
		$this->data['iphone'] = $this->_get_winner($tahap, 'iPhone XS');
		$this->data['iphonexr'] = $this->_get_winner($tahap, 'iPhone XR');
		$this->data['samsungnote10'] = $this->_get_winner($tahap, 'Samsung Note 10');
		$this->data['tabungansimas5'] = $this->_get_winner($tahap, ' Tabungan Sinarmas @Rp. 5.000.000');
		$this->data['bali'] = $this->_get_winner($tahap, 'Holiday Package');
		$this->data['samsung'] = $this->_get_winner($tahap, 'Samsung A50');
		$this->data['samsunga10s'] = $this->_get_winner($tahap, 'Samsung A10s');
		$this->data['speaker'] = $this->_get_winner($tahap, 'Bluetooth Swarovski');
		$this->data['speakerjbl'] = $this->_get_winner($tahap, 'Bluetooth Speaker JBL GO');
		$this->data['voucher'] = $this->_get_winner($tahap, 'Voucher Pulsa 500rb');
		$this->data['voucher300rb'] = $this->_get_winner($tahap, 'Voucher PULSA 300RB');
		$this->data['belanja'] = $this->_get_winner($tahap, 'Voucher Shopping');
		$this->data['belanja200rb'] = $this->_get_winner($tahap, 'Voucher Belanja 200RB');
		$this->data['voucher250rb'] = $this->_get_winner($tahap, 'Voucher PULSA @ Rp.250.000');
		$this->data['vouchertoped100'] = $this->_get_winner($tahap, 'Voucher Belanja Tokopedia Rp. 100.000');

		$this->data['tahap'] = $tahap;

		// $param['where_clause']	= array(
		// 	'tahap'		=>	$tahap,
		// 	'tipe'		=>	'video'
		// );
		// $param['table']	= 'sfweb_wow_galeriundian';
		// $this->data['videos'] = $this->general_model->get_table(0,5,'id','asc',$param);

		// $param['where_clause']	= array(
		// 	'tahap'		=>	$tahap,
		// 	'tipe'		=>	'photo'
		// );
		// $param['table']	= 'sfweb_wow_galeriundian';
		// $this->data['photos'] = $this->general_model->get_table(0,5,'id','asc',$param);

		$this->data['videos'] = $this->general_model->get_galeri_undian($tahap,'video');
		$this->data['photos'] = $this->general_model->get_galeri_undian($tahap,'photo');
		// var_dump($this->data['videos']);exit;
		
		
		$this->data['content'] = $this->load->view("pemenang",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	function winner_search(){
		if(!$_POST){
			exit;
		}
		$mdn = $this->input->post('mdn');
		$tahap = $this->input->post('tahap');
		$this->load->model('general_model');
		$return =  $this->general_model->search_winners($tahap, $mdn);
		$returns = '';
		if(!empty($return)){
			$returns['mdn'] = ccMasking($return->mdn);
			$returns['coupon'] = $return->coupon;
			$returns['prize'] = $return->prize;
		}
		echo json_encode($returns);
	}

	public function meraihmimpi(){
		$this->data['fb_share']	= "Yuk #BeraniMeraihMimpi mu sekarang dengan cover theme song #smartfrenWOW competition dapatkan Hadiahnya #IdolaSmartfrenWOW ".base_url()."meraihmimpi";
		$this->data['tw_share'] = "Yuk #BeraniMeraihMimpi mu sekarang dengan cover theme song #smartfrenWOW competition dapatkan Hadiahnya #IdolaSmartfrenWOW ".base_url()."meraihmimpi";

		$this->data['content'] = $this->load->view("meraihmimpi",$this->data, true);
		$this->load->view('theme', $this->data);
	}

	public function infos(){
		$this->data['content'] = $this->load->view("info",$this->data, true);
		$this->load->view('theme', $this->data);
	}


}
