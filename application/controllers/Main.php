<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->data['fb_share']	= "Yuk Bisa Yuk Bikin #RamadanExtraFaedah Tahun Ini!";
		$this->data['tw_share']	= "Yuk Bisa Yuk Bikin #RamadanExtraFaedah Tahun Ini!";
		$this->data['per_page']	= 100;
	}

	function index(){
		
		$this->load->view('nonstopkreasiq/teaser', $this->data);
	}

	function home(){
		$this->load->model('post_model');
		$param['table']	= "submission";
		$param['where_clause'] = array(
			"status"		=> 	1,
			"thumbnail <>"	=>  ""
		);
		$data = $this->post_model->get_all(0,$this->data['per_page'],'id','desc',$param);
		$this->data['submission'] = $data['results'];
		// var_dump($data);exit;
		$this->load->view('nonstopkreasiq/theme', $this->data);
	}

	function home2(){
		$this->load->model('post_model');
		$param['table']	= "submission";
		$param['where_clause'] = array(
			"status"		=> 	1,
			"thumbnail <>"	=>  ""
		);
		$data = $this->post_model->get_all(0,$this->data['per_page'],'id','desc',$param);
		$this->data['submission'] = $data['results'];
		// var_dump($data);exit;
		$this->load->view('nonstopkreasiq/theme2', $this->data);
	}

	function extrafaedah(){
		$this->load->library('libauth');
        // $this->libauth->logged_in();
		$this->load->model('post_model');
		$param['table']	= "submission";
		$param['where_clause'] = array(
			"status"		=> 	1,
			"thumbnail <>"	=>  ""
		);
		$data = $this->post_model->get_all(0,$this->data['per_page'],'id','desc',$param);
		$this->data['submission'] = $data['results'];

		$this->load->view('extrafaedah/theme', $this->data);
	}
}