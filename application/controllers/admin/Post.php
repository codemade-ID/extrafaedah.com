<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('libauth');
        $this->libauth->logged_in();
		$this->load->model('post_model');
		$this->data['title'] = "Post / Page";
		$this->data['query_string'] = $_SERVER['QUERY_STRING'];
		// var_dump($this->data['query_string']);exit;
	}

	function setthumnail(){
	  define('UPLOAD_DIR', './uploads/');  
      $img = $_POST['imgBase64'];  
      $img = str_replace('data:image/png;base64,', '', $img);  
      $img = str_replace(' ', '+', $img);  
      $data = base64_decode($img);  
      $filename = uniqid() . '.png';
      $file = UPLOAD_DIR . $filename;  
      $success = file_put_contents($file, $data);  
      print $success ? $filename : 'Unable to save the file.';  
	}

	function index(){
		
		$param['select'] = "a.*";
		$param['where_clause']['a.status >='] = 0;

		// $data = $this->post_model->get_all_page(0,2000,'a.id', 'DESC', $param);
		$data = $this->post_model->get_all(0,2000,'a.id', 'DESC', $param);
	
		$this->data['rows'] = $data['results'];

		$this->data['content'] = "admin/post/index";
		$this->load->view('admin/theme', $this->data);
	}

	function add(){
		$this->load->library('form_validation');
		$this->load->helper('security');
		// $this->form_validation->set_rules('namalengkap','Nama Lengkap','required|xss_clean');
		// $this->form_validation->set_rules('profesi','Profesi','required|xss_clean');
		$this->form_validation->set_rules('instagramlink','Instagram Link','required|xss_clean');
		// $this->form_validation->set_rules('email','Email','required|xss_clean');
		$this->form_validation->set_rules('status','Status','required|xss_clean');
		// $this->form_validation->set_rules('thumbnail','Thumbnail','required|xss_clean');
		
		if($this->form_validation->run() === TRUE):
			$this->post_model->add();
			redirect('/admin/post?'.$this->data['query_string']);
		endif;

		
		
		$this->data['content'] = "admin/post/add";
		$this->load->view('admin/theme', $this->data);
	}

	function edit($id=0){
		$this->load->library('form_validation');
		$this->load->helper('security');
		// $this->form_validation->set_rules('namalengkap','Nama Lengkap','required|xss_clean');
		// $this->form_validation->set_rules('profesi','Profesi','required|xss_clean');
		$this->form_validation->set_rules('instagramlink','Instagram Link','required|xss_clean');
		// $this->form_validation->set_rules('email','Email','required|xss_clean');
		$this->form_validation->set_rules('status','Status','required|xss_clean');
		// $this->form_validation->set_rules('thumbnail','Thumbnail','required|xss_clean');
		
		if($this->form_validation->run() === TRUE):
			$this->post_model->update($id);
			redirect('/admin/post?'.$this->data['query_string']);
		endif;

		unset($param);
		$param['where_clause']['id'] = $id;
		$data = $this->post_model->get_all(0,1,'a.id', 'DESC', $param);
		$this->data['row'] = $data['results'][0];
		if(empty($this->data['row']))
			show_404();
		
		$this->data['content'] = "admin/post/add";
		$this->load->view('admin/theme', $this->data);
	}

	function delete(){
		if($_POST):
			$this->post_model->delete($this->input->post('id'));
			echo "success";
			exit;
		else:
			redirect(base_url().'admin/post/');
		endif;
	}
	
}