<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ktt extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->data['fb_share']	= "#KreatifTanpaTapi";
		$this->data['per_page']	= 16;
	}

	function index(){
		$this->load->model('general_model');
		$param['table']	= "submission";
		$param['where_clause'] = array(
			"status"		=> 	1,
			"thumbnail <>"	=>  ""
		);
		$data = $this->general_model->get_table(0,$this->data['per_page'],'id','desc',$param);
		$this->data['submission'] = $data['results'];

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'mp3|mp4|jpg|png';
        $config['max_size']             = 25000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
		$this->load->library('upload', $config);

		$this->load->library("form_validation");
		$this->load->helper('security');
		$this->form_validation->set_error_delimiters('<p class="color-merah">', '</p>');

		$this->form_validation->set_rules('namalengkap','Nama Lengkap','required|xss_clean');
		$this->form_validation->set_rules('profesi','Profesi','required|xss_clean');
		$this->form_validation->set_rules('instagramlink','Instagram Link','required|xss_clean');
		$this->form_validation->set_rules('email','Email','required|xss_clean');
		$this->form_validation->set_rules('filenames','File Upload','required|xss_clean');

		if ($this->form_validation->run() != FALSE){

			// if ($this->upload->do_upload('filename'))
   //          {	

            	$result_upload = $this->upload->data();
            	$ext = pathinfo('./uploads/'.$this->input->post("filenames"), PATHINFO_EXTENSION);
            	$data = array(
            		"namalengkap"		=>	$this->input->post("namalengkap"),
            		"profesi"			=>	$this->input->post("profesi"),
            		"instagramlink"		=>	$this->input->post("instagramlink"),
            		"email"				=>	$this->input->post("email"),
            		"file"				=>	$this->input->post("filenames"),
            		"file_ext"			=>	$ext,
            		"created_datetime"	=>	date("Y-m-d H:i:s")
            	);

            	$this->db->insert("submission", $data);
            	$this->send_email($this->input->post("email"));

                $this->session->set_flashdata('msg','success');
				redirect(base_url());    
    //         }else{
    //         	$this->data['content'] = $this->load->view("ktt/index",$this->data, true);
				// $this->load->view('ktt/theme', $this->data);
    //         }
		}else{
			$this->data['content'] = $this->load->view("ktt/index",$this->data, true);
			$this->load->view('ktt/theme', $this->data);
		}
	}

	function upload(){
		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'mp3|mp4|jpg|png';
        $config['max_size']             = 25000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('filename'))
        {	

        	$result_upload = $this->upload->data();
        	echo $result_upload['file_name'];
        }else{
        	echo $this->upload->display_errors();
        }
	}

	function send_email($to = "mazkurni@gmail.com"){
		$this->load->config('email');
        $this->load->library('email');
        
        $from = $this->config->item('smtp_user');
        $to = $to;
        $subject = "Subject";
        $message = "Hi, \n\nTerimakasih sudah submit karya kamu di smartfren #KreatifTanpaTapi. Karya kamu sudah diterima dan akan kami review. Karyamu akan muncul 1x24 Jam di Gallery Website.\n\nTunggu pengumuman selanjutnya ya!\n\nSalam, \nsmartfren";

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
        // if ($this->email->send()) {
        //     echo 'Your Email has successfully been sent.';
        // } else {
        //     show_error($this->email->print_debugger());
        // }

        return true;
	}

	public function load($rowno=0){
		$rowperpage = $this->data['per_page'];
 
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }


		$this->load->model('post_model');
		$param['table']	= "submission";
		$param['where_clause'] = array(
			"status"		=> 	1,
			"thumbnail <>"	=>  ""
		);
		$data = $this->post_model->get_all($rowno,$rowperpage,'id','desc',$param);
		$total = $data['total_results'];
		$results = $data['results'];
        
  
        $allcount = $total;
 
        $users_record = $results;
  
        $config['base_url'] = base_url().'load';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
 
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close']  = '</span></li>';
 
 		$this->load->library("pagination");
        $this->pagination->initialize($config);
 
        $data['pagination'] = $this->pagination->create_links();
        $data['submission'] = $users_record;
        $data['row'] = $rowno;

        $this->load->view('ktt/load_gallery', $data);
 
        // echo json_encode($data);
  }

}